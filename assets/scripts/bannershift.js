﻿$(function () {
    var len=$("#bannerShift li").length;
    if(len > 1){
        //banner切换开始
        var width=$(window).width(),
            len=$("#bannerShift li").length,
            index=0;
        start();
        $("#contro li").each(function(i){
            $(this).click(function(){
                parse();
                index=i;
                $("#contro li").removeClass("active");
                $("#contro li").eq(index).addClass("active");
                $("#bannerShift").stop(true,false).animate({"left":-width*i},500,function(){
                    start();
                });
            });
        });
        $("#bannerShift").width(width*len);
        $("#bannerShift li").width(width);

        var time;
        function start(){
            time= setInterval(function(){
                index++;
                if(index > $("#bannerShift li").length-1){
                    index=0;
                }
                $("#contro li").removeClass("active");
                $("#contro li").eq(index).addClass("active");
                $("#bannerShift").animate({"left":-width*index},500);
            },1500);
        }
        function parse(){
            clearInterval(time);
        }
        //banner切换结束
    }
});

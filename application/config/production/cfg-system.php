<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| 权限控制设置
|--------------------------------------------------------------------------
|
| active ：TRUE/FALSE 控制是否开始权限和登陆验证
| user_tag ：在session中存放登陆标志的键值
| right_tag ：在session中存放用户权限资源的键值
| login->return_url ：如果没有登陆，跳转的页面
| more_role->return_url ：如果没有权限，跳转的页面
| 权限数据用array保存，如下示例：
| array( 'model' => array ('index','page','view','showmodify') )
| ignore 不进行权限验证的模块
|
*/
$config['acl']['active'] = true;
$config['acl']['sessionname'] = 'sessioninfo';
$config['acl']['user_tag'] = 'userId';
$config['acl']['right_tag'] = 'rights';
$config['acl']['default'] = 'home';
$config['acl']['login'] = 'admin/login';
$config['acl']['ignore']=array('public','front','home','service','app');
$config['acl']['fullright']=array('admin/home','admin/metronic');

$config['system']['cachetime'] = 0;			//缓冲定义，0 为不开启， 1以上为缓冲最小时间，单位分
$config['system']['cachetime_max'] = 15;		//缓冲定义，随机最大缓冲时间，单位分
$config['system']['perpageno'] = 6;             // 每页行数
$config['system']['commentno'] = 2;             // 产品详情页评价行数
$config['system']['dispmode'] = 'WIN8';  // WIN8,METRONIC


$config['seo']['title']='网络信息管理系统';         // 标题
$config['seo']['description']='';         // description
$config['seo']['keywords']='';         // description

$config['centreon']['active'] = false;
$config['centreon']['host'] = '192.168.80.131';
$config['centreon']['account'] = 'centreon';
$config['centreon']['password'] = 'centreon';
$config['centreon']['db'] = 'centreon';



<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'] = array(
	 'class' => 'Acl',    //类名
	 'function' => 'filter',  //方法名称
	 'filename' => 'acl.php',  //文件名称
	 'filepath' => 'hooks',  //路径
);
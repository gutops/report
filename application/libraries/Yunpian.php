<?php
/**
 * @copyright (c) 2011 aircheng.com
 * @file zhutong.php
 * @brief **短信发送接口
 * @author nswe
 * @date 2015/5/30 15:46:38
 * @version 3.3
 */

 /**
 * @class zhutong
 * @brief 短信发送接口 短信后台地址
 */
class Yunpian
{
	private $submitUrl  = "https://sms.yunpian.com/v2/sms/single_send.json";

	/**
	 * @brief 获取config用户配置
	 * @return array
	 */
	public function getConfig()
	{
	    $CI = & get_instance();
        $CI->config->load('cfg-system', true);
        $smsconfig = $CI->config->item('cfg-system');
        $smsconfig = $smsconfig['sms'];
        return $smsconfig;
	}

	/**
	 * @brief 发送短信
	 * @param string $mobile
	 * @param string $content
	 * @return
	 */
	public function send($mobile,$content)
	{
		$config = self::getConfig();
		$post_data = array(
			'apikey' => $config['sms_userid'],
			'text'  => $content,
			'mobile'   => $mobile,
		);
		log_message("DEBUG","sms info: ".print_r($post_data,true));
		$url    = $this->submitUrl;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //如果需要将结果直接返回到变量里，那加上这句。
		$result = curl_exec($ch);
        $result_array = json_decode($result,true);
        log_message("DEBUG","SMS Result:".print_r($result_array,true));
        if($result_array['code'] == 0){
            return 'success';
        }else{
            return $result_array['detail'];
        }

	}




	/**
	 * @brief 解析结果
	 * @param $result 发送结果
	 * @return string success or fail
	 */
	public function response($result)
	{
		if(strpos($result,'1,') === 0)
		{
			return 'success';
		}
		else
		{
			return $this->getMessage($result);
		}
	}

	/**
	 * @brief 获取参数
	 */
	public function getParam()
	{
		return array(
			"username" => "用户名",
			"userpwd"  => "密码",
			"usersign" => "短信签名",
		);
	}

	/**
	 * @brief 根据短信内容返回产品ID
	 * @param $content
	 */
	public function getProductCode($content)
	{
		$codeWord = array(
			1 => array('验证码'),
			2 => array('通知'),
			3 => array('营销','活动','购买','广告','打折','降价','促销','机会')
		);

		$resultCode = 2;
		foreach($codeWord as $codeNum => $wordArray)
		{
			if(is_array($wordArray) && $wordArray)
			{
				foreach($wordArray as $word)
				{
					if(strpos($content,$word) !== false)
					{
						$resultCode = $codeNum;
						break;
					}
				}
			}
		}
		return self::$productCode[$resultCode];
	}

	//返回消息提示
	public function getMessage($code)
	{
		$messageArray = array(
			-1 =>"用户名或者密码不正确或用户禁用",
			2  =>"余额不够或扣费错误",
			3  =>"扣费失败异常（请联系客服）",
			6  =>"有效号码为空",
			7  =>"短信内容为空",
			8  =>"无签名，必须，格式：【签名】",
			9  =>"没有Url提交权限",
			10 =>"发送号码过多,最多支持200个号码",
			11 =>"产品ID异常或产品禁用",
			12 =>"参数异常",
			13 =>"30分种重复提交",
			14 =>"用户名或密码不正确，产品余额为0，禁止提交，联系客服",
			15 =>"Ip验证失败",
			19 =>"短信内容过长，最多支持500个",
			20 =>"定时时间不正确：格式：20130202120212(14位数字)",
		);
		return isset($messageArray[$code]) ? $messageArray[$code] : "未知错误";
	}
}
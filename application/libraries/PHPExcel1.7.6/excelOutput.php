<?php

error_reporting(E_ALL); //开启错误
set_time_limit(0); //脚本不超时

date_default_timezone_set('Europe/London'); //设置时间

/** Include path **/
set_include_path(get_include_path() . PATH_SEPARATOR . 'http://www.cnblogs.com/../Classes/');//设置环境变量

/** PHPExcel_IOFactory */
include '/libraries/PHPExcel1.7.6/PHPExcel/IOFactory.php';

//$inputFileType = 'Excel5'; //这个是读 xls的
$inputFileType = 'Excel2007';//这个是计xlsx的
//$inputFileName = './sampleData/example2.xls';
$inputFileName = './sampleData/book.xlsx';

echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory with a defined reader type of ',$inputFileType,'<br />';
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($inputFileName);
/*
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow(); //取得总行数
$highestColumn = $sheet->getHighestColumn(); //取得总列
*/
$objWorksheet = $objPHPExcel->getActiveSheet();//取得总行数
$highestRow = $objWorksheet->getHighestRow();//取得总列数

echo 'highestRow='.$highestRow;
echo "<br>";
$highestColumn = $objWorksheet->getHighestColumn();
$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);//总列数
echo 'highestColumnIndex='.$highestColumnIndex;
echo "<br />";
$headtitle=array();
for ($row = 1;$row <= $highestRow;$row++)
{
$strs=array();
//注意highestColumnIndex的列数索引从0开始
for ($col = 0;$col < $highestColumnIndex;$col++)
{
$strs[$col] =$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
}
$info = array(
'word1'=>"$strs[0]",
'word2'=>"$strs[1]",
'word3'=>"$strs[2]",
'word4'=>"$strs[3]",
);
//在这儿，你可以连接，你的数据库，写入数据库了
print_r($info);
echo '<br />';
}

?>
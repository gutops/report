<!DOCTYPE html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/user/gb.css"/>
    <script language="javascript" type="text/javascript"
            src="<?php echo base_url(); ?>assets/scripts/jquery-1.6.1.js"></script>
    <script type="text/javascript">
        window.onload = function () {

            $(".p-app-tab li").live("click", function () {
                if ($(this).attr("class") == "active")
                    return;
                var old = $(".active").attr("class", "no-active").attr("id");
                $(this).attr("class", "active");
                $("li." + old).css("display", "none");
                $("li." + $(this).attr("id")).css("display", "");
                show_type();
            });
            $("li.shortcut").css({"display": ""});

            $("div.btn-app-add").live("click", function () {
                var page = $("#desk_switch", parent.document).text();
                var id = $(this).parent().attr("id");
                var col = window.parent.bg_div_default_col_num;
                var row = window.parent.bg_div_default_row_num;
                var url = "<?php echo site_url() . '/public/addapp/changebtn';?>";
                $(this).parent().find(".btn-show").load(url, {
                    "id": id,
                    "type": "add",
                    "page": page,
                    "row": row,
                    "col": col
                }, function (response, status) {
                    if (status == "success") {
                        if (response == false) {
                            //alert("没有足够空间，添加失败！");
                        }
                        else {
                            $(this).parent().find("[class='btn-app-del']").css("display", "");
                            $(this).parent().find("[class='btn-app-add']").css("display", "none");
                            response = eval('(' + response + ')');

                            window.parent.createDrapDropDivsByData(response);
                        }
                    }
                });
            });
            $("div.btn-app-del").live("click", function () {
                var page = $("#desk_switch", parent.document).text();
                var id = $(this).parent().attr("id");
                var url = "<?php echo site_url() . '/public/addapp/changebtn';?>";
                $(this).parent().find(".btn-show").load(url, {
                    "id": id,
                    "type": "delete",
                    "page": page,
                    "row": 0,
                    "col": 0
                }, function (response, status) {
                    if (status == "success") {
                        if (response == false) {
                            //alert("删除失败！");
                        }
                        else {
                            response = eval('(' + response + ')');
                            window.parent.delDrapDropDivById(response);
                        }
                    }
                });
                $(this).parent().find("[class='btn-app-add']").css("display", "");
                $(this).css("display", "none");
                //parent.location.reload();
            });
            init_page_list();
            show_type();
            $("ul.p-app-tab").find("li:first").click();
        };
        function show_type() {
            $("li.p-app-type").css({"display": "", "border": 0});
            $("li.p-app-type").each(function () {
                var tmp = $(this).next("li");
                while (tmp.length != 0 && tmp.css("display") == "none")
                    tmp = tmp.next("li");
                if (tmp.length == 0 || tmp.attr("class") == "p-app-type")
                    $(this).css("display", "none");
            });
            var i = 1;
            $("li.p-app-type").each(function () {
                if ($(this).css("display") != "none") {
                    if (1 == i)
                        $(this).css("border-bottom", "2px solid green");
                    else
                        $(this).css({"border-bottom": "2px solid green", "border-top": "2px solid green"});
                    i++;
                }
            });
            var tmp = $("ul.p-app-bd").find("li:last");
            while (tmp.length != 0 && tmp.css("display") == "none")
                tmp = tmp.prev("li");
            tmp.css("border-bottom", "2px solid green");
        }
        function init_page_list() {
            var exitlist = <?php echo $exitlist;?>;
            for (var i in exitlist) {
                $("#" + exitlist[i]["conf_id"]).find("[class='btn-app-add']").css({"display": "none"});
                $("#" + exitlist[i]["conf_id"]).find("[class='btn-app-del']").css({"display": ""});
            }
        }
    </script>
</head>
<body>
<div class="p-app">
    <ul class="p-app-tab">
        <li class="no-active" id="newshortcut">快捷方式</li>
        <li class="no-active" id="app">小应用</li>
        <li class="no-active" id="outersite">外链</li>
    </ul>
    <ul class="p-app-bd clearfix">
        <?php if (isset($applist)) {
            $module_type = "";
            foreach ($applist as $key => $value) {
                $value["icon"] = str_replace("32", "64", $value["icon"]);
                if ($module_type == "") {
                    echo "<li class='p-app-type'>" . $value["group"] . "</li>";
                    $module_type = $value["group"];
                } else if ($module_type != "" && $module_type != $value["group"]) {
                    echo "<li class='p-app-type'>" . $value["group"] . "</li>";
                    $module_type = $value["group"];
                }
                echo "<li class='" . $value["type"] . "' style='display:none;border-left:2px solid green;border-right:2px solid green;' id='" . $value["id"] . "'>
                <em style='background:" . $value["bg_color"] . "'><img src='" . base_url() . "assets/img/webQQ/" . $value['icon'] . "' /></em>
                <div class='app-desc'><h1>" . $value['name'] . "</h1><p>" . $value['description'] . "</p></div><div class='btn-app-add' ></div>
                <div class='btn-app-del' style='display:none'></div><div class='btn-show' style='display:none'></div></li>";
            }
        }
        ?>

    </ul>
</div>
</body>
</html>
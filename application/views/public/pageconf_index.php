<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/scripts/jquery-1.6.1.js"></script>
<script type="text/javascript">
$("#confirm").live("click",function(){
	var url = "<?php echo site_url() . '/public/pageconf/savepageconf';?>";
	var data_num = "";
	var data_name = "";
	$("table.desk-change").find("#list").each(function(){
		data_num += $(this).find("td:first").find("span:first").text()+",";
		data_name += $(this).find("td").find("input[name=name]").val()+",";
	});
	var reg=/,$/gi;
   	data_num=data_num.replace(reg,"");
   	data_name=data_name.replace(reg,"");
   
	$("#divLoginErrorMsg").load(url,{"num":data_num,"name":data_name},function(response,status){
		if (status=="success")
		{
			alert("更新成功");
		}
		else
		{
			alert("更新失败");
		}
	});
});
$("#Img0").live("click",function(){
	var url = "<?php echo site_url() . '/public/pageconf/deletepageconf';?>";
	var li = $(this).closest("td").closest("tr");
	var id = li.find("span:first").text();
	$("#divLoginErrorMsg").load(url,{"id":id},function(response,status){
		if (status=="success")
		{
			li.remove();
			//alert("删除成功");
		}
		else
		{
			//alert("删除失败");
		}
	});
});
$("#add").live("click",function(){
	var url = "<?php echo site_url() . '/public/pageconf/addpageconf';?>";
	var ul = $("#desk-change_table");
	var num = (parseInt(ul.find("tr:last").find("td:first").find("span").text())*1)+1;
	if(isNaN(num) == true){
		num=2;
	}
	$("#divLoginErrorMsg").load(url,{"num":num},function(response,status){
		if (status=="success")
		{
			var new_tr = $("<tr/>");
			new_tr.appendTo(ul);
			var new_li = $("<td/>");
			new_li.appendTo(new_tr);
			$("<span/>").text(num).appendTo(new_li);
			var new_li = $("<td/>");
			new_li.appendTo(new_tr);
			$("<input/>").attr({"name":"name","type":"text"}).appendTo(new_li);
			var new_li = $("<td/>");
			new_li.appendTo(new_tr);
			$("<img/>").attr({"id":"Img0","src":"<?php echo base_url() . 'assets/img/login/zmyy2_rl_icon02_s.jpg';?>","title":"删除"}).appendTo(new_li);
			//alert("添加成功");
		}
		else
		{
			//alert("添加失败");
		}
	});
	
});
</script>

<base href="<?php echo site_url();?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="" name="description" />
<meta content="" name="author" />
<meta name="MobileOptimized" content="320">
<link href="<?php echo base_url() . 'assets/css/user/main.css';?>" rel="stylesheet" type="text/css"/>

<style type="text/css">

input {
	width:100px;
}
.mybtn
{
    background:url(<?php echo base_url() . 'assets/img/login/login_btn2.jpg';?>) no-repeat;
	width: 82px;
	height: 30px;
	border: 0;
	color: #fff;
	font-family: "微软雅黑", "黑体";
	font-size: 16px;
}
</style>
</head>
<body bgcolor="#e3e4e8">
<div class="nr">
	<table class="tab01 desk-change" id="desk-change_table" background="#f6f8fa" bgcolor="#f6f8fa" width="400px" border="0" cellspacing="0" cellpadding="0">
	<tr><td class="tit" colspan="7">修改页面名称 </td></tr>
	<tr><td class="t" width="20%">页码</td><td class="t" width="60%">名称</td><td class="t" width="20%"> 操作</td></tr>
	<?php foreach($page as $k=>$value):?>
	<?php if ($value["page_num"] ==0):?>
		<tr id="list"><td class="a"><span>默认打开</span></td><td><input type='text' name='name' value='<?php echo $value["page_name"];?>' /></td><td>页</td></tr>
	<?php else:?>
		<tr id="list"><td class="a"><span><?php echo $value["page_num"];?></span></td><td><input type='text' name='name' value='<?php echo $value["page_name"];?>' /></td>
		<td><img id="Img0" src="<?php echo base_url() . 'assets/img/login/zmyy2_rl_icon02_s.jpg';?>" style="cursor: pointer" title="删除" /></td></tr>
	<?php endif;?>
	<?php endforeach;?>
	</table>
	<table><tr><td colspan="2" class="a" style="text-align:right;">
		<button id="confirm" class="mybtn" style="cursor: pointer">更新</button>
        <button id="add" class="mybtn" style="cursor: pointer">添加</button></td></tr>
	</table>
</div>
<div id="divLoginErrorMsg" style="clear:both;float:left;width:315px;overflow:hidden;margin:15px 0 0 0;color:#f60"></div>
</body>
</html>
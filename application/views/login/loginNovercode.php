<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $seo['title'];?></title>
    <link type="text/css" href="<?php echo base_url(); ?>assets/css/user/main.css" rel="stylesheet"/>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
</head>

<body>
<form id="formLogin" action="<?php echo site_url(); ?>/admin/login/logingonovercode" method="post">
    <div class="loginbody">
        <div class="loginall">
            <div class="login">
                <h2 style="float:left">用户登录</h2>
                <div style="color:red;float:left;margin-left:20px;margin-top:4px;" id="divLoginErrorMsg"></div>
                <div class="bar">
                    <div class="fl t">账号：</div>
                    <div class="fl bk"><input class="in" id="username" name="username" data-validate="{required:true}"
                                              data-message="{required:'用户名不能为空'}" type="text"/></div>
                </div>
                <div class="bar">
                    <div class="fl t">密码：</div>
                    <div class="fl bk"><input class="in" id="passeword" name="passeword" data-validate="{required:true}"
                                              data-message="{required:'密码不能为空'}" type="password"/></div>
                </div>
                <div class="bar2 ac">
                    <button style="cursor:pointer" type="button" id="denglu" onclick="go(this);" class="btn">登录</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

    function go(obj) {
        var url = $('#formLogin').attr("action"),
            username = $("#username"),
            passeword = $("#passeword"),
            css = "border:1px solid red;",
            error = 2;
        $('#formLogin input').each(function () {
            if ($(this).val() == "") {
                error = 1;
                $(this).attr("style", css);
            }
        });
        if (error == 1) return false;
        $.ajax({
            url: url,
            type: "post",
            data: {"username": username.val(), "passeword": passeword.val()},
            success: function (data) {
                if (data == 1) {
                    window.location.reload();
                } else {
                    $("#divLoginErrorMsg").html(data);
                }
            }
        });

    }
    $(document).ready(function () {
        var ft = $('#formLogin input');
        ft.on("focus", function () {
            $(this).removeAttr("style");
        })
    })
</script>
</body>
</html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>网络信息管理系统</title>
    <link type="text/css" href="<?php echo base_url(); ?>assets/css/user/main.css" rel="stylesheet"/>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
</head>

<body>
<form id="formLogin" action="<?php echo site_url(); ?>/admin/login/forcechgpass" method="post">
    <div class="loginbody">
        <div class="loginpassall">
            <div class="loginpass">
                <h2 style="float:left">密码时间超期，修改密码</h2>
                <div class="bar">
                    <div class="fl t">账号：</div>
                    <div class="fl bk"><input class="in" id="username" name="username" data-validate="{required:true}"
                                              data-message="{required:'用户名不能为空'}" type="text"
                        <?php if(isset($username)) echo "value='".$username."''" ;?>/></div>
                </div>
                <div class="bar">
                    <div class="fl t">原密码：</div>
                    <div class="fl bk"><input class="in" id="oldpassword" name="oldpassword" data-validate="{required:true}"
                                              data-message="{required:'密码不能为空'}" type="password"/></div>
                </div>
                <div class="bar">
                    <div class="fl t">新密码：</div>
                    <div class="fl bk"><input class="in" id="newpassword" name="newpassword" data-validate="{required:true}"
                                              data-message="{required:'密码不能为空'}" type="password"/></div>
                </div>
                <div class="bar">
                    <div class="fl t">重复新密码：</div>
                    <div class="fl bk"><input class="in" id="new2password" name="new2password" data-validate="{required:true}"
                                              data-message="{required:'密码不能为空'}" type="password"/></div>
                </div>
                <div style="color:red;" id="divLoginErrorMsg"></div>
                <div class="bar2 ac">
                    <button style="cursor:pointer" type="button" id="denglu" onclick="go(this);" class="btn">提交</button>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">

    function go(obj) {
        var url = $('#formLogin').attr("action"),
            data = $('#formLogin').serialize();
            username = $("#username"),
            passeword = $("#passeword"),
            css = "border:1px solid red;",
            error = 2;
        $('#formLogin input').each(function () {
            if ($(this).val() == "") {
                error = 1;
                $(this).attr("style", css);
            }
        });
        if (error == 1) return false;
        $.getJSON(url+'?'+data,function (data) {
                if (data.success) {
                    window.location.href="<?php echo site_url('admin/metronic');?>";
                } else {
                    $("#divLoginErrorMsg").html(data.msg);
                }
            }
        );

    }
    $(document).ready(function () {
        var ft = $('#formLogin input');
        ft.on("focus", function () {
            $(this).removeAttr("style");
        })
    })
</script>
</body>
</html>

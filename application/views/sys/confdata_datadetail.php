<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>数据详情</div>
            </div>
            <div class="portlet-body form">
                <form action="" data-method-url='<?php echo $url_module . '/' . $url_model; ?>' method="POST"
                      class="data_formDetail form-horizontal" novalidate="novalidate"
                      data-keyid="<?php echo $keyid = isset($keyId) ? $keyId : ''; ?>"
                      editType="<?php echo $editType; ?>" jsonDetail='<?php echo json_encode(isset($datalist)?$datalist:array()); ?>'>
                    <!-- 表单主体 开始  -->
                    <div class="form-body">

                        <!-- 表格样式表单 开始 -->
                        <div class="tableForm">
                            <table class="table table-bordered ">
                                <!-- MODIFY START -->
                                <caption><span class="left">&nbsp;</span>数据字典<span class="right">&nbsp;</span></caption>
                                <!-- MODIFY END -->
                                <tbody>
                                <!-- MODIFY START -->
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">类型:</label></td>
                                    <td>
                                        <select class="form-control input-sm" data-validate="{required:'true'}"
                                                name="type">
                                            <?php echo bs_type_option($menuinfo,"type"); ?>
                                        </select>
                                    </td>
                                    <td class="lightgrey"></td>
                                    <td class="lightgrey"></td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">序列编码:</label></td>
                                    <td>
                                        <input class="form-control" name="seqno" data-validate="{required:true}"
                                               data-message="{required:'序列编码不能为空！'}" type="text"/>
                                    </td>

                                    <td><label class="control-label">排序:</label></td>
                                    <td>
                                        <input class="form-control" name="seq" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">名称:</label></td>
                                    <td>
                                        <input class="form-control" name="name" data-validate="{required:true}"
                                               type="text"/>
                                    </td>

                                    <td><label class="control-label">值:</label></td>
                                    <td>
                                        <input class="form-control" name="value" type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">文字附加1:</label></td>
                                    <td>
                                        <input class="form-control" name="msg1" type="text"/>
                                    </td>

                                    <td><label class="control-label">文字附加2:</label></td>
                                    <td>
                                        <input class="form-control" name="msg2" type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">文字附加3:</label></td>
                                    <td>
                                        <input class="form-control" name="msg3" type="text"/>
                                    </td>

                                    <td><label class="control-label">文字附加4:</label></td>
                                    <td>
                                        <input class="form-control" name="msg4" type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">文字附加5:</label></td>
                                    <td>
                                        <input class="form-control" name="msg5" type="text"/>
                                    </td>

                                    <td><label class="control-label">日期附加6:</label></td>
                                    <td>
                                        <div class="input-group date input-medium date-picker">
                                            <input id="msg6" type="text" size="16"
                                                   class="form-control"
                                                   name="msg6">
                                            <span class="input-group-btn">
                                                <button class="btn default date-set" type="button"><i
                                                            class="icon-calendar"></i></button>
            								</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">日期附加7:</label></td>
                                    <td>
                                        <div class="input-group date input-medium date-picker">
                                            <input id="msg7" type="text" size="16"
                                                   class="form-control"
                                                   name="msg7">
                                            <span class="input-group-btn">
                                                <button class="btn default date-set" type="button"><i
                                                            class="icon-calendar"></i></button>
            								</span>
                                        </div>
                                    </td>

                                    <td><label class="control-label">日期附加8:</label></td>
                                    <td>
                                        <div class="input-group date input-medium date-picker">
                                            <input id="msg8" type="text" size="16"
                                                   class="form-control"
                                                   name="msg8">
                                            <span class="input-group-btn">
                                                <button class="btn default date-set" type="button"><i
                                                            class="icon-calendar"></i></button>
            								</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">数值附加9:</label></td>
                                    <td>
                                        <input class="form-control" name="msg9" type="text"/>
                                    </td>

                                    <td><label class="control-label">数值附加10:</label></td>
                                    <td>
                                        <input class="form-control" name="msg10" type="text"/>
                                    </td>
                                </tr>                                <!-- MODIFY END -->
                                </tbody>
                            </table>

                        </div>
                        <!-- 表格样式表单 结束 -->
                    </div>
                    <!-- 表单主体 结束  -->

                    <!-- 表单操作 开始  -->
                    <div class="modal-footer">
                        <a type="button" class="btn default"
                           href="<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>">返回</a>
                        <button type="submit" class="btn blue submitBtn">确认</button>
                    </div>
                    <!-- 表单操作 开始  -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<?php $jscss_load = array('Detail', 'formValidate','FormDatePicker'); ?>
<?php include APPPATH . '/views/common/loadjs.php'; ?>

<script type="text/javascript">

    Detail().init();

    var cusVali = {};

    formValidate(".data_formDetail").init(cusVali);

    FormDatePicker.init();

</script>

<?php include APPPATH . '/views/common/footer.php'; ?>

</body>
<!-- END BODY -->
</html>


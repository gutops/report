<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<link href="<?php echo base_url(); ?>assets/css/user/gb-nobg.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>数据详情</div>
            </div>
            <div class="portlet-body form">
                <form action="" data-method-url='<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>'
                      method="POST"
                      class="data_formDetail form-horizontal" novalidate="novalidate"
                      data-keyid="<?php echo $keyid = isset($keyId) ? $keyId : ''; ?>"
                      editType="<?php echo $editType; ?>" jsonDetail='<?php echo json_encode(isset($datalist)?$datalist:array()); ?>'>

                    <!--- tab start -->
                    <div class="tabbable-custom nav-justified">
                        <ul class="nav nav-tabs nav-justified">
                            <?php foreach ($projects as $key => $projectName): ?>
                                <li <?php if ($key == 0): ?>class="active" <?php endif; ?>><a
                                            href="#project_area<?php echo $key; ?>"
                                            data-toggle="tab"><?php echo $projectName['projectName']; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content">
                            <?php foreach ($projects as $key => $projectName): ?>
                                <div id="project_area<?php echo $key; ?>"
                                     class="tab-pane p-app-2 <?php if ($key == 0): ?>active <?php endif; ?> ">
                                    <!-- 按钮权限循环 begin -->
                                    <ul class="clearfix">
                                        <?php $i = 0;
                                        foreach ($datalist as $arr):
                                            if ($arr["project_name"] != $projectName["projectName"])
                                                continue;
                                            ?>

                                            <?php $i++;
                                            if ($arr["icon"] == "") $arr["icon"] = "app/64/attendancerecord.png"; else $arr["icon"] = str_replace("32", "64", $arr["icon"]); ?>
                                            <?php $ll = explode(',', $arr["btnDefine"]); ?>
                                            <?php if ($arr["uriid"] == "") : ?>
                                            <li>
                                            <div class="headCheckbox"><input type="checkbox"
                                                                             name="<?php echo "id_" . $arr["id"]; ?>"
                                                                             class="form-control"/></div>
                                        <?php else: ?>
                                            <li>
                                            <div class="headCheckbox"><input type="checkbox"
                                                                             name="<?php echo "id_" . $arr["id"]; ?>"
                                                                             checked class="form-control"/></div>
                                        <?php endif; ?>
                                            <em style="background:<?php echo $arr["bg_color"]; ?>;">
                                                <img src="<?php echo base_url() . "assets/img/webQQ/" . $arr["icon"]; ?>"/></em>
                                            <div class="app-desc"><h1><?php echo $arr["name"]; ?></h1>
                                                <p><?php echo $arr["description"]; ?></p></div>
                                            <div class="p-app-checkbox">
                                                <?php foreach ($ll as $tmp): ?>
                                                    <?php if ($tmp != ""): ?>
                                                        <?php if (strpos($arr["btnrights"], $tmp) > -1): ?>
                                                            <div class="p-app-checkbox-li"><input type="checkbox"
                                                                                                  class="form-control"
                                                                                                  checked
                                                                                                  name="<?php echo "id_" . $arr["id"] . "_" . $tmp; ?>"/>
                                                                <p><?php echo $tmp; ?></p></div>
                                                        <?php else: ?>
                                                            <div class="p-app-checkbox-li"><input type="checkbox"
                                                                                                  class="form-control"
                                                                                                  name="<?php echo "id_" . $arr["id"] . "_" . $tmp; ?>"/>
                                                                <p><?php echo $tmp; ?></p></div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <!-- 按钮权限循环 end -->
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!--- tab end -->
                    <!-- 表单操作 开始  -->
                    <div class="modal-footer">
                        <a type="button" class="btn default"
                           href="<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>">返回</a>
                        <button type="submit" class="btn blue submitBtn">确认</button>
                    </div>
                    <!-- 表单操作 开始  -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<?php $jscss_load = array('Detail', 'formValidate'); ?>
<?php include APPPATH . '/views/common/loadjs.php'; ?>
<script type="text/javascript">
    Detail().init();
    var cusVali = {};
    formValidate(".data_formDetail").init(cusVali);
</script>
<?php include APPPATH . '/views/common/footer.php'; ?>
</body>
<!-- END BODY -->
</html>


<script>

    $(document).ready(function () {
        //初始化弹窗
        $('#dialogModifyCompanyPwd').dialog({
            title: '修改登录密码'
            , width: 300
            , height: 200
            , modal: true
            , resizable: false
            , position: [500, 300]
            , '关闭': function (event, ui) {
                $(this).remove();
            }
        });
        $(".ui-icon-closethick").html("关闭");
    });

    function checkData() {

    }
</script>

<style>
    body {
        color: #333;
        font-size: 12px;
    }

    input, select {
        height: 24px;
        margin: 3px 0 3px 0;
        border-top: 1px solid #aaa;
        border-left: 1px solid #aaa;
        border-bottom: 1px solid #ddd;
        border-right: 1px solid #ddd;
        float: left;
        line-height: 24px !important;
    }

    #dialogModifyCompanyPwd {
        font-size: 12px;
        color: #505050;
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold;
        background-color: #fff;
        text-align: center;
        padding: 20px;
        overflow: hidden;
        border: 1px solid #ccc;
    }

    .ui-dialog-titlebar {
        background: #333;
        color: #fff;
        height: 20px;
        line-height: 20px;
        text-indent: 10px;
    }

    .ui-icon-closethick {
        position: absolute;
        top: 0;
        right: 10px;
        color: #fff;
    }

    .ui-dialog-titlebar-close:hover {
        text-decoration: none;
        color: #ccc;
    }

    .ui-state-error {
        border: 1px solid red;
    }
</style>

<div id="dialogModifyCompanyPwd">
    <form id="formModifyCompanyPwd" action="" method="post">
        <input type="hidden"/>
        <input type="hidden"/>
        <table style="width:100%;white-space:nowrap;font-size:12px;">
            <tr>
                <td>原密码：</td>
                <td><input type="password" name="oldCompanyPwd" id="pwdOldCompanyPwd"/></td>
            </tr>
            <tr>
                <td>新密码：</td>
                <td><input type="password" name="newCompanyPwd" id="pwdNewCompanyPwd"/></td>
            </tr>
            <tr>
                <td>确认密码：</td>
                <td><input type="password" name="pwdConfirmCompanyPwd" id="pwdConfirmCompanyPwd"/></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;margin-top:10px;">
                    <input id="gofrom"
                           style="float:left;margin:0 10px 0 82px;width:60px;height:26px;line-height:26px;border:1px solid #ccc;cursor:pointer;"
                           type="button" value="确定"/>
                    <input style="float:left;margin:0 10px 0 10px;width:60px;height:26px;line-height:26px;border:1px solid #ccc;cursor:pointer;"
                           type="reset" value="清空"/>
                </td>
            </tr>
        </table>
    </form>
    <script>
        $("#gofrom").click(function () {
            var oldpwd = $("#pwdOldCompanyPwd"),
                newpwd = $("#pwdNewCompanyPwd"),
                Confirm = $("#pwdConfirmCompanyPwd"),
                url = "<?php echo site_url();?>/public/tools/modifypwdupdate";
            if (oldpwd.val() == "") {
                oldpwd.css({"border": "1px solid red"});
                return false;
            } else {
                oldpwd.attr("style", "");
            }
            if (newpwd.val() == "") {
                newpwd.css({"border": "1px solid red"});
                return false;
            } else {
                newpwd.attr("style", "");
            }
            if (newpwd.val() != Confirm.val()) {
                Confirm.css({"border": "1px solid red"});
                return false;
            } else {
                Confirm.attr("style", "");
            }
            $.post(url, $("#formModifyCompanyPwd").serialize(), function (data) {
                if (data == 1) {
                    alert("密码修改成功！");
                    window.location.reload();
                } else if (data == 2) {
                    alert("密码修改失败！");
                } else if (data == 3) {
                    alert("新密码和原始密码一样！");
                } else if (data == 4) {
                    alert("原始密码填写错误！");
                }
            });
        });
    </script>
</div>

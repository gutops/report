<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>桌面图标管理</div>
            </div>
            <div class="portlet-body form">
                <form action="" data-method-url='<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>'
                      method="POST"
                      class="data_formDetail form-horizontal" novalidate="novalidate"
                      data-keyid="<?php echo $keyid = isset($keyId) ? $keyId : ''; ?>"
                      editType="<?php echo $editType; ?>" jsonDetail='<?php echo json_encode(isset($datalist)?$datalist:array()); ?>'>
                    <!-- 表单主体 开始  -->
                    <div class="form-body">

                        <!-- 表格样式表单 开始 -->
                        <div class="tableForm">
                            <table class="table table-bordered ">
                                <!-- MODIFY START -->
                                <caption><span class="left">&nbsp;</span>明细信息<span class="right">&nbsp;</span></caption>
                                <!-- MODIFY END -->
                                <tbody>
                                <!-- MODIFY START -->
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">所属项目:</label></td>
                                    <td>
                                        <select class="form-control input-sm" data-validate="{required:'true'}"
                                                name="project_name">
                                            <?php echo bs_type_option($menuinfo,"project_name"); ?>
                                        </select>
                                    </td>
                                    <td><label class="control-label">类型:</label></td>
                                    <td>
                                        <select class="form-control input-sm" data-validate="{required:'true'}"
                                                name="type">
                                            <?php echo  bs_type_option($menuinfo,"type"); ?>
                                        </select>
                                    </td>
                                    <td><label class="control-label">名称:</label></td>
                                    <td>
                                        <input class="form-control" name="name" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">源URL/图片URL:</label></td>
                                    <td>
                                        <input class="form-control" name="src_url" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">目的URL/图片URL:</label></td>
                                    <td>
                                        <input class="form-control" name="des_url" type="text"/>
                                    </td>
                                    <td><label class="control-label">跨域登陆URL:</label></td>
                                    <td>
                                        <input class="form-control" name="login_url" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">背景颜色:</label></td>
                                    <td>
                                        <select class="form-control input-sm" data-validate="{required:'true'}"
                                                name="bg_color" id="bg_color" onchange="docolorchange(this);">
                                            <?php echo bs_type_option($menuinfo,"bg_color"); ?>
                                        </select>
                                    </td>
                                    <td><label class="control-label">等级:</label></td>
                                    <td>
                                        <input class="form-control" name="level" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">按钮:</label></td>
                                    <td>
                                        <input class="form-control" name="btnDefine" type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">列数:</label></td>
                                    <td>
                                        <input class="form-control" name="col_num" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">行数:</label></td>
                                    <td>
                                        <input class="form-control" name="row_num" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">图标路径:</label></td>
                                    <td>
                                        <select class="form-control input-sm" data-validate="{required:'true'}"
                                                name="icon">
                                            <?php echo bs_type_option($menuinfo,"icon"); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">点击出现iframe的X值:</label></td>
                                    <td>
                                        <input class="form-control" name="pos_x" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">点击出现iframe的Y值:</label></td>
                                    <td>
                                        <input class="form-control" name="pos_y" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">点击出现iframe的宽度:</label></td>
                                    <td>
                                        <input class="form-control" name="width" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">点击出现iframe的高度:</label></td>
                                    <td>
                                        <input class="form-control" name="height" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">遮罩层的X值:</label></td>
                                    <td>
                                        <input class="form-control" name="div_pos_x" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">遮罩层的Y值:</label></td>
                                    <td>
                                        <input class="form-control" name="div_pos_y" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">遮罩层的宽度:</label></td>
                                    <td>
                                        <input class="form-control" name="div_width" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">遮罩层的高度:</label></td>
                                    <td>
                                        <input class="form-control" name="div_height" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">能否拖动边框变动大小:</label></td>
                                    <td>
                                        <input class="form-control" name="canresize" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">modulename（权限）:</label></td>
                                    <td><input class="form-control" name="modulename" data-validate="{required:true}"
                                               type="text"/></td>
                                    <td><label class="control-label">controllername（权限）:</label></td>
                                    <td><input class="form-control" name="controllername"
                                               data-validate="{required:true}" type="text"/></td>
                                    <td><label class="control-label">权限组:</label></td>
                                    <td>
                                        <select class="form-control input-sm" data-validate="{required:'true'}"
                                                name="group">
                                            <?php echo bs_type_option($menuinfo,"group"); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td>描述</td>
                                    <td colspan=5>
				            <textarea class="form-control" name="description">
				   		    </textarea>
                                    </td>
                                </tr>
                                <!-- MODIFY END -->
                                </tbody>
                            </table>

                        </div>
                        <!-- 表格样式表单 结束 -->
                    </div>
                    <!-- 表单主体 结束  -->

                    <!-- 表单操作 开始  -->
                    <div class="modal-footer">
                        <a type="button" class="btn default"
                           href="<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>">返回</a>
                        <button type="submit" class="btn blue submitBtn">确认</button>
                    </div>
                    <!-- 表单操作 开始  -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<?php $jscss_load = array('Detail', 'formValidate'); ?>
<?php include APPPATH . '/views/common/loadjs.php'; ?>
<script type="text/javascript">
    Detail().init();
    var cusVali = {};
    formValidate(".data_formDetail").init(cusVali);
    //FormDatePicker.init();

    function docolorchange(obj){
        console.log($(obj).val());
        $(obj).css("background-color",$(obj).val());
        $(obj).css("color","white");
    }

    $(function(){
        docolorchange($("#bg_color"));
    });

</script>

<?php include APPPATH . '/views/common/footer.php'; ?>

</body>
<!-- END BODY -->
</html>


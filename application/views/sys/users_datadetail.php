<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH . 'views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>用户数据</div>
            </div>
            <div class="portlet-body form">
                <form action="" data-method-url='<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>'
                      method="POST"
                      class="data_formDetail form-horizontal" novalidate="novalidate"
                      data-keyid="<?php echo $keyid = isset($keyId) ? $keyId : ''; ?>"
                      editType="<?php echo $editType; ?>"
                      jsonDetail='<?php if (isset($datalist)) echo json_encode($datalist); ?>'>
                    <!-- 表单主体 开始  -->
                    <div class="form-body">

                        <!-- 表格样式表单 开始 -->
                        <div class="tableForm">
                            <table class="table table-bordered ">
                                <!-- MODIFY START -->
                                <caption><span class="left">&nbsp;</span>用户数据<span class="right">&nbsp;</span></caption>
                                <!-- MODIFY END -->
                                <tbody>
                                <!-- MODIFY START -->
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">用户名称:</label></td>
                                    <td>
                                        <input class="form-control" id="username" name="username"
                                               data-validate="{required:true}" data-message="{required:'用户名称不能为空！'}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">用户密码:</label></td>
                                    <td>
                                        <input class="form-control" name="password" data-validate="{required:true}"
                                               type="password"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">用户全名:</label></td>
                                    <td>
                                        <input class="form-control" name="fullname" data-validate="{required:true}"
                                               type="text"/>
                                    </td>
                                    <td><label class="control-label">账号类型:</label></td>
                                    <td>
                                        <select class="form-control input-sm" data-validate="{required:'true'}"
                                                name="accounttype">
                                            <?php echo Ousuclass::all_html_option($dicttype); ?>
                                        </select>
                                    </td>
                                </tr>
                                <!-- MODIFY END -->
                                </tbody>
                            </table>
                        </div>
                        <!-- 表格样式表单 结束 -->
                    </div>
                    <!-- 表单主体 结束  -->

                    <!-- 表单操作 开始  -->
                    <div class="modal-footer">
                        <a type="button" class="btn default"
                           href="<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>">返回</a>
                        <button type="submit" class="btn blue submitBtn">确认</button>
                    </div>
                    <!-- 表单操作 开始  -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<?php $jscss_load = array('Detail', 'formValidate'); ?>
<?php include APPPATH . 'views/common/loadjs.php'; ?>

<script type="text/javascript">

    Detail().init(function (objForm) {
        var regionStr = "<?php if (isset($datalist['region'])) echo $datalist['region']; ?>";
        var regionArr = regionStr.split(',');
        for (var i = 0; i < regionArr.length; i++) {
            var thsInp = $("input[name=\"region[" + regionArr[i] + "]\"]");
            thsInp.attr("checked", true);
            $.uniform.update(thsInp);
        }

    });

    var cusVali = {};

    formValidate(".data_formDetail").init(cusVali);

    $(document).ready(function () {
        var initusername = $("#username").val();
    });


    //FormDatePicker.init();

</script>

<?php include APPPATH . 'views/common/footer.php'; ?>

</body>
<!-- END BODY -->
</html>


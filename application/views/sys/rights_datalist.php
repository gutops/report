<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container" data-method-url='<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>'
     data-btnstatus='<?php echo json_encode($menuinfo['btnStatus']); ?>'>
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <?php echo bs_configsearch($menuinfo,$selectedsearch);?>
        <?php echo bs_configdatalist($datagridinfo,$selectedlistdata);?>
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- MODIFY START -->
                <?php echo bs_breadcrumb("系统管理/权限管理 "); ?>
                <!-- MODIFY END -->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <!-- BEGIN SEARCH AREA-->
        <div id="searchArea" class="row" data-searchdata='<?php echo $searchdata; ?>'>
            <div class="col-md-12">
                <div class="portlet box light-grey scrollnew">
                    <?php echo bs_searchbar($menuinfo['dispbutton']); ?>
                    <?php echo $searchArea; ?>
                </div>
            </div>
        </div>
        <!-- END SEARCH AREA-->
        <!-- BEGIN DATA AREA-->
        <div id="dataArea" class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box light-grey">
                    <?php echo bs_databar(); ?>
                    <?php echo bs_datatable($datagridinfo, $dataList = array()); ?>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            <!-- /.modal -->
            <div class="modal fade" id="basic-modal" tabindex="-1" role="basic" aria-hidden="true">
                <div class="modal-dialog--">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">添加预设权限</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" id="checkSubmitForm"
                                  action="<?php echo site_url() . '/' . $url_module; ?>/check/submit">
                                <div class="alert alert-success">
                                    <strong>信息:</strong>配置预设权限！
                                </div>
                                <input type="hidden" name="id" data-validate="{required:true}"/>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn blue" id="setRoleSubmit">提交</button>
                            <button type="button" class="btn default" data-dismiss="modal">返回</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->


        </div>
        <!-- END DATA AREA-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<?php $jscss_load = array('TableManaged', 'FormDatePicker'); ?>
<?php include APPPATH . '/views/common/loadjs.php'; ?>

<script type="text/javascript">
    !$(document).ready(function () {
        var settings = {
            "bServerSide": true,
            "iDisplayStart":<?php echo $iDisplayStart;?>,
            //服务请求地址
            "sAjaxSource": "<?php echo site_url('/' . $url_module . '/' . $url_model . '/page');?>",
            //须接收的字段，对应各列
            <?php echo bs_tableinit($datagridinfo);?>
        };
        TableManaged('', function (tabObj) {
        }).init(settings);
        FormDatePicker.init();
        btnAddRight();
    });


    function btnAddRight() {
        $("#btnAddRight").unbind();

        $("#btnAddRight").click(function () {
            var keyid = $("#models-data").find(":checkbox:checked");
            $('#basic-modal').find('input[type=hidden][name=id]').val(keyid.val());
            $('#basic-modal').modal('show');
        });
    }
</script>
<?php include APPPATH . '/views/common/footer.php'; ?>
</body>
<!-- END BODY -->
</html>

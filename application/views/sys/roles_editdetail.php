<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH.'/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container" >
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>数据详情</div>
            </div>
            <div class="portlet-body form">
                <form action="" data-method-url='<?php echo $url_module.'/'.$url_model; ?>' method="POST"
                      class="data_formDetail form-horizontal" novalidate="novalidate" data-keyid="<?php echo $keyid = isset($keyId)? $keyId:'';?>"
                      editType="<?php echo $editType; ?>"  >
                    <!-- 表单主体 开始  -->
                    <div class="form-body">
                        <!-- 表格样式表单 开始 -->
                        <div class="tableForm">
                            <table class="table table-bordered ">
                                <!-- MODIFY START -->
                                <caption><span class="left">&nbsp;</span>权限组管理<span class="right">&nbsp;</span></caption>
                                <!-- MODIFY END -->
                                <tbody>
                                <!-- MODIFY START -->
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">权限组名称:</label></td>
                                    <td>
                                        <input class="form-control" name="rolename" data-validate="{required:true}" data-message="{required:'权限组名称不能为空！'}"  type="text" />
                                    </td>

                                    <td ><label class="control-label">备注:</label></td>
                                    <td>
                                        <input class="form-control" name="remarks" data-validate="{required:true}"   type="text" />
                                    </td>
                                </tr>
                                <!-- MODIFY END -->
                                </tbody>
                            </table>

                        </div>
                        <!-- 表格样式表单 结束 -->
                    </div>
                    <!-- 表单主体 结束  -->

                    <!-- 表单操作 开始  -->
                    <div class="modal-footer">
                        <a type="button" class="btn default" href="<?php echo base_url().$url_module.'/'.$url_model; ?>">返回</a>
                        <button type="submit" class="btn blue submitBtn">确认</button>
                    </div>
                    <!-- 表单操作 开始  -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<?php $jscss_load = array('Detail', 'formValidate'); ?>
<?php include APPPATH.'/views/common/loadjs.php'; ?>
<script type="text/javascript">
    Detail().init();
    var cusVali = {};
    formValidate(".data_formDetail").init(cusVali);
    //FormDatePicker.init();
</script>
<?php include APPPATH.'/views/common/footer.php'; ?>
</body>
<!-- END BODY -->
</html>


<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>数据详情</div>
            </div>
            <div class="portlet-body form">
                <form action=""
                      data-method-url='<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>'
                      method="POST"
                      accept-charset=""
                      class="data_formDetail form-horizontal"
                      novalidate="novalidate"
                      data-keyid="<?php echo $keyid = isset($keyId) ? $keyId : ''; ?>"
                      accesskey=""
                      editType="<?php echo $editType; ?>"
                      jsonDetail='<?php echo json_encode($datalist); ?>'>
                    <!-- 表单主体 开始  -->
                    <div class="form-body">
                        <!-- 表格样式表单 开始 -->
                        <div class="tableForm">
                            <table class="table table-bordered ">
                                <!-- MODIFY START -->
                                <caption><span class="left">&nbsp;</span>企业帐号信息<span class="right">&nbsp;</span>
                                </caption>
                                <!-- MODIFY END -->
                                <tbody>
                                <!-- MODIFY START -->

                                <!--  输入样例，开始   -->
                                <tr class="gradeX gradeY">
                                    <!--  标准输入框，开始   -->
                                    <td><label class="control-label">企业名称:</label></td>
                                    <td colspan="1">  <!-- 跨行 -->
                                        <input class="form-control" data-disable name="COMPANY_NAME"
                                               data-disable    <!-- 禁止输入 -->
                                        data-validate="{required:true}"  <!-- 输入校验 -->
                                        <!--		可用以下方式  retuired:true,email:true
                                                        required : "必选字段",
                                                        remote : "请修正该字段",
                                                email : "请输入正确格式的电子邮件",
                                                url : "请输入合法的网址",
                                                date : "请输入合法的日期",
                                                dateISO : "请输入合法的日期 (ISO).",
                                                number : "请输入合法的数字",
                                                digits : "只能输入整数",
                                                creditcard : "请输入合法的信用卡号",
                                                equalTo : "请再次输入相同的值",
                                                accept : "请输入拥有合法后缀名的字符串",
                                                maxlength : jQuery.validator.format("请输入一个长度最多是 {0} 的字符串"),
                                                minlength : jQuery.validator.format("请输入一个长度最少是 {0} 的字符串"),
                                                rangelength : jQuery.validator.format("请输入一个长度介于 {0} 和 {1} 之间的字符串"),
                                                range : jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
                                                max : jQuery.validator.format("请输入一个最大为 {0} 的值"),
                                                min : jQuery.validator.format("请输入一个最小为 {0} 的值")
                                        -->
                                        data-message="{required:'帐号名不能为空！'}"   <!-- 提醒信息 -->
                                        type="text" />
                                    </td>
                                    <!--  标准输入框，结束   -->
                                    <!--  标准下拉框，开始 ID,NAME   -->
                                    <td><label class="control-label">企业帐号类型:</label></td>
                                    <td>
                                        <select class="form-control input-sm" data-disable
                                                data-validate="{required:'true'}" name="ACCOUNTTYPE">
                                            <?php echo Ousuclass::all_html_option($accounttype); ?>
                                        </select>
                                    </td>
                                    <!--  标准下拉框，结束 ID,NAME   -->
                                    <!--  标准只读显示，开始   -->
                                    <td class="lightgrey"><label class="control-label">重置密码</label></td>
                                    <!--  标准只读显示，结束   -->
                                </tr>
                                <!--  输入样例，结束   -->
                                <!--  时间输入，开始   -->
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">预约时间:</label></td>
                                    <td>
                                        <div class="input-group input-medium date datetime-picker ">
                                            <input class="form-control" name="PLAN_TIME" type="text"/>
                                            <span class="input-group-btn">
                                                    <button id="examinationDatetimeBtn" class="btn default date-set"
                                                            type="button"><i class="icon-calendar"></i></button>
                                                    </span></div>
                                    </td>
                                </tr>
                                <!--  时间输入，结束   -->
                                <!--  级联控制，开始   -->
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">企业类型:</label></td>
                                    <td>
                                        <select class="form-control masterlink" data-sub="value,PID"
                                                data-validate="{required:'true'}" name="LEVEL" id="LEVEL">
                                            <?php echo Ousuclass::all_html_option($leveltype, 'value', 'name'); ?>
                                        </select>
                                    </td>
                                    <td><label class="control-label">父节点名称:</label></td>
                                    <td>
                                        <select class="form-control" name="PID" id="PID">
                                            <?php echo Ousuclass::all_html_option($pid); ?>
                                        </select>
                                    </td>
                                </tr>
                                <!--  级联控制，结束   -->
                                <!--  UEDIT，开始   -->
                                <tr class="gradeX gradeY lightgrey">
                                    <td colspan="6"><label class="control-label">内容:</label></td>
                                </tr>
                                <tr style="height:550px;">
                                    <td colspan="6">
                                        <script type="text/plain" id="ueditor" name="content"></script>
                                    </td>
                                </tr>
                                <!--  UEDIT，结束   -->

                                <!-- MODIFY END -->
                                </tbody>
                            </table>

                        </div>
                        <!-- 表格样式表单 结束 -->
                    </div>
                    <!-- 表单主体 结束  -->

                    <!-- 表单操作 开始  -->
                    <div class="modal-footer">
                        <a type="button" class="btn default"
                           href="<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>">返回</a>
                        <button type="submit" class="btn blue submitBtn">确认</button>
                    </div>
                    <!-- 表单操作 开始  -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- modal start 弹出页面样例--------------------------------------------->
<div class="modal fade" id="detail_form" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">领用物品详细信息</h4>
        </div>
        <div class="modal-body">
            <form action="" id="refuseForm" method="post">
                <div class="tableForm">
                    <table class="table table-bordered ">
                        <caption><span class="left"></span>物品信息<span class="right"></span></caption>
                        <tbody id='tbody'>
                        <tr class="gradeX gradeY">
                            <td><label for="checkcause">物品大类</label></td>
                            <td>
                                <select class="form-control masterlink" data-sub="value,goodssubtype" name="goodstype"
                                        id='goodstype'>  <!--级联定义 , 主-->
                                    <?php echo Ousuclass::all_html_option($listgoodstype); ?>
                                </select>
                            </td>
                        </tr>
                        <tr class="gradeX gradeY">
                            <td><label for="checkcause">物品分类</label></td>
                            <td>
                                <select class="form-control" data-validate="{required:true}" name="goodssubtype"
                                        id="goodssubtype"> <!--级联定义 , 从-->
                                    <?php echo Ousuclass::all_html_option($listgoodssubtype); ?>
                                </select>
                            </td>
                        </tr>
                        <tr class="gradeX gradeY">
                            <td><label for="checkcause">物品名称</label></td>
                            <td>
                                <input type="text" class="form-control" name="goodsname" id="goodsname" placeholder="">
                            </td>
                        </tr>
                        <tr class="gradeX gradeY">
                            <td><label for="checkcause">备注</label></td>
                            <td>
                                <textarea name="remark" id="remark" class="form-control"
                                          style="width:100%;height:100%;"></textarea>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-actions right">
                    <button type="button" data-dismiss="modal" class="btn default">关闭</button>
                    <button id="submit_form" type="button" class="btn green">提交</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- modal end ------------------------------------------------->

<!--- 多tab样例 start -->
<div class="tabbable-custom nav-justified">
    <ul class="nav nav-tabs nav-justified">
        <li class="active"><a href="#project_area_base" data-toggle="tab">订单明细</a></li>
        <li class=""><a href="#project_area_detail" data-toggle="tab">详细清单</a></li>
        <li class=""><a href="#project_area_service" data-toggle="tab">实际服务详情</a></li>
        <li class=""><a href="#project_area_log" data-toggle="tab">订单日志</a></li>
        <li class=""><a href="#project_area_message" data-toggle="tab">反馈处理</a></li>
    </ul>
    <div class="tab-content">
        <div id="project_area_base" class="tab-pane p-app-2 active ">
            <ul class="clearfix">
                <h>订单明细</h>
            </ul>
        </div>
        <div id="project_area_detail" class="tab-pane p-app-2 ">
            <ul class="clearfix">
                <h>详细清单</h>
            </ul>
        </div>
        <div id="project_area_service" class="tab-pane p-app-2 ">
            <ul class="clearfix">
                <h>实际服务详情</h>
            </ul>
        </div>
        <div id="project_area_log" class="tab-pane p-app-2  ">
            <ul class="clearfix">
                <h>订单日志</h>
            </ul>
        </div>
        <div id="project_area_message" class="tab-pane p-app-2  ">
            <ul class="clearfix">
                <h>反馈处理</h>
            </ul>
        </div>
    </div>
</div>
<!--- 多tab样例 end -->
<!--  定义需要加载的js和样式表文件 -->
<?php $jscss_load = array('Cascade', 'Detail', 'formValidate', 'FormDatePicker', 'TableManaged',
    'TableEditable', 'dialog_search', 'flexselect', 'ueditor'); ?>
<?php include APPPATH . '/views/common/loadjs.php'; ?>

<script type="text/javascript">

    Cascade('masterlink', "<?php echo site_url() . '/' . $url_module . '/' . $url_model;?>/cascade"); // 级联控制

    Detail().init(function (obj) {
        // 明细页面初始化后进行的动作
    });

    var cusVali = {
        errorElement: 'span', // default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        invalidHandler: function (event, validator) { // display error
            return false;
        },
        highlight: function (element) { // hightlight error inputs
            $(element).addClass('has-error').css('border', '1px solid red'); // set error class to the
        },
        unhighlight: function (element) { // revert the change done by
            $(element).removeClass('has-error').css('border', 'none'); // set
        },
        success: function (label) {
        },
        errorPlacement: function (error, element) {
        },
        submitHandler: function (formObj) {
            ajaxSubmitAndMsg.init(formObj, true);
        }
    };

    // 禁止重复提交 submit button id="saveorder"
    var cusVali = {
        submitHandler: function (formObj) {
            $("#saveorder").attr("disabled", "true");
            ajaxSubmitAndMsg.init(formObj, true, '', function () {
                $("#saveorder").removeAttr("disabled");
            });
        }
    };

    formValidate(".data_formDetail").init(cusVali);

    FormDatePicker.init();
    FormDatetimePicker.init();
    FormDatehourPicker.init();
    FormDatetimePickerIn.init();
    var editor = UE.getEditor('ueditor', {
        minFrameHeight: 300
        , initialFrameWidth: '100%'
        , initialFrameHeight: 470
        , autoHeightEnabled: true
    });
</script>

// 联系电话(手机/电话皆可)验证
jQuery.validator.addMethod("isnumber30", function(value,element) {
var number = new Array(1,2,3,4,5,6,7,8,9,10,11);
return this.optional(element) || $.inArray(value,number);
}, "请输入1-30");

<?php include APPPATH . '/views/common/footer.php'; ?>

</body>
<!-- END BODY -->
</html>


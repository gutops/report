<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/excanvas.min.js"></script>
<script src="assets/plugins/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>

<!-- 消息类 START -->
<link href="<?php echo base_url(); ?>/assets/plugins/bootstrap-messenger/css/messenger.css" rel="stylesheet"
      type="text/css"/>
<link href="<?php echo base_url(); ?>/assets/plugins/bootstrap-messenger/css/messenger-theme-future.css"
      rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-messenger/js/messenger.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-messenger/js/messenger-theme-future.js"
        type="text/javascript"></script>
<!-- 消息类 END -->
<!-- END CORE PLUGINS -->

<script type="text/javascript">
    function showMsg(data) {
        $.globalMessenger().post({
            message: data['msg'],
            type: data['success'] ? 'info' : 'error',
            showCloseButton: true,
            hideAfter: 3
        });
    }
</script>
<!-- END JAVASCRIPTS -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/plugins/excanvas.min.js"></script>
<script src="assets/plugins/respond.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery.form.js" type="text/javascript"></script>
<link href="<?php echo base_url(); ?>/assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet"
      type="text/css"/>
<link href="<?php echo base_url(); ?>/assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"
        type="text/javascript"></script>
<!-- 消息类 START -->
<link href="<?php echo base_url(); ?>/assets/plugins/bootstrap-messenger/css/messenger.css" rel="stylesheet"
      type="text/css"/>
<link href="<?php echo base_url(); ?>/assets/plugins/bootstrap-messenger/css/messenger-theme-future.css"
      rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-messenger/js/messenger.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/bootstrap-messenger/js/messenger-theme-future.js"
        type="text/javascript"></script>
<!-- 消息类 END -->
<!-- END CORE PLUGINS -->

<?php if (in_array('TableManaged', $jscss_load)): ?>
    <!-- data table start -->
    <script src="<?php echo base_url(); ?>/assets/plugins/data-tables/jquery.dataTables.min.js"
            type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>/assets/plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script>
    <!-- data table end -->
<?php endif; ?>

<?php if (in_array('formValidate', $jscss_load)): ?>
    <!-- data table sta
    <!-- 验证库 START -->
    <script type="text/javascript"
            src="<?php echo base_url(); ?>/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url(); ?>/assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <!-- 验证库 END -->
<?php endif; ?>

<?php if (in_array('FormDatePicker', $jscss_load)): ?>
    <!-- data table sta
    <!-- 时间空间 START -->
    <link href="<?php echo base_url(); ?>/assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet"
          type="text/css"/>
    <script type="text/javascript"
            src="<?php echo base_url(); ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <link href="<?php echo base_url(); ?>/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"
          rel="stylesheet" type="text/css"/>
    <script type="text/javascript"
            src="<?php echo base_url(); ?>/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
    <!-- 时间空间 END -->
<?php endif; ?>

<?php if (in_array('flexselect', $jscss_load)): ?>
    <!-- data table sta
    <!-- flexselect类 START -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/scripts/jquery-flexselect-master/flexselect.css"
          type="text/css" media="screen"/>
    <script src="<?php echo base_url(); ?>/assets/scripts/jquery-flexselect-master/liquidmetal.js"></script>
    <script src="<?php echo base_url(); ?>/assets/scripts/jquery-flexselect-master/jquery.flexselect.js"></script>
    <!-- flexselect单类 END -->
<?php endif; ?>

<?php if (in_array('ueditor', $jscss_load)): ?>
    <!-- data table sta
    <!-- 编辑类 START -->
    <script language="javascript" type="text/javascript"
            src="<?php echo base_url(); ?>ueditor/ueditor.config.js"></script>
    <script language="javascript" type="text/javascript"
            src="<?php echo base_url(); ?>ueditor/ueditor.all.min.js"></script>
    <script language="javascript" type="text/javascript"
            src="<?php echo base_url(); ?>ueditor/lang/zh-cn/zh-cn.js"></script>
    <!-- 编辑类 END -->
<?php endif; ?>

<script type="text/javascript">
    var g_base_url = "<?php echo base_url();?>";
    var g_site_url = "<?php echo site_url();?>";
</script>
<script src="<?php echo base_url(); ?>/assets/scripts/app.js"></script>
<script src="<?php echo base_url(); ?>/assets/scripts/page-define.js"></script>
<script src="<?php echo base_url(); ?>/assets/scripts/scrollnew.js"></script>


<script type="text/javascript">
    function dosearchconfig() {
        $('#form-config-search').ajaxSubmit({
            url: "<?php echo site_url() . $url_module . '/' . $url_model . '/saveSearchDefine';?>",//默认是form action
            success: function (data) {
                console.log(data);
                window.location.reload();
            }
        });
    };
    function dolistdataconfig() {
        $('#form-config-listdata').ajaxSubmit({
            url: "<?php echo site_url() . $url_module . '/' . $url_model . '/saveListdataDefine';?>",//默认是form action
            success: function (data) {
                console.log(data);
                window.location.reload();
            }
        });
    };
    function showMsg(data) {
        $.globalMessenger().post({
            message: data['msg'],
            type: data['success'] ? 'info' : 'error',
            showCloseButton: true,
            hideAfter: 3
        });
    }

    function createBtn(selecter) {
        var btns = $(selecter);
        var baseUrl = $("base").attr("href") || '';
        btns.each(function () {
            var options = {};
            options.href = $(this).attr('href');
            options.button = "#" + $(this).attr('id');
            if ($(this).hasClass("os_check")) {
                options.before = true;
            }
            //判断是否是绝对路径
            if (options.href.indexOf('/') !== 0 && options.href.indexOf('http') !== 0) {
                options.href = baseUrl + options.href;
            }
            $(this).removeAttr('href');
            BtnMaker.factory(options);
        });
    }

    <?php if (in_array('Cascade', $jscss_load)):?>
    //级联
    function Cascade(obj, url) {
        $("." + obj).on('change', function () {
            var master = $('.' + obj).attr("data-sub"),
                val = $(this).val();
            if (master) {
                cascadecomm(master, val, url);
            }
        });
    }
    //初始化
    function initialise(obj) {
        var val = $('.' + obj).val();
        if (val) {
            var master = $('.' + obj).attr("data-sub");
            if (master) {
                cascadecomm(master, val, '');
            }
        }
    }
    //初始化和级联的公共调用
    function cascadecomm(master, val, url) {
        var strs = new Array(),
            strs = master.split(",");
        if (strs['0']) {
            var type = strs['0'],
                name = strs['1'],
                params = strs['2'],    // 附件传送参数的ID
                loadname = $("form").find("[name='" + name + "']"),
                val1 = loadname.val(),
                url1 = "<?php echo site_url()?>/public/tools/cascade";
            if (!url) {
                url = url1;
            }
            if (!val) loadname.html('<option></option>');
            else if (params && params.length > 0) {
                params = $("#" + params).val();
            }
            loadname.load(url, {"type": type, "value": val, "params": params}, function () {
                if (val1) {
                    loadname.val(val1);
                }
            });
        }
    }
    <?php endif;?>

    <?php if (in_array('TableManaged', $jscss_load)):?>
    //表格的初始化调用
    var TableManaged = function (selecter, callback, type) {
        if (selecter === '' || typeof(selecter) !== 'string') {
            selecter = "#models-data";
        }
        var tabObj = $(selecter);
        if (typeof callback === 'function') {
            callback(tabObj);
        }
        return {
            init: function (settings) {
                if (!jQuery().dataTable) {
                    return;
                }
                if (settings === "" && typeof(settings) !== 'object') {
                    var settings = {};
                }
                var defaults = {
                    'bFilter': true //搜索栏
                    //'sScrollX': '100%'
                    //,'sScrollXInner': '98%'
                    //,'sScrollY': '410px'
                    //,'bAutoWidth': false //自动宽度
                    //,'bPaginate': false //分页功能
                    //,'bInfo': false //总数信息
                    //,'bLengthChange': false //是否允许用户通过一个下拉列表来选择分页后每页的行数
                    //,'bSort': true //排序栏
                    //,'bInfo': false //页脚功能
                    , 'bStateSave': false //保存状态到cookie
                    , 'iDisplayLength': 20 //每页的行数，每页默认数量:10，-1表示全部
                    , 'aLengthMenu': [[5, 10, 15, 20, 50, 100, -1], [5, 10, 15, 20, 50, 100, '全部']] //定义每页显示数据数量
                    , 'sPaginationType': 'bootstrap' //分页，一共两种样式。另一种为two_button(是datatables默认bootstrap)
                    , 'bProcessing' : true
                    , 'oLanguage': { //国际化
                        'sLengthMenu': '每页显示 _MENU_ 条记录'
                        , 'sZeroRecords': '抱歉， 没有数据'
                        , 'sInfo': '从 _START_ 到 _END_ /共 _TOTAL_ 条数据'
                        , 'sInfoEmpty': '显示第 0 至 0 项结果，共 0 项'
                        , 'sInfoFiltered': '(从 _MAX_ 条数据中检索)'
                        , 'sSearch': '筛选:'
                        , 'sProcessing': '<img src="<?php echo base_url();?>/assets/img/ajax-loading.gif" />'
                        , 'oPaginate': {
                            'sFirst': '首页'
                            , 'sPrevious': '上页'
                            , 'sNext': '下页'
                            , 'sLast': '尾页'
                        }
                    }
                    , "aoColumnDefs": [{"bSortable": false, "aTargets": [0]}]
                    , "fnCreatedRow": function (nRow, aData, iDataIndex) {
                        var uuid = $(nRow).attr("id");
                        $('td:first', nRow).html('<input type="checkbox" class="checkboxes" value="' + uuid + '" />');
                        $('td:first', nRow).attr('style', 'width:8px;');
                        // 根据interface定义重新设定列表元素
                        <?php if(isset($datagridinfo["listfield"])):?>
                        <?php $i = 0;foreach($datagridinfo["listfield"] as $key => $value):?>
                        <?php if (isset($value['type']) && ($value['type'] == 'checkbox')) :?>
                        var fieldname = '<?php echo $value['fieldname'];?>';
                        if (aData[fieldname] === 1) {
                            $('td:eq(<?php echo $i + 1;?>)', nRow).html('<div class="checker disabled"><span class="checked"><input type="checkbox" disabled /></span></div>');
                        }
                        else {
                            $('td:eq(<?php echo $i + 1;?>)', nRow).html('<div class="checker disabled"><span><input type="checkbox" disabled /></span></div>');
                        }
                        <?php endif;?>
                        <?php $i++;endforeach;?>
                        <?php endif;?>
                        if (BtnToggleEffect) {
                            var fieldstatus = BtnToggleEffect.jsonInitConf['fieldstatus'];
                            if (fieldstatus !== undefined && fieldstatus !== "") {
                                $(nRow).click(function () {
                                    btnRight(fieldstatus, aData);
                                });
                            }
                        }
                    }
                    , "fnDrawCallback": function () {
                        checkbox();
                    }
                    , "fnInitComplete": function () {
                        this.fnAdjustColumnSizing(true);
                    }
                };
                var settings = $.extend(defaults, settings);
                // begin datatable
                tabObj.dataTable(settings);
                $(selecter + "_wrapper .dataTables_filter input").addClass("form-control input-medium"); // modify table search input
                $(selecter + "wrapper .dataTables_length select").addClass("form-control input-small"); // modify table per page dropdown
                $(selecter + "_wrapper .dataTables_length select").select2({
                    showSearchInput: false //hide search box with special css class
                }); // initialize select2 dropdown }
            }
        };
        function checkbox() {
            var checkboxes = tabObj.find('tbody tr .checkboxes');
            //具体某一项前面的checkbox，只能选择某一项，不能全选
            checkboxes.uniform();  //jQuery.uniform更新数据
            checkboxes.click(function () {
                checkboxes.parents('tr').removeClass('active'); //移除所有tr的active class
                checkboxes.not($(this)).attr("checked", false); //将除$(this)之外所有的.checkboxes取消选择
                $(this).attr("checked") ? $(this).parents('tr').addClass('active') : $(this).parents('tr').removeClass('active');
                $.uniform.update(checkboxes);  //jQuery.uniform更新数据
                //按钮控制
                if (tabObj.find('tbody tr .checkboxes:checked').length > 0) {
                    if (BtnToggleEffect) {
                        BtnToggleEffect.btnAssignEnable('datacheck');
                    }
                    if ($("#success_modal").attr("disabled") === "disabled") {
                        $("#success_modal").removeAttr("disabled");
                        $("#modal_uuid").val($(this).parents('tr').attr("id"));
                    }
                } else {
                    $("#success_modal").attr("disabled", "disabled");
                    BtnToggleEffect.btnAssignDisable('datacheck');
                }
            });
        }

        function btnRight(fieldstatus, aData) {
            $.each(fieldstatus, function (k, v) {
                //按钮默认亮
                var status = true;
                for (var i in v) {
                    var valList = v[i].split(',');
                    for (var ind in valList) {
                        valList[ind] = valList[ind].trim();
                    }
                    var isIn = $.inArray(aData[i], valList);
                    if (isIn === -1) {
                        status = false;
                        break;
                    }
                }
                if (status) {
                    //按钮控制
                    if (tabObj.find('.checkboxes:checked').length > 0) {
                        BtnToggleEffect.btnEnable(k);
                    } else {
                        BtnToggleEffect.btnDisable(k);
                    }
                } else {
                    BtnToggleEffect.btnDisable(k);
                }
            });
        }
    };
    <?php endif;?>

    <?php if (in_array('TableEditable', $jscss_load)):?>
    //表内编辑
    var TableEditable = function () {
        return {
            //main function to initiate the module
            init: function (table_id, htmlarr, urlarr, hidden, czan) {
                if (czan.length !== 4) {
                    alert("缺失按钮ID");
                    return false;
                }
                $("#" + czan[3]).attr("disabled", "disabled");
                $("#" + czan[1]).attr("disabled", "disabled");
                $("#" + czan[2]).attr("disabled", "disabled");
                var i = 0, count = htmlarr.length, checkboxes = $(table_id).find('tbody tr .checkboxes');

                function restoreRow(oTable, nRow, uuid) {
                    var aData = oTable.fnGetData(nRow), jqTds = $('>td', nRow);
                    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                        if (i === 0) {
                            oTable.fnUpdate('<input name="uuid" class="checkboxes" value="' + uuid + '" type="checkbox">', nRow, i, false);
                        } else {
                            oTable.fnUpdate(aData[i], nRow, i, false);
                        }
                    }
                    oTable.fnDraw();
                }

                function editRow(oTable, nRow, uuid, obj) {
                    var aData = oTable.fnGetData(nRow), jqTds = $('>td', nRow);
                    $("#" + czan[3]).removeAttr("disabled");
                    if (uuid) {
                        jqTds[0].innerHTML = "<input type='checkbox' name='uuid' class='checkboxes' value='" + uuid + "' />";
                        for (var i = 1; i < count; i++) {
                            jqTds[i].innerHTML = htmlarr[i];
                            var tag = jqTds[i].childNodes[0];
                            if (tag.tagName === "INPUT") {
                                tag.value = aData[i];
                            }
                            if (tag.tagName === "SELECT") {
                                $(tag.options).each(function () {
                                    if ($(this).text() === aData[i]) {
                                        $(this).attr('selected', true);
                                        return false;
                                    }
                                });
                            }
                            if (tag.tagName === "DIV") {
                                $(jqTds[i]).find("input[type=text]").val("" + aData[i] + "");
                            }
                        }
                        $("#" + czan[0] + ",#" + czan[2]).attr("disabled", "disabled");
                    } else {
                        jqTds[0].innerHTML = "<input type='checkbox' name='uuid' style='display:none;'  class='checkboxes' value='' />";
                        for (var i = 1; i < count; i++) {
                            jqTds[i].innerHTML = htmlarr[i];
                        }
                        $("#" + czan[1] + ",#" + czan[2]).attr("disabled", "disabled");
                    }
                    var checkboxes = $(table_id).find('tbody tr .checkboxes');
                    checkboxes.uniform();  //jQuery.uniform更新数据
                    FormDatePicker.init();
                }

                //后台操作完毕之后  改变TABLE的内容
                function saveRow(oTable, nRow, tag, html, uuid) {
                    var jqInputs = $('>td', nRow);
                    $(table_id).find('tbody tr td').attr("style", "height:30px;line-height:30px;");
                    oTable.fnUpdate(html, nRow, 0, false);
                    if (uuid) {
                        $(jqInputs.get(0)).parent().attr("id", uuid);
                    }
                    for (i = 1; i < count; i++) {
                        //如果是SELECT的时候获取的是 它的 文本值 而不是 VALUE值
                        //alert(jqInputs.get(i).childNodes[0].tagName);
                        if (jqInputs.get(i).childNodes[0].tagName === "SELECT") {
                            var options = jqInputs.get(i).childNodes[0].options,
                                opt = "";
                            for (var y = 0, len = options.length; y < len; y++) {
                                opt = options[y];
                                if (opt.value === tag[i]) {
                                    oTable.fnUpdate(opt.text, nRow, i, false);
                                    break;
                                }
                            }
                        } else {
                            oTable.fnUpdate(tag[i], nRow, i, false);
                        }
                    }
                    oTable.fnDraw();
                    var checkboxes = $(table_id).find('tbody tr .checkboxes');
                    checkboxes.uniform();  //jQuery.uniform更新数据
                }

                var oTable = $(table_id).dataTable({
                    'bFilter': false //搜索栏
                    , 'bSort': false //排序栏
                    , 'bLengthChange': false //是否允许用户通过一个下拉列表来选择分页后每页的行数
                    // ,"iDisplayLength": 5,
                    // ,"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
                    , "sPaginationType": "bootstrap"
                    , 'oLanguage': { //国际化
                        'sLengthMenu': '每页显示 _MENU_ 条记录'
                        , 'sZeroRecords': '抱歉， 没有数据'
                        , 'sInfo': '从 _START_ 到 _END_ /共 _TOTAL_ 条数据'
                        , 'sInfoEmpty': '无数据'
                        , 'sInfoFiltered': '(从 _MAX_ 条数据中检索)'
                        , 'sSearch': '筛选:'
                        , 'sProcessing': '<img src="<?php echo base_url();?>/assets/img/loading.gif" />'
                        , 'oPaginate': {
                            'sFirst': '首页'
                            , 'sPrevious': ''
                            , 'sNext': ''
                            , 'sLast': '尾页'
                        }
                    },
                    "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [0]
                    }]
                });
                jQuery(table_id + '_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
                jQuery(table_id + '_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
                jQuery(table_id + '_wrapper .dataTables_length select').select2({
                    showSearchInput: false //hide search box with special css class
                }); // initialize select2 dropdown
                var nEditing = null;
                //添加行
                $("#" + czan[0]).click(function (e) {
                    var cz = $("#" + czan[0]);
                    e.preventDefault();
                    if (cz.attr("cexiao") === 1) {
                        oTable.fnDeleteRow(nEditing);
                        cz.html("添加<i class='icon-plus'></i>");
                        cz.attr("cexiao", "2");
                        nEditing = null;
                        return false;
                    }
                    if (obj) {
                        obj.removeClass('active');
                        obj = uuid = myupdaterow = "";
                    }
                    var _TheArray = new Array();   //先声明一维
                    for (i = 0; i <= count; i++) {
                        _TheArray[i] = "";
                    }
                    var aiNew = oTable.fnAddData(_TheArray), nRow = oTable.fnGetNodes(aiNew['0']);
                    editRow(oTable, nRow);
                    var nRow1 = oTable.fnGetNodes();
                    nEditing = nRow;
                    cz.html("撤消<i class='icon-plus'></i>");
                    cz.attr("cexiao", "1");
                });
                //操作数据 添加和修改
                $("#" + czan[3]).click(function (e) {
                    e.preventDefault();
                    var post = "",
                        post1 = "",
                        key = "",
                        val = "",
                        url = "",
                        len = 0,
                        tag = new Array(),
                        tagname = "",
                        dom1 = $('>td', nEditing),
                        dom = $('>td  input,select', nEditing);
                    for (i = 1; i < count; i++) {
                        val = dom.eq(i).val();
                        if (val === "undefined" || !val) {
                            tagname = dom1.get(i).childNodes[0];
                            if (tagname.tagName === "SELECT") {
                                //val=$('>td select', nEditing).val();
                                val = $(tagname).val();
                            }
                        }
                        key = dom.eq(i).attr("name");
                        if (key === "undefined" || !key) {
                            if (tagname.tagName === "SELECT") {
                                key = $(tagname).attr("name");
                            }
                        }
                        if (hidden[0] && len < 1) {
                            len = hidden.length;
                            //拼接隐藏域参数
                            for (var j = 0; j < len; j++) {
                                if ($("#" + hidden[j]).val() || $("#" + hidden[j]).val() !== "undefined") {
                                    post1 += hidden[j] + "=" + $("#" + hidden[j]).val() + "&";
                                }
                            }
                            len = 1;
                        }
                        //拼接表单参数
                        //if(val && key ){
                        post += '' + key + '' + "=" + val + "&";
                        tag[i] = val;
                        //}
                    }
                    if (post) {
                        //当有隐藏域值 和 参数值
                        if (post.length > 0 && post1.length > 0) {
                            post = post.substring(0, post.length - 1);
                            post = post1 + post;
                        } else if (post.length > 0 && post1.length < 1) {//当有 参数值没有隐藏域值
                            post = post.substring(0, post.length - 1);
                        } else if (post.length < 1 && post1.length > 0) {//当有隐藏域值 没有参数值
                            post = post1.substring(0, post1.length - 1);
                        }
                        //拼接添加URL
                        if (!uuid) {
                            url = urlarr[0] + "?" + post;
                        } else {
                            if (urlarr[1]) {
                                //拼接修改URL
                                url = urlarr[1] + "?uuid=" + uuid + "&" + post;
                            } else {
                                alert("抱歉您的修改URL没有！");
                                return false;
                            }
                        }
                        $.post(url, {}, function (data) {
                            $("#" + czan[2] + ",#" + czan[3]).attr("disabled", "disabled");
                            $("#" + czan[0]).removeAttr("disabled").html("添加<i class='icon-plus'></i>").attr("cexiao", "2");
                            $("#" + czan[1]).attr("disabled", "disabled").html("修改<i class='icon-plus'></i>").attr("cexiao", "2");
                            if (obj) {
                                obj.removeClass('active');
                            }
                            if (data !== -1) {
                                var inputcheckbox = "<input type='checkbox' class='checkboxes' name='uuid' value='" + data + "' />";
                                saveRow(oTable, nEditing, tag, inputcheckbox, data);
                                obj = myupdaterow = "";
                                nEditing = null;
                                return true;
                            }
                            obj = myupdaterow = "";
                            nEditing = null;
                            alert("操作失败!");
                            return false;
                        });
                    }
                });
                //.childNodes[0]
                //按钮控制  选择数据 是哪一条数据需要 操作
                var obj = "", uuid = "", myupdaterow = "", ischeck = 1;
                if (ischeck === 1) {
                    checkboxes.live("click", function (e) {
                        $("#" + czan[3] + ",#" + czan[0]).attr("disabled", "disabled");
                        $("#" + czan[1] + ",#" + czan[2]).removeAttr("disabled");
                        obj = $(this).parents('tr');
                        myupdaterow = $(this).parents('tr')[0];
                        uuid = obj.attr('id');
                        checkboxes.parents('tr').removeClass('active'); //移除所有tr的active class
                        checkboxes.not($(this)).attr("checked", false); //将除$(this)之外所有的.checkboxes取消选择
                        if ($(this).attr("checked")) {
                            $(this).parents('tr').addClass('active');
                        } else {
                            $(this).parents('tr').removeClass('active');
                            $("#" + czan[2] + ",#" + czan[1]).attr("disabled", "disabled");
                            $("#" + czan[0]).removeAttr("disabled");
                        }
                        $.uniform.update(checkboxes);  //jQuery.uniform更新数据
                        ischeck = 2;
                    });
                }
                //点击修改按钮之后
                $("#" + czan[1]).click(function (e) {
                    var cz = $("#" + czan[1]);
                    if (cz.attr("cexiao") === 1) {
                        e.preventDefault();
                        restoreRow(oTable, nEditing, uuid);
                        $("#" + czan[2] + ",#" + czan[1] + ",#" + czan[3]).attr("disabled", "disabled");
                        $("#" + czan[0]).removeAttr("disabled");
                        var checkboxes = $(table_id).find('tbody tr .checkboxes');
                        checkboxes.parents('tr').removeClass('active'); //移除所有tr的active class
                        checkboxes.uniform();  //jQuery.uniform更新数据
                        cz.html("修改<i class='icon-plus'></i>");
                        cz.attr("cexiao", "2");
                        nEditing = null;
                        ischeck = 1;
                        return false;
                    }
                    if (uuid) {
                        restoreRow(oTable, myupdaterow, uuid);
                        editRow(oTable, myupdaterow, uuid, obj);
                        nEditing = myupdaterow;
                        cz.html("撤消<i class='icon-plus'></i>");
                        cz.attr("cexiao", "1");
                        e.preventDefault();
                    } else {
                        alert("编号丢失！");
                        return false;
                    }
                });
                //数据删除操作
                $("#" + czan[2]).click(function (e) {
                    e.preventDefault();
                    if (uuid) {
                        if (confirm("您是否要删除此条数据？") === false) {
                            return;
                        }
                        if (urlarr[2]) {
                            $.post(urlarr[2], {"uuid": uuid}, function (data) {
                                if (data !== -1) {
                                    var nRow = myupdaterow;
                                    oTable.fnDeleteRow(nRow);
                                    $("#" + czan[3]).attr("disabled", "disabled");
                                    $("#" + czan[1]).attr("disabled", "disabled");
                                    $("#" + czan[2]).attr("disabled", "disabled");
                                    $("#" + czan[0]).removeAttr("disabled");
                                }
                            });
                        } else {
                            alert("删除时URL丢失！");
                            return false;
                        }
                    } else {
                        alert("编号丢失！");
                        return false;
                    }
                    nEditing = null;
                });
            }
        };
    }();
    <?php endif;?>

    <?php if (in_array('dialog_search', $jscss_load)):?>
    //<a class="btn default" id="ajax-demo" data-toggle="modal">View Demo</a>
    function dialog_device_Search(settings) {
        var def = {
            id: "ajax-demo",
            fromid: "modelform",
            width: 900,
            left: "31%",
            css: "{width:900px,left:31%}",
            //height:500,
            title: "标题",
            close: function () {
            }
        };
        var settings = $.extend(def, settings);
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
            '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
            '</div>';
        $('body').after("<div id='ajax-modal' class='modal fade' tabindex='-1'></div>"); //插入内容
        $.fn.modalmanager.defaults.resize = true;
        var $modal = $('#ajax-modal');
        $('#' + settings.id).on('click', function () {
            $('body').modalmanager('loading');
            setTimeout(function () {
                $modal.load(settings.url,
                    {
                        "fromid": settings.fromid,
                        "title": settings.title,
                        "needfield": settings.needfield,
                        "getsType": settings.getsType,
                        "getsName": settings.getsName,
                        "needFilter": settings.needFilter,
                        "needField": settings.needField
                    }, function (data) {
                        $modal.modal().css({"height": "700px", "width": settings.width + "px", "left": settings.left});
                        $("#modalclose").click(function () {
                            settings.close();
                        });
                    });
            }, 500);
        });
    }
    <?php endif;?>
</script>
<!-- END JAVASCRIPTS -->

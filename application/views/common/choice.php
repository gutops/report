<!DOCTYPE html>
<?php include APPPATH.'/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
	<!-- BEGIN CONTAINER -->   
	<div class="page-container" data-method-url='<?php echo site_url().'/'.$url_module.'/'.$url_model; ?>' data-btnstatus='<?php echo json_encode($menuinfo['btnStatus']);?>'>
		<!-- BEGIN PAGE -->
		<div class="page-content">

			 <!-- BEGIN SEARCH AREA-->
			 <div id="searchArea" class="row" data-searchdata='<?php echo $searchdata;?>'>
			 <div class="col-md-12">
			 <div class="portlet box light-grey">
			  	<?php echo bs_searchbar();?>
			  	<?php echo $searchArea;?>
			 </div></div></div>
			 <!-- END SEARCH AREA-->
			 <!-- BEGIN DATA AREA-->
			 <div id="dataArea" class="row">
			    <div class="col-md-12">
			       <!-- BEGIN EXAMPLE TABLE PORTLET-->
			       <div class="portlet box light-grey">
				   <?php echo bs_databar();?>
				   <?php echo bs_datatable($datagridinfo);?>
			       </div>
			       <!-- END EXAMPLE TABLE PORTLET-->
			    </div>
			 </div>
			 <!-- END DATA AREA-->
		</div>
		<!-- END PAGE -->    
	</div>
	<!-- END CONTAINER -->
<?php include APPPATH.'/views/common/loadjs.php'; ?>

<script type="text/javascript">
!$(document).ready(function(){
	var settings = {
		 "bServerSide": true,
		 //服务请求地址
		 "sAjaxSource": "<?php echo site_url('/'.$url_module.'/'.$url_model.'/page');?>",
		 //须接收的字段，对应各列
		<?php echo bs_tableinit($datagridinfo);?>	
	};

	TableManaged('',function(tabObj){}).init(settings);
	//时间控件启动
	//FormDatePicker.init(); 
	//FormDatetimePicker.init(); 
});

</script>
<?php include APPPATH.'/views/common/footer.php'; ?>
</body>
<!-- END BODY -->
</html>

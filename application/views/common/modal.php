 <!-- ajax -->
<div class="modal-header">
	<button class="close" aria-hidden="true" data-dismiss="modal" id="modalclose" type="button"></button>
	<h4 class="modal-title"><?php echo $title?></h4>
</div>
<div class="page-container" style="margin-top:0px;" data-method-url='<?php echo site_url().'/'.$url_module.'/'.$url_model; ?>' data-btnstatus='<?php echo json_encode($menuinfo['btnStatus']);?>'>
		<!-- BEGIN PAGE -->
		<div class="page-content" style="padding-top:2px;">
<div id="searchArea" class="row" data-searchdata='<?php echo $searchdata;?>'>
    <div class="col-md-12">
		<div class="portlet box light-grey">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-search"></i>高级查询  
				</div>
				<div class="actions pull-left">
					<a id="seachmodel" class="btn blue">
						<i class="icon-plus-sign"></i>查询
					</a>
				</div>
				<div class="tools pull-right">
					<a class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<?php if (isset($searchArea)):?>
			   <?php echo $searchArea;?>
			<?php endif;?>

		</div>
		
	</div>
</div>
<div id="dataArea" class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
			<div class="portlet box light-grey">
				<?php echo bs_databarmodal();?>
				<?php echo bs_datatable($datagridinfo,$dataList= array());?>
			 </div>
	 </div>
</div>
</div>
</div>
<input type="hidden" id="modal_uuid" value="">
<script type="text/javascript">
!$(document).ready(function(){
	$("#success_modal").attr("disabled","disabled");
	var settings=page(),mi=$("#modelfrom :input");
	$("#seachmodel").click(function(){
		var def1={},name="",val="",url="<?php echo site_url('/'.$url_module.'/'.$url_model.'/modelsearch');?>";
		mi.each(function(){
			if($(this).val() != ""){
				name=$(this).attr('name');
				val=$(this).val();
				def1[name]=val;
			}
		});
		$.post(url,def1,function(data){
			//table.Rows.Clear() 
			var settings = {
					 "bDestroy": true,
					 "bServerSide": true,
					 'bAutoWidth': true, //自动宽度
					 'bFilter': false, //搜索栏
					 'bLengthChange': false, //是否允许用户通过一个下拉列表来选择分页后每页的行数
					 //服务请求地址
					'iDisplayLength': 5,
					 "sAjaxSource": "<?php echo site_url('/'.$url_module.'/'.$url_model.'/modelpage');?>",
					 //须接收的字段，对应各列
					<?php echo bs_tableinit($datagridinfo);?>	
				};
			
			TableManaged('',function(tabObj){}).init(settings);
		});
		
	});
	TableManaged('',function(tabObj){}).init(settings);
	$("#success_modal").click(function(){
		var url="<?php echo site_url('/'.$url_module.'/'.$url_model.'/readdata');?>",
			keyid=$("#modal_uuid").val();
		if(typeof(keyid) == "undefined") return ;
		$.post(url,{'serkeyid':keyid},function(data){
				if(data == 1)
				{
 					alert("抱歉您参数未获取到！");
 					return ;
 				}
				if(typeof(data) != "undefined"){
					data=eval(data);
					var fromid="<?php echo $fromid?>",fid=$("#"+fromid),inputvalue="";
					for(var i=0,l=data.length;i<l;i++){
						for(var key in data[i])
						{	
							inputvalue=fid.find('[name='+key+']');
							
							if(inputvalue[0].tagName == "SELECT" || inputvalue[0].tagName =="INPUT"){
								inputvalue.val(data[i][key]);
								if(inputvalue.attr("type") == "checkbox"){
									if(data[i][key] == 1){
											inputvalue.attr('checked','checked');
										}else{
											inputvalue.removeAttr('checked');
									}
								}
							 }
						}
					}
					//关闭弹出层
					$("#modalclose").click();
				}
		});
		
	});
	
});
function  page(){
	var settings = {
			'bFilter': true,
			'bLengthChange': false,
			'bAutoWidth': true, //自动宽度
			 "bServerSide": true,
			 'iDisplayLength': 5,
			 //服务请求地址
			 "sAjaxSource": "<?php echo site_url('/'.$url_module.'/'.$url_model.'/modelpage');?>",
			 //须接收的字段，对应各列
			<?php echo bs_tableinit($datagridinfo);?>	
		};
	return settings;
}

</script>
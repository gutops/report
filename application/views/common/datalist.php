<!DOCTYPE html>
<?php include APPPATH.'/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
	<!-- BEGIN CONTAINER -->   
	<div class="page-container" >
		<!-- BEGIN PAGE -->
		<div class="page-content">
			 <!-- BEGIN DATA AREA-->
			 <div id="dataArea" class="row">
			    <div class="col-md-12">
			       <!-- BEGIN EXAMPLE TABLE PORTLET-->
			       <div class="portlet box light-grey">
				   <?php echo bs_databar();?>
				   <?php echo bs_datatable($datagridinfo,$datalist);?>
			       </div>
			       <!-- END EXAMPLE TABLE PORTLET-->
			    </div>
			 </div>
			 <!-- END DATA AREA-->
		</div>
		<!-- END PAGE -->    
	</div>
	<!-- END CONTAINER -->
<?php include APPPATH.'/views/common/loadjs.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
	var settings = {
		 "bServerSide": false,
		 'iDisplayLength': -1,
		 //须接收的字段，对应各列
		<?php echo bs_tableinit($datagridinfo);?>	
	};
});

</script>
<?php include APPPATH.'/views/common/footer.php'; ?>
</body>
<!-- END BODY -->
</html>

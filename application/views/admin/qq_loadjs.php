<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/user/gb.css"/>
<link type="text/css" href="<?php echo base_url(); ?>assets/css/user/main.css" rel="stylesheet"/>
<!--[if lte IE 9]>
<script src="<?php echo base_url();?>assets/scripts/html5.js"></script><![endif]-->
<script language="javascript" type="text/javascript">
    g_params = <?php echo $webApp;?>;
    g_user_session = <?php echo $session1;?>;
    g_default_img_url = g_params["g_default_img_url"];//default img url,
    overflow_x = g_params["overflow_x"];//the size of the window to show&hidden scroll bar
    overflow_y = g_params["overflow_y"];
    minWidth = g_params["minWidth"];// the min width&height of the dialog div,when resize
    minHeight = g_params["minHeight"];//background divs' default params
    bg_div_default_left = g_params["bg_div_default_left"];
    bg_div_default_top = g_params["bg_div_default_top"];
    bg_div_default_width = g_params["bg_div_default_width"];
    bg_div_default_height = g_params["bg_div_default_height"];
    bg_div_default_num = g_params["bg_div_default_num"];
    bg_div_default_row_num = g_params["bg_div_default_row_num"];
    bg_div_default_col_num = g_params["bg_div_default_col_num"];
    bg_div_default_interval_width = g_params["bg_div_default_interval_width"];
    bg_div_default_interval_height = g_params["bg_div_default_interval_height"];
    bg_div_default_background_img = g_params["bg_div_default_background_img"];//not include the g_default_img_url,just like "/index.jpg" is ok
    bg_div_default_zindex = g_params["bg_div_default_zindex"];
    bg_div_default_classname = g_params["bg_div_default_classname"];
    bg_div_default_prefix = g_params["bg_div_default_prefix"];
    //the drag drop divs' default params
    //default x,y is the same to background divs
    drag_drop_div_default_classname = g_params["drag_drop_div_default_classname"];
    drag_drop_div_default_content_classname = g_params["drag_drop_div_default_content_classname"];
    drag_drop_div_inner_classname = g_params["drag_drop_div_inner_classname"];
    drag_drop_div_default_bg_img = g_params["drag_drop_div_default_bg_img"];
    drag_drop_div_default_zindex = g_params["drag_drop_div_default_zindex"];
    drag_move_classname = g_params["drag_move_classname"];
    drag_move_prefix = g_params["drag_move_prefix"];
    //the dialog divs' default params
    dialog_pos_x = g_params["dialog_pos_x"];
    dialog_pos_y = g_params["dialog_pos_y"];
    dialog_pos_width = g_params["dialog_pos_width"];
    dialog_pos_height = g_params["dialog_pos_height"];
    dialog_prefix = g_params["dialog_prefix"];
    dialog_num = g_params["dialog_num"];
    dialog_default_bg_img = g_params["dialog_default_bg_img"];
    dialog_default_classname = g_params["dialog_default_classname"];
    dialog_default_zindex = g_params["dialog_default_zindex"];
    //dialog content include the iframe window
    dialog_default_content_classname = g_params["dialog_default_content_classname"];
    dialog_default_content_prefix = g_params["dialog_default_content_prefix"];
    dialog_default_margin = g_params["dialog_default_margin"];

    dialog_default_resize_classname = g_params["dialog_default_resize_classname"];
    //dialog title default params
    dialog_title_classname = g_params["dialog_title_classname"];
    dialog_title_prefix = g_params["dialog_title_prefix"];
    dialog_title_default_title = g_params["dialog_title_default_title"];
    dialog_title_default_height = g_params["dialog_title_default_height"];
    dialog_title_default_drag_classname = g_params["dialog_title_default_drag_classname"];
    g_baseUrl = "<?php echo site_url();?>";
    g_div_url = new Array();
    g_div_url_default = new Array();
    g_user_info = <?php echo $datalist;?>;
    g_dialog_params = new Array();
    g_addapp_params = <?php echo $default;?>;
    g_page = <?php echo $page;?>;

    /*
     * 注销
     */
    function logout(obj) {
        $(obj).closest('form').submit();
    }

</script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery-1.6.1.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/json2.js"></script>
<script language="javascript" type="text/javascript"
        src="<?php echo base_url(); ?>assets/scripts/jquery-UI/jquery-ui.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/mine.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>assets/scripts/jquery.form.js"></script>
<script language="javascript" type="text/javascript">
    function domodifypwd() {
        var url = "<?php echo site_url();?>/public/tools/modifypwd";
        $('#changepassword').load(url);
    }

    function changehoverclass(e) {
        if (e.attr("class") != "hover")
            e.addClass("hover");
        $("#desk_switch").text(e.find("em").text());
    }

    function changedisplay(classname, page, ischange) {
        $("div." + classname).each(function () {
            if ($(this).css("display") != "none" && $(this).attr("page") != page)
                $(this).css("display", "none");
            else if ($(this).css("display") == "none" && $(this).attr("page") == page && ischange)
                $(this).css("display", "");
            if ($(this).attr("class") == drag_drop_div_default_classname && $(this).attr("div_name") == "工地标志") {
                $(this).find("iframe").attr("src", $(this).find("iframe").attr("src"));
            }
        });

    }

    function createDrapDropDivsByData(response) {
        var params = new Array();
        var divs = new Array();
        params.push(response["pos_x"] + "," + response["pos_y"], response["row_num"], response["col_num"],
            response["bg_color"], response["name"], response["src_url"], response["div_pos_x"], response["div_pos_y"],
            response["div_width"], response["div_height"], response["page"], response["id"], response["type"], response["description"]);
        divs.push(params.join("="));
        var id = parseInt(response["pos_x"]) + (parseInt(response["pos_y"]) - 1) * bg_div_default_col_num;
        g_div_url_default[id + "_" + parseInt(response["row_num"]) + "_" + response["col_num"] + "_" + response["id"]] = new Array(response["des_url"], response["id"], false, response["d_pos_x"], response["d_pos_y"],
            response["d_pos_width"], response["d_pos_height"], response["icon"], response["canresize"], response["name"], response["page"], response["login_url"], response["id"], 0, 0);
        createDrapDropDivs(divs);
    }

    function createDialogDivByList2(data) {
        var is_reload = 0;
        if ("GET" == data["method"]) {
            createIframeByPhp(data["name"], data["url"], data["is_max"], is_reload);
        }
        else if ("POST" == data["method"]) {
            var name = data["data"]["name"];
            var url = data["data"]["url"];
            var is_max = data["data"]["is_max"];
            var para = "";
            for (var i in data["data"]) {
                if ("name" != i && "url" != i && "is_max" != i && "_method" != i) {
                    para += i + "=" + data["data"][i] + "&";
                }
            }
            para = para.slice(0, -1);
            if ("" != para)
                url += "?" + para;
            is_reload = 1;
            createIframeByPhp(name, url, is_max, is_reload);
        }
        else {
            alert("未知的提交方式");
        }
    }

    function loadIframeByNoPara(method, name, url, is_max) {
        $("#form_test").load($("#form_test").attr("action"), {
            "_method": method,
            "name": name,
            "url": url,
            "method": method,
            "is_max": is_max
        });
    }

    window.onload = function () {
        var divs = new Array();
        createBackGroundDivs(0, 50, 200, 10, 20, 100, 100, 8, 8);
        var tdwy = <?php echo $tdwy;?>;
        for (var i = 0; i < tdwy.length; i++) {
            g_div_url_default["0_0_0_" + tdwy[i]["id"]] = new Array(tdwy[i]["des_url"], tdwy[i]["id"], false, tdwy[i]["d_pos_x"], tdwy[i]["d_pos_y"],
                tdwy[i]["d_pos_width"], tdwy[i]["d_pos_height"], tdwy[i]["icon"], tdwy[i]["canresize"], tdwy[i]["name"], -1, tdwy[i]["login_url"], tdwy[i]["id"], 0, 0);
            $("li.tdwy").css("display", "");
        }

        for (var i = 0; i < g_user_info.length; i++) {
            if (bg_div_default_row_num >= g_user_info[i]["pos_y"] && bg_div_default_col_num >= g_user_info[i]["pos_x"]) {
                var params = new Array();
                params.push(g_user_info[i]["pos_x"] + "," + g_user_info[i]["pos_y"], g_user_info[i]["row_num"], g_user_info[i]["col_num"], g_user_info[i]["bg_color"],
                    g_user_info[i]["name"], g_user_info[i]["src_url"], g_user_info[i]["div_pos_x"], g_user_info[i]["div_pos_y"], g_user_info[i]["div_width"],
                    g_user_info[i]["div_height"], g_user_info[i]["page"], g_user_info[i]["id"], g_user_info[i]["type"], g_user_info[i]["description"]);
                divs.push(params.join("="));
                var id = parseInt(g_user_info[i]["pos_x"]) + (parseInt(g_user_info[i]["pos_y"]) - 1) * bg_div_default_col_num;
                g_div_url_default[id + "_" + parseInt(g_user_info[i]["row_num"]) + "_" + g_user_info[i]["col_num"] + "_" + g_user_info[i]["id"]] = new Array(g_user_info[i]["des_url"], g_user_info[i]["id"], false, g_user_info[i]["d_pos_x"], g_user_info[i]["d_pos_y"],
                    g_user_info[i]["d_pos_width"], g_user_info[i]["d_pos_height"], g_user_info[i]["icon"], g_user_info[i]["canresize"], g_user_info[i]["name"], g_user_info[i]["page"], g_user_info[i]["login_url"], g_user_info[i]["id"], 0, 0);
            }
            else {
                var page = g_user_info[i]["page"];
                var id = g_user_info[i]["id"];
                var url = "<?php echo base_url() . 'public/addapp/changebtn';?>";
                $("#save").load(url, {"id": id, "type": "delete", "page": page});
                $("#save").load(url, {"id": id, "type": "add", "page": page}, function (response, status) {
                    if (status == "success") {
                        if (response == false) {
                            alert("没有足够空间，添加失败！");
                        }
                        else {
                            createDrapDropDivsByData(eval('(' + response + ')'));
                        }
                    }
                });
            }
        }
        for (var i = 0; i < g_addapp_params.length; i++) {
            if (g_addapp_params[i]["name"] == "添加应用")
                g_div_url_default["addappdialogdiv"] = new Array(g_addapp_params[i]["des_url"], g_addapp_params[i]["id"], false, g_addapp_params[i]["pos_x"], g_addapp_params[i]["pos_y"],
                    g_addapp_params[i]["width"], g_addapp_params[i]["height"], g_addapp_params[i]["icon"], g_addapp_params[i]["canresize"], g_addapp_params[i]["name"], 0, 0);
            else if (g_addapp_params[i]["name"] == "修改页面名称") {
                g_div_url_default["changepageconf"] = new Array(g_addapp_params[i]["des_url"], g_addapp_params[i]["id"], false, g_addapp_params[i]["pos_x"], g_addapp_params[i]["pos_y"],
                    g_addapp_params[i]["width"], g_addapp_params[i]["height"], g_addapp_params[i]["icon"], g_addapp_params[i]["canresize"], g_addapp_params[i]["name"], 0, 0);
            }

        }

        createDrapDropDivs(divs);

        $("#addapp").live("click", function () {
            e_url = createDialogDiv("addappdialogdiv");
            if (e_url)
                g_div_url["addappdialogdiv"] = e_url.id;
        });
        $("#changepageconf").live("click", function () {
            e_url = createDialogDiv("changepageconf");
            if (e_url)
                g_div_url["changepageconf"] = e_url.id;
        });
        $("<div/>").attr("id", "logomove").css({
            "width": "233px", "height": "50px", "position": "absolute", "left": "0px", "top": "0px", "z-index": "130"
        }).bind({
            mousemove: function () {
                if ($("#menu_display").css("display") == "none")
                    $("#menu_display").fadeIn("slow");
            },
            mouseout: function (a) {
                if ($("#menu_display").css("display") != "none") {
                    if (!a)
                        a = window.event;
                    var browser = navigator.userAgent;
                    if (browser.indexOf("MSIE") > 0) {  //if is IE
                        if (this.parentNode.contains(event.toElement)) {  //if is child node
                            return;
                        }
                    }
                    else {
                        if (this.parentNode.contains(a.relatedTarget)) {  //if is child node
                            return;
                        }
                    }
                    $("#menu_display").fadeOut();
                }
            }
        }).appendTo($("#menu"));

        $("#menu_display").live("mouseout", function (a) {
            if ($(this).css("display") != "none") {
                if (!a)
                    a = window.event;
                var browser = navigator.userAgent;
                if (browser.indexOf("MSIE") > 0) {
                    if (this.parentNode.contains(event.toElement)) {
                        return;
                    }
                }
                else {
                    if (this.parentNode.contains(a.relatedTarget)) {
                        return;
                    }
                }
                $(this).fadeOut();
                //alert("out");
            }
        });
        $("ul.desk-switch").find("li").live("click", function () {
            if ($(this).attr("class") != "hover") {
                var new_page = $(this).find("em").text();
                var change = true;
                if (6 == new_page)
                    change = false;
                changedisplay(drag_move_classname, new_page, change);
                changedisplay(drag_drop_div_default_classname, new_page, change);
                changeBackgroundDivClass(new_page);
                $("ul.desk-switch").find("li.hover").removeClass();
                changehoverclass($(this));
            }
        });
        $("#save_default").live("click", function () {
            var url = g_baseUrl + "/admin/home/saveEnv";
            $("#load").load(url);
        });
        $(".topc").find("a").live("click", function () {
            if ($(this).attr("class") == "active") {
                return;
            }
            else {
                $(".topc").find("a.active").find("img").attr("src", "<?php echo base_url();?>assets/img/login/topdian.jpg");
                $(".topc").find("a.active").removeClass();
                $(this).attr("class", "active");
                $(this).find("img").attr("src", "<?php echo base_url();?>assets/img/login/topdian_sel.jpg");
                var browser = navigator.userAgent;   //get the browser attribut
                if (browser.indexOf("MSIE") > 0) {  //if is IE
                    $("li").filter("[name=" + $(this).attr("name") + "]").click();
                }
                else { //if not IE
                    $("li[name=" + $(this).attr("name") + "]").click();
                }

            }
        });
        var click_page = "";
        for (var i in g_page) {
            if (g_page[i]["page_num"] == "0") {
                click_page = g_page[i]["page_name"];
            }
            else {
                var ul = $("ul.desk-switch");
                var li = $("<li/>");
                li.attr({"name": g_page[i]["page_num"]});
                li.appendTo(ul);
                var span = $("<span/>").appendTo(li);
                span.text(g_page[i]["page_name"]);
                var em = $("<em/>").appendTo(li);
                em.text(g_page[i]["page_num"]);
                var a = $("<a/>").appendTo($("div.topc"));
                a.attr({"href": "#", "name": g_page[i]["page_num"], "title": g_page[i]["page_name"]});
                var img = $("<img/>").appendTo(a);
                img.attr("src", "<?php echo base_url();?>assets/img/login/topdian.jpg");
                if (g_page[i]["page_num"] == click_page) {
                    a.click();
                    click_page = "";
                }
            }
        }
        if ("" != click_page) {
            $("div.topc").find("a").each(function () {
                if (click_page == $(this).attr("name"))
                    $(this).click();
            });
        }

    }
    window.onresize = function () {
        setOverflow();
    };

    var t = null;
    t = setTimeout(time, 1000);
    function time() {
        clearTimeout(t);
        dt = new Date();
        var h = dt.getHours();
        var m = dt.getMinutes();
        var s = dt.getSeconds();
        var y = dt.getFullYear();
        var mon = dt.getMonth() + 1;
        var d = dt.getDate();
        $(".time").html(h + ":" + m + ":" + s + " " + y + "-" + mon + "-" + d);
        t = setTimeout(time, 1000);
    }

</script>

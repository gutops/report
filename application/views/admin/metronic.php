<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5
Author: KeenThemes
Website: /
Purchase: /themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?php echo $cfgsystem['seo']['title']; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--[if gte IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <![endif]-->
    <!--[if IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=9"/>
    <![endif]-->
    <!--[if IE 7]>
    <meta http-equiv="X-UA-Compatible" content="IE=7"/>
    <![endif]-->
    <!--[if IE 8]>
    <meta http-equiv="X-UA-Compatible" content="IE=8"/>
    <![endif]-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <meta name="MobileOptimized" content="320">
    <link id="style_color" type="text/css" rel="stylesheet"
          href="<?php echo base_url(); ?>/assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/bootstrap.css"/>
    <link href="<?php echo base_url(); ?>/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet"
          type="text/css"/>
    <link id="style_color" type="text/css" rel="stylesheet"
          href="<?php echo base_url(); ?>/assets/css/style-metronic.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style-responsive.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/plugins.css">
    <link id="style_color" type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/themes/grey.css">
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
<!-- 页面顶部 -->
<div class="header navbar navbar-inverse navbar-fixed-top">
    <div class="header-inner">
        <a class="navbar-brand" href="<?php echo site_url(); ?>">
            <img class="img-responsive" alt="logo" src="<?php echo base_url(); ?>assets/img/logo.png">
        </a>
        <a  href="<?php echo site_url(); ?>">
            <img alt="标题" src="<?php echo base_url(); ?>assets/img/logo1.png">
        </a>
        <a class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" href="javascript:;">
            <img alt="" src="<?php echo base_url(); ?>assets/img/menu-toggler.png">
        </a>
        <ul class="nav navbar-nav pull-right" id="pull-right">
            <li class="dropdown user">
                <a href="/" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                   data-close-others="true">
                    <!--  <img alt="" src="assets/img/avatar1_small.jpg"/>-->
                    <span class="username"><?php echo $session['fullName'] ?></span>
                    <i class="icon-angle-down"></i>
                </a>
                <?php if (!empty($session)): ?>
                    <ul class="dropdown-menu">
                        <li><a>-------------</a></li>
                        <li><a onclick="domodifypwd()" href="javascript:void(0)">修改密码</a></li>
                        <li><a href="javascript:void(0)" onclick="logout(this)"> 退出系统</a></li>
                    </ul>
                <?php endif; ?>
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<!-- 页面顶部全部结束 -->
<!-- 页面核心 -->
<div class="page-container">
    <div id="page-sidebar" class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu" id="my-page-sidebar-menu">
            <li>
                <div class="sidebar-toggler hidden-phone" style="margin-top: 59px;"></div>
            </li>
            <?php if (!empty($datalist['0'])): ?>
                <?php foreach ($datalist['0'] as $key => $title): ?>
                    <?php $title = explode("_", $title); ?>
                    <li class='start'<?php echo (!empty($datalist['2']) && in_array($title['0'], $datalist['2'])) ? "id='li_hexin'" : "" ?>>
                        <a href="#">
                            <i class="<?php echo $title['1']; ?>"></i>
                            <span class="title"><?php echo $title['0']; ?></span>
                            <span class="selected"></span>
                        </a>
                        <?php if (!empty($datalist['1']["{$title['0']}"])): ?>
                            <ul class="sub-menu">
                                <?php foreach ($datalist['1']["{$title['0']}"] as $key => $val): ?>
                                    <?php $val = explode("_", $val); ?>
                                    <li>
                                        <?php if ($key == 0): ?>
                                            <input type="hidden" id="key"
                                                   value="<?php echo (!empty($val['1'])) ? $val['1'] : '' ?>">
                                        <?php endif; ?>
                                        <a href="javascript:;"
                                           onclick="goUrl(this,'<?php echo (!empty($val['1'])) ? $val['1'] : '' ?>','<?php echo (!empty($val['2'])) ? $val['2'] : '' ?>')">
                                            <?php echo (!empty($val['0'])) ? $val['0'] : "" ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
    <div class="page-content" style="padding: 0px 0px 0px 0px;background-image: url(<?php echo base_url();?>assets/img/webQQ/bg.jpg)" id="page-content"></div>
</div>
<!-- 页面核心全部结束  -->
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        2018 &copy; LOCKCOIN 版权所有.
    </div>
    <div class="footer-tools">
         <span class="go-top">
         <i class="icon-angle-up"></i>
         </span>
    </div>
</div>
<div id="save" class="saveuserinfo" style="display:none"></div>
<div id="changepassword" style="z-index:999"></div>
<div class="pwdstatus"></div>
<div id='divTempDialog'></div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url();?>/assets/plugins/respond.min.js"></script>
<script src="<?php echo base_url();?>/assets/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>/assets/scripts/app.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init(); // initlayout and core plugins
        $("#submit").click(function () {
            if ($("#searchtext").val() != "") {
                //$("#mysearch").submit();
                window.location.href = $("#mysearch").attr("action") + '?search=' + $("#searchtext").val();
            }
        });

        $("#pull-right").mouseover(function () {
            $(this).children().attr("class", "dropdown user open");
        });
        $("#pull-right").mouseout(function () {
            $(this).children().attr("class", "dropdown user");
        })

    });
    function goUrl(obj, url, id) {
        go("<?php echo $baseUrl;?>" + url);
        $(obj).parent().addClass("active").siblings().removeClass("active");
        $(obj).parent().parent().parent().addClass("active").siblings().removeClass("active");
    }

    function go(url) {
        var pc = $("#page-content"), width = $(window).width() - $("#page-sidebar").width();
        pc.html('<iframe id="dialog_bd_dialog_1"  width="' + width + 'px"   align="top" style="position: absolute; border: 0px none;height: 95%" src="' + url + '"></iframe>');
    }
    function domodifypwd() {
        //var url = "<?php echo site_url();?>/sys/users/modifypwd";
        //$('#changepassword').load(url);
        window.location.href = "<?php echo $baseUrl;?>/admin/login/forcechange/<?php echo $session['loginName']?>";
    }
    /*
     * 注销
     */
    function logout(obj) {
        window.location.href = "<?php echo $baseUrl;?>/admin/login/logout";
    }

</script>

<!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>
<html>
<head>
    <title>网络信息管理系统</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <?php require_once 'qq_loadjs.php'; ?>
</head>
<body style="overflow-x:hidden;overflow-y:hidden">
<div class="header">
    <h1 class="fl logo"><img src="<?php echo base_url(); ?>assets/img/login/logo.jpg"/></h1>
    <h1 class="fl topc">网络信息管理系统</h1>
    <ul class="desk-switch" style="display: none;">
        <div style="display:none" id="desk_switch">3</div>
    </ul>
    <div class="fr loading">
        <div class="fr text">
            <p class="name"><?php if (isset($session["fullName"])) echo $session["fullName"]; else echo "XXXXX"; ?></p>
            <p class="time"></p>
        </div>
    </div>
    <div class="topc">
    </div>
</div>

<div id="menu" style="position:absolute;right:0px;top:0px;width:210px;height:500px">
    <div class="aside" id="menu_display" style="display:none">
        <ul class="nav">
            <li class="nav-name" style="background:rgb(243,156,18);"><em class="nav-1"></em>
                <p><?php echo $session["fullName"]; ?></p></li>
            <li id="changepageconf"><a href="#"><em class="nav-desk"></em>更改桌面名称</a></li>
            <li id="addapp"><a href="#"><em class="nav-4"></em>添加应用</a></li>
            <?php if ($session['userId'] == 'superadmin'): ?>
            <!--                <li id="save_default"><a href="#"><em class="nav-4"></em>保存环境</a></li>-->
            <?php endif; ?>
            <li><a href="javascript:void(0)" onClick="domodifypwd()" class="logout"><em class="nav-pwd"></em>修改密码</a>
            </li>
            <li><a href="#"><em class="nav-5"></em>
                    <form action="<?php echo site_url(); ?>/admin/login/logout" target="_top" style="display:inline;">
                        <span onclick="logout(this)">退出系统</span></form>
                </a></li>
        </ul>
    </div>
</div>

<div id="footer">
    <div class="footer-con">
        <ul class="task"></ul>
        <div class="copyright">&nbsp;&nbsp;&nbsp;&nbsp;COPYRIGHT&copy;&nbsp;&nbsp;2017&nbsp;&nbsp;NSSOL&nbsp;&nbsp;&nbsp;&nbsp;版权所有</div>
    </div>
    <div class="footer-bg"></div>
</div>

<div id="load">

</div>
<form id="form_load" style="display:none"></form>
<form id="form_test" action="<?php echo site_url(); ?>/admin/home/loadiframe" method="post" style="display:none"></form>
<div id="save" class="saveuserinfo" style="display:none"></div>
<div id="changepassword" style="z-index:999"></div>
<div class="pwdstatus"></div>
<div id='divTempDialog'></div>
<style type="text/css">
    .pwdstatus {
        z-index: 999;
        position: absolute;
        right: 50%;
        top: 0;
        padding: 25px 10px;
        font-size: 24px;
        background: #000;
        z-index: 999;
        display: none;
        filter: alpha(opacity=60);
        -moz-opacity: 0.6;
        -khtml-opacity: 0.6;
        opacity: 0.6;
        color: #fff;
    }
</style>
</body>
</html>

<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container" data-method-url='<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>'
     data-btnstatus='<?php echo json_encode($menuinfo['btnStatus']); ?>'>
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <?php echo bs_configsearch($menuinfo,$selectedsearch);?>
        <?php echo bs_configdatalist($datagridinfo,$selectedlistdata);?>
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- MODIFY START -->
                    <?php echo bs_breadcrumb("流程管理/冷钱包购币"); ?>
                    <!-- MODIFY END -->
                </div>
            </div>
            <!-- END PAGE HEADER-->

            <!-- BEGIN SEARCH AREA-->
            <div id="searchArea" class="row" data-searchdata='<?php echo $searchdata; ?>'>
                <div class="col-md-12">
                    <div class="portlet box light-grey scrollnew">
                        <?php echo bs_searchbar($menuinfo['dispbutton']); ?>
                        <?php echo $searchArea; ?>
                    </div>
                </div>
            </div>
            <!-- END SEARCH AREA-->
            <!-- BEGIN DATA AREA-->
            <div id="dataArea" class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box light-grey">
                        <?php echo bs_databar(); ?>
                        <?php echo bs_datatable($datagridinfo, $dataList = array()); ?>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END DATA AREA-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
    <?php $jscss_load = array('TableManaged', 'FormDatePicker'); ?>
    <?php include APPPATH . '/views/common/loadjs.php'; ?>
    <script type="text/javascript">
        !$(document).ready(function () {
            var settings = {
                "bServerSide": true,
                "iDisplayStart":<?php echo $iDisplayStart;?>,
                //服务请求地址
                "sAjaxSource": "<?php echo site_url('/' . $url_module . '/' . $url_model . '/page');?>",
                //须接收的字段，对应各列
                <?php echo bs_tableinit($datagridinfo);?>
            };
            TableManaged('', function (tabObj) {
            }).init(settings);
            FormDatePicker.init();
        });
    </script>
    <?php include APPPATH . '/views/common/footer.php'; ?>
</body>
<!-- END BODY -->
</html>

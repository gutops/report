<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container" data-method-url='<?php echo $url_module . '/' . $url_model; ?>'
     data-btnstatus='<?php echo json_encode($menuinfo['btnStatus']); ?>'>
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <?php echo bs_configsearch($menuinfo,$selectedsearch);?>
        <?php echo bs_configdatalist($datagridinfo,$selectedlistdata);?>
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- MODIFY START -->
                <?php echo bs_breadcrumb("报表分析/LOCK币核算"); ?>
                <!-- MODIFY END -->
            </div>
        </div>
        <!-- END PAGE HEADER-->


        <div id="batchArea" class="row">
            <div class="col-md-12">
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-search"></i>重新核算数据</div>
                        <div class="tools pull-right">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form action="<?php echo site_url($url_module . '/' . $url_model . '/recalc');?>" method="post"
                              class="form-horizontal" id="batchForm">
                            <div class="form-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label class="control-label col-md-6">开始日期</label>
                                        <div class="col-md-6">
                                            <div data-date-viewmode="years" data-date-format="yyyy-mm-dd"
                                                 class="input-group input-medium date date-picker">
                                                <input type="text" id="starttime" name="starttime"  readonly="" class="form-control" value="<?php echo $starttime;?>" >
                                                <span class="input-group-btn">
                                                        <button type="button" class="btn default"><i class="icon-calendar"></i></button>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label col-md-6">结束日期</label>
                                        <div class="col-md-6">
                                            <div data-date-viewmode="years" data-date-format="yyyy-mm-dd"
                                                 class="input-group input-medium date date-picker">
                                                <input type="text" id="endtime" name="endtime"  readonly="" class="form-control" value="<?php echo $endtime;?>" >
                                                <span class="input-group-btn">
                                                        <button type="button" class="btn default"><i class="icon-calendar"></i></button>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-9">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group col-md-offset-10 col-md-1">
                                            <a type="button" href="<?php echo site_url($url_module . '/' . $url_model . '/recalc');?>"
                                               class="btn green"  id="btnRecalc">计算</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN DATA AREA-->
        <div id="dataArea" class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box light-grey">
                    <?php echo bs_databar(); ?>
                    <?php echo bs_datatable($datagridinfo, $dataList = array()); ?>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END DATA AREA-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<?php $jscss_load = array('TableManaged', 'FormDatePicker'); ?>
<?php include APPPATH . '/views/common/loadjs.php'; ?>
<script type="text/javascript">
    !$(document).ready(function () {
        var settings = {
            "bServerSide": true,
            "iDisplayStart":<?php echo $iDisplayStart;?>,
            //服务请求地址
            "sAjaxSource": "<?php echo site_url($url_module . '/' . $url_model . '/page');?>",
            //须接收的字段，对应各列
            <?php echo bs_tableinit($datagridinfo);?>
        };
        TableManaged('', function (tabObj) {
        }).init(settings);
        //时间控件启动
        FormDatePicker.init();
        //FormDatetimePicker.init();
        createBtn("#btnRecalc");
        var btnRecalc = BtnMaker.instance.get('btnRecalc');
        btnRecalc.skip = function () {
            var pageContent = $('.page-content');
            var starttime = $('#starttime').val();
            var endtime = $('#endtime').val();
            if(starttime == "" ){
                data = {'success': false, 'msg': '必须选择【开始日期】'};
                showMsg(data);
                return false;
            }
            if(endtime == "" ){
                data = {'success': false, 'msg': '必须选择【结束日期】'};
                showMsg(data);
                return false;
            }
            App.blockUI(pageContent);
            $.getJSON(this.href, {starttime:starttime,endtime:endtime}, function (response) {
                App.unblockUI(pageContent);
                console.log(response);
                if (response.success == false) {
                    showMsg(response);
                } else {
                    showMsg(response);
                    window.location.reload();
                }
            });
        }
    });

</script>
<?php include APPPATH . '/views/common/footer.php'; ?>
</body>
<!-- END BODY -->
</html>

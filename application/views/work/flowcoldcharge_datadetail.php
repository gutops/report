<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>数据详情</div>
            </div>
            <div class="portlet-body form">
                <form action="" data-method-url='<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>'
                      method="POST"
                      class="data_formDetail form-horizontal" novalidate="novalidate"
                      data-keyid="<?php echo $keyid = isset($keyId) ? $keyId : ''; ?>"
                      editType="<?php echo $editType; ?>" jsonDetail='<?php echo json_encode(isset($datalist)?$datalist:array()); ?>'>
                    <!-- 表单主体 开始  -->
                    <div class="form-body">

                        <!-- 表格样式表单 开始 -->
                        <div class="tableForm">
                            <table class="table table-bordered ">
                                <!-- MODIFY START -->
                                <caption><span class="left">&nbsp;</span>数据字典<span class="right">&nbsp;</span></caption>
                                <!-- MODIFY END -->
                                <tbody>
                                <!-- MODIFY START -->
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">申请单号:</label></td>
                                    <td>
                                        <input class="form-control" name="order_no"  readonly type="text"/>
                                    </td>
                                    <!--  标准下拉框，开始 ID,NAME   -->
                                    <td><label class="control-label">币种:</label></td>
                                    <td>
                                        <select class="form-control input-sm"
                                                data-validate="{required:'true'}" name="coincode">
                                            <?php echo Ousuclass::all_html_option($cointype,'name','name'); ?>
                                        </select>
                                    </td>
                                    <!--  标准下拉框，结束 ID,NAME   -->
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">申请时间:</label></td>
                                    <td>
                                        <div class="input-group input-medium date datetime-picker ">
                                            <input class="form-control" name="act_time" data-validate="{required:true}" type="text"/>
                                            <span class="input-group-btn">
                                                    <button id="examinationDatetimeBtn" class="btn default date-set"
                                                            type="button"><i class="icon-calendar"></i></button>
                                                    </span></div>
                                    </td>
                                    <td><label class="control-label">审批时间:</label></td>
                                    <td>
                                        <input class="form-control" name="oper_time"  readonly type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">数量:</label></td>
                                    <td>
                                        <input class="form-control" name="amount" data-validate="{required:true}"  type="text"/>
                                    </td>
                                    <td><label class="control-label">状态:</label></td>
                                    <td>
                                        <input class="form-control" name="status"  readonly type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <!--  标准下拉框，开始 ID,NAME   -->
                                    <td><label class="control-label">冷钱包级别:</label></td>
                                    <td>
                                        <select class="form-control input-sm"
                                                data-validate="{required:'true'}" name="act_type">
                                            <?php echo Ousuclass::all_html_option(array(array('name'=>'一级冷钱包'),array('name'=>'二级冷钱包')),'name','name'); ?>
                                        </select>
                                    </td>
                                    <!--  标准下拉框，结束 ID,NAME   -->
                                    <!--  标准下拉框，开始 ID,NAME   -->
                                    <td><label class="control-label">短信发送:</label></td>
                                    <td>
                                        <select class="form-control input-sm"
                                                data-validate="{required:'true'}" name="targetname">
                                            <?php echo Ousuclass::all_html_option(array(array('name'=>'发送'),array('name'=>'不发送')),'name','name'); ?>
                                        </select>
                                    </td>
                                    <!--  标准下拉框，结束 ID,NAME   -->
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td ><label class="control-label">备注:</label></td>
                                    <td colspan="3">
                                        <input class="form-control" name="memo"   type="text"/>
                                    </td>
                                </tr>
                                <!--  UEDIT，开始   -->
                                <tr class="gradeX gradeY lightgrey">
                                    <td colspan="6"><label class="control-label">图片:</label></td>
                                </tr>
                                <tr style="height:550px;">
                                    <td colspan="6">
                                        <script type="text/plain" id="ueditor" name="file"></script>
                                    </td>
                                </tr>
                                <!--  UEDIT，结束   -->
                                <!-- MODIFY END -->
                                </tbody>
                            </table>

                        </div>
                        <!-- 表格样式表单 结束 -->
                    </div>
                    <!-- 表单主体 结束  -->

                    <!-- 表单操作 开始  -->
                    <div class="modal-footer">
                        <a type="button" class="btn default"
                           href="<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>">返回</a>
                        <button type="submit" class="btn blue submitBtn">确认</button>
                    </div>
                    <!-- 表单操作 开始  -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<?php $jscss_load = array('Detail', 'formValidate', 'FormDatePicker','ueditor'); ?>
<?php include APPPATH . '/views/common/loadjs.php'; ?>

<script type="text/javascript">

    Detail().init();

    var cusVali = {};
    FormDatetimePicker.init();
    formValidate(".data_formDetail").init(cusVali);

    var editor = UE.getEditor('ueditor', {
        minFrameHeight: 300
        , initialFrameWidth: '100%'
        , initialFrameHeight: 470
        , autoHeightEnabled: true
    });

</script>

<?php include APPPATH . '/views/common/footer.php'; ?>

</body>
<!-- END BODY -->
</html>


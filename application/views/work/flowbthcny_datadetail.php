<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-full-width">
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>数据详情</div>
            </div>
            <div class="portlet-body form">
                <form action="" data-method-url='<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>'
                      method="POST"
                      class="data_formDetail form-horizontal" novalidate="novalidate"
                      data-keyid="<?php echo $keyid = isset($keyId) ? $keyId : ''; ?>"
                      editType="<?php echo $editType; ?>" jsonDetail='<?php echo json_encode(isset($datalist)?$datalist:array()); ?>'>
                    <!-- 表单主体 开始  -->
                    <div class="form-body">

                        <!-- 表格样式表单 开始 -->
                        <div class="tableForm">
                            <table class="table table-bordered ">
                                <!-- MODIFY START -->
                                <caption><span class="left">&nbsp;</span>数据字典<span class="right">&nbsp;</span></caption>
                                <!-- MODIFY END -->
                                <tbody>
                                <!-- MODIFY START -->
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">交易单号:</label></td>
                                    <td>
                                        <input class="form-control" name="order_no"  readonly type="text"/>
                                    </td>
                                    <td><label class="control-label">币种:</label></td>
                                    <td>
                                        <input class="form-control" name="coincode"  readonly type="text" />
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">用户姓名:</label></td>
                                    <td>
                                        <input class="form-control" name="username" data-validate="{required:true}" type="text"/>
                                    </td>
                                    <td><label class="control-label">用户账号:</label></td>
                                    <td>
                                        <input class="form-control" name="accountname" data-validate="{required:true}"  type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">转款时间:</label></td>
                                    <td>
                                        <div class="input-group input-medium date datetime-picker ">
                                            <input class="form-control" name="act_time" data-validate="{required:true}" type="text"/>
                                            <span class="input-group-btn">
                                                    <button id="examinationDatetimeBtn" class="btn default date-set"
                                                            type="button"><i class="icon-calendar"></i></button>
                                                    </span></div>
                                    </td>
                                    <td><label class="control-label">状态:</label></td>
                                    <td>
                                        <input class="form-control" name="status"  readonly type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td><label class="control-label">USDT:</label></td>
                                    <td>
                                        <input class="form-control" name="random" data-validate="{required:true}" type="text"/>
                                    </td>
                                    <td><label class="control-label">金额:</label></td>
                                    <td>
                                        <input class="form-control" name="amount" data-validate="{required:true}"  type="text"/>
                                    </td>
                                </tr>
                                <tr class="gradeX gradeY">
                                    <td ><label class="control-label">备注:</label></td>
                                    <td colspan="3">
                                        <input class="form-control" name="memo"   type="text"/>
                                    </td>
                                </tr>
                                <!--  UEDIT，开始   -->
                                <tr class="gradeX gradeY lightgrey">
                                    <td colspan="6"><label class="control-label">图片:</label></td>
                                </tr>
                                <tr style="height:550px;">
                                    <td colspan="6">
                                        <script type="text/plain" id="ueditor" name="file"></script>
                                    </td>
                                </tr>
                                <!--  UEDIT，结束   -->
                                <!-- MODIFY END -->
                                </tbody>
                            </table>

                        </div>
                        <!-- 表格样式表单 结束 -->
                    </div>
                    <!-- 表单主体 结束  -->

                    <!-- 表单操作 开始  -->
                    <div class="modal-footer">
                        <a type="button" class="btn default"
                           href="<?php echo site_url() . '/' . $url_module . '/' . $url_model; ?>">返回</a>
                        <button type="submit" class="btn blue submitBtn">确认</button>
                    </div>
                    <!-- 表单操作 开始  -->
                </form>
            </div>
        </div>
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<?php $jscss_load = array('Detail', 'formValidate', 'FormDatePicker','ueditor'); ?>
<?php include APPPATH . '/views/common/loadjs.php'; ?>

<script type="text/javascript">

    Detail().init();

    var cusVali = {};
    FormDatetimePicker.init();
    formValidate(".data_formDetail").init(cusVali);

    var editor = UE.getEditor('ueditor', {
        minFrameHeight: 300
        , initialFrameWidth: '100%'
        , initialFrameHeight: 470
        , autoHeightEnabled: true
    });

</script>

<?php include APPPATH . '/views/common/footer.php'; ?>

</body>
<!-- END BODY -->
</html>


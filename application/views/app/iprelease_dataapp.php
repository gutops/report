<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
<?php include APPPATH . '/views/common/header.php'; ?>
<!-- BEGIN BODY -->
<body>

<!-- MODIFY START -->
<div class="portlet-body blue">
    <table class="table table-striped table-bordered table-hover " style="background-color: white;">
        <caption style="background-color: #c66964;color: white;height: 32px;text-align: center;padding-top: 8px;">IP需要释放清单
        </caption>
        <thead>
        <tr>
            <td>网段名称</td>
            <td>IP地址</td>
            <td>据点</td>
            <td>系统</td>
            <td>环境</td>
            <td>占用人</td>
            <td>申请人</td>
            <td>到期时间</td>
            <td>状态</td>
            <td>动作</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($datalist as $item): ?>
            <tr>
                <td><?php echo $item['NAME']; ?></td>
                <td><?php echo $item['IP']; ?></td>
                <td><?php echo $item['LOCATION']; ?></td>
                <td><?php echo $item['SYSTEM']; ?></td>
                <td><?php echo $item['ENV']; ?></td>
                <td><?php echo $item['MAN_USE']; ?></td>
                <td><?php echo $item['MAN_APPLY']; ?></td>
                <td><?php echo $item['DATE_END']; ?></td>
                <td><?php echo $item['STATUS']; ?></td>
                <td>
                    <button type="button" class="btn blue" style="padding: 3px 14px;font-size: 12px;" dataname="<?php echo $item['ID'];?>" onclick="expendend(this);">延长</button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php include APPPATH . '/views/common/loadmessage.php'; ?>
<script type="text/javascript">
    function popwin(obj) {
        var arr = $(obj).attr("dataname").split('-');
        window.parent.loadIframeByNoPara('get', arr[0], arr[1]);
    }
    function expendend(obj){
        var dataid = $(obj).attr("dataname");
        var url = "<?php echo site_url();?>/app/iprelease/expendend/"+dataid;
        $.getJSON(url, '', function (data) {
            showMsg(data);
            if (data['success']) {
                window.location.reload();
            }
        })
    }
</script>
<!-- MODIFY END -->
</body>
<!-- END BODY -->
</html>


<?php

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('guid');
        $this->load->library('session');
        $this->load->database();
    }

    public function index()
    {
        $webApp = array();
        $webApp["g_default_img_url"] = base_url() . "assets/img/webQQ";
        $webApp["overflow_x"] = 800;
        $webApp["overflow_y"] = 600;
        $webApp["minWidth"] = 400;
        $webApp["minHeight"] = 300;

        $webApp["bg_div_default_left"] = 300;
        $webApp["bg_div_default_top"] = 300;
        $webApp["bg_div_default_width"] = 200;
        $webApp["bg_div_default_height"] = 100;
        $webApp["bg_div_default_num"] = 16;
        $webApp["bg_div_default_row_num"] = 4;
        $webApp["bg_div_default_col_num"] = 4;
        $webApp["bg_div_default_interval_width"] = 20;
        $webApp["bg_div_default_interval_height"] = 20;
        $webApp["bg_div_default_background_img"] = "/img_chrysanthemum.jpg";
        $webApp["bg_div_default_zindex"] = 4;
        $webApp["bg_div_default_classname"] = "base_background";
        $webApp["bg_div_default_prefix"] = "bg_div_";

        $webApp["drag_drop_div_default_classname"] = "drag_drop";
        $webApp["drag_drop_div_inner_classname"] = "drag_drop_inner";
        $webApp["drag_drop_div_default_content_classname"] = "drag_drop_div";
        $webApp["drag_drop_div_default_bg_img"] = "./img_desert.jpg";
        $webApp["drag_drop_div_default_zindex"] = "7";
        $webApp["drag_move_prefix"] = "drag_move_";
        $webApp["drag_move_classname"] = "drag_move";

        $webApp["dialog_pos_x"] = 400;
        $webApp["dialog_pos_y"] = 50;
        $webApp["dialog_pos_width"] = 1100;
        $webApp["dialog_pos_height"] = 500;
        $webApp["dialog_prefix"] = "dialog_";
        $webApp["dialog_num"] = 0;
        $webApp["dialog_default_bg_img"] = "/img_koala.jpg";
        $webApp["dialog_default_classname"] = "box";
        $webApp["dialog_default_zindex"] = "200";
        $webApp["dialog_default_content_classname"] = "bd";
        $webApp["dialog_default_content_prefix"] = "dialog_bd_";
        $webApp["dialog_default_margin"] = "1px";
        $webApp["dialog_default_resize_classname"] = "resize_div";

        $webApp["dialog_title_classname"] = "hd";
        $webApp["dialog_title_prefix"] = "dialog_title_";
        $webApp["dialog_title_default_title"] = "demo title";
        $webApp["dialog_title_default_height"] = "24px";
        $webApp["dialog_title_default_drag_classname"] = "dialog_move";
        $webApp = json_encode($webApp);
        header("Content-Type:text/html;charset=utf-8");
        $url = array();
        $session = $this->session->userdata('sessioninfo');
        //$session['userId']='1';
        $sql = "select type,page,src_url,des_url,login_url,bg_color,name,col_num,row_num,sys_conf.icon as icon,sys_conf.canresize as canresize,user_conf.pos_x as pos_x,user_conf.pos_y as pos_y,sys_conf.id as id,
                sys_conf.pos_x as d_pos_x,sys_conf.pos_y as d_pos_y,sys_conf.width as d_pos_width,sys_conf.height as d_pos_height,sys_conf.div_pos_x as div_pos_x,
                sys_conf.div_pos_y as div_pos_y,sys_conf.div_width as div_width,sys_conf.div_height as div_height,sys_conf.description
                from sys_conf,user_conf,sys_rights 
                where user_id='" . $session["userId"] . "' and user_conf.conf_id=sys_conf.id and user_conf.is_show = 1 and user_conf.can_show = 1 and sys_rights.userid = user_conf.user_id and sys_rights.uriid = user_conf.conf_id";
        $query = $this->db->query($sql);
        $datalist = $query->result_array();
        if (count($datalist) == 0) {
            $default = 'default';
            $sql = "select type,page,src_url,des_url,login_url,bg_color,name,col_num,row_num,sys_conf.icon as icon,sys_conf.canresize as canresize,user_conf.pos_x as pos_x,user_conf.pos_y as pos_y,sys_conf.id as id,
			sys_conf.pos_x as d_pos_x,sys_conf.pos_y as d_pos_y,sys_conf.width as d_pos_width,sys_conf.height as d_pos_height,sys_conf.div_pos_x as div_pos_x,
			sys_conf.div_pos_y as div_pos_y,sys_conf.div_width as div_width,sys_conf.div_height as div_height,sys_conf.description
			from sys_conf,user_conf
			where user_id='$default' and user_conf.conf_id=sys_conf.id and user_conf.is_show = 1 and user_conf.can_show = 1";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            $sql = "insert into user_conf 
			select uuid() as id,'" . $session["userId"] . "' as user_id,sys_conf.id as conf_id,conf_name,page,user_conf.pos_x,user_conf.pos_y,user_conf.can_show,user_conf.is_show
			from sys_conf,user_conf
			where user_id='$default' and user_conf.conf_id=sys_conf.id and user_conf.is_show = 1 and user_conf.can_show = 1";
            $result = $this->db->query($sql);
            $sql = "select type,page,src_url,des_url,login_url,bg_color,name,col_num,row_num,sys_conf.icon as icon,sys_conf.canresize as canresize,user_conf.pos_x as pos_x,user_conf.pos_y as pos_y,sys_conf.id as id,
			sys_conf.pos_x as d_pos_x,sys_conf.pos_y as d_pos_y,sys_conf.width as d_pos_width,sys_conf.height as d_pos_height,sys_conf.div_pos_x as div_pos_x,
			sys_conf.div_pos_y as div_pos_y,sys_conf.div_width as div_width,sys_conf.div_height as div_height,sys_conf.description
			from sys_conf,user_conf,sys_rights
			where user_id='" . $session["userId"] . "' and user_conf.conf_id=sys_conf.id and user_conf.is_show = 1 and user_conf.can_show = 1 and sys_rights.userid = user_conf.user_id and sys_rights.uriid = user_conf.conf_id";
            $query = $this->db->query($sql);
            $datalist = $query->result_array();
        }
        $sql = "select * from sys_conf where src_url = 'default'";
        $query = $this->db->query($sql);
        $default = $query->result_array();
        $user_info = "select conf_id,conf_name from user_conf where user_id='" . $session["userId"] . "'";
        $query = $this->db->query($user_info);
        $user_info = $query->result_array();

        $sql = "select page_num,page_name from user_pageconf where user_id='" . $session["userId"] . "' order by page_num";
        $query = $this->db->query($sql);
        $page = $query->result_array();
        if (count($page) == 0) {
            $sql = "select page_num,page_name from user_pageconf where user_id='default' order by page_num";
            $query = $this->db->query($sql);
            $page = $query->result_array();
            foreach ($page as $value) {
                $gg = $this->guid->newGuid();
                $sql = "insert into user_pageconf values('" . $gg->toString() . "','" . $session["userId"] . "'," . $value["page_num"] . ",'" . $value["page_name"] . "')";
                $this->db->query($sql);
            }
        }
        $sql = "select type,src_url,des_url,login_url,bg_color,name,col_num,row_num,sys_conf.icon as icon,sys_conf.canresize as canresize,sys_conf.id as id,
		sys_conf.pos_x as d_pos_x,sys_conf.pos_y as d_pos_y,sys_conf.width as d_pos_width,sys_conf.height as d_pos_height,sys_conf.div_pos_x as div_pos_x,
		sys_conf.div_pos_y as div_pos_y,sys_conf.div_width as div_width,sys_conf.div_height as div_height,sys_conf.description
		from sys_conf,sys_rights
		where sys_rights.userid = '" . $session["userId"] . "' and sys_rights.uriid = sys_conf.id and sys_conf.id='63'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $data['session'] = $session;
        $data['session1'] = json_encode($session);
        $data['addapp'] = json_encode($user_info);
        $data['webApp'] = $webApp;
        $data['datalist'] = json_encode($datalist);
        $data['baseUrl'] = site_url();
        $data['default'] = json_encode($default);
        $data['page'] = json_encode($page);
        $data['tdwy'] = json_encode($result);
        //密码修改状态信息获取
        if (isset($_REQUEST["modiMess"])) {
            $data['changepwd'] = $_REQUEST["modiMess"];
        }
        //print_r(base_url());exit();
        $this->load->view('admin/home.php', $data);
    }

    public function returninfo()
    {
        $data = $_REQUEST["data"];
        $data = json_decode($data, true);
        $sql = "";
        if ("update" == $data["type"])
            $sql = "update user_conf set pos_x=" . $data["pos_x"] . ",pos_y=" . $data["pos_y"] . ",page=" . $data["page"] . " where user_id='" . $data["user_id"] . "' and conf_id=" . $data["id"];
        else if ("delete" == $data["type"]) {
            $sql = "delete from user_conf where user_id='" . $data["user_id"] . "' and conf_id=" . $data["id"] . " and page=" . $data["page"];
        }
        if ("" == $sql) {
            echo "save failed!";
            return false;
        } else {
            try {
                $this->db->query($sql);
                echo "save successful!<br />";
                echo $sql;
            } catch (Exception $e) {
                echo "save failed!";
            }
        }
    }

    public function loadiframe()
    {
        $data = array();
        if ("POST" == strtoupper($_REQUEST["method"])) {
            $data["method"] = "POST";
            if (!isset($_REQUEST["is_max"]))
                $_REQUEST["is_max"] = 0;
            $data["data"] = $_REQUEST;
        } else if ("GET" == strtoupper($_REQUEST["method"])) {
            $data["method"] = "GET";
            $data["url"] = $_REQUEST["url"];
            $data["name"] = $_REQUEST["name"];
            if (isset($_REQUEST["is_max"]))
                $data["is_max"] = $_REQUEST["is_max"];
            else
                $data["is_max"] = 0;
        }
        $data = json_encode($data);
        echo "<script>createDialogDivByList2($data);</script>";
    }

    public function saveEnv()
    {
        $session = $this->session->userdata('sessioninfo');
        $user = $session["accounttype"];
        $sql = "select value from sys_dictdata where type='账号类型' and name='$user'";
        $query = $this->db->query($sql);
        $query = $query->result_array();
        $sql = "delete from user_conf where user_id='" . $query[0]["value"] . "'";
        $this->db->query($sql);
        $sql = "INSERT INTO user_conf SELECT UUID() AS UUID,'" . $query[0]["value"] . "' as user_id,conf_id,conf_name,page,pos_x,pos_y,can_show,is_show 
        FROM user_conf WHERE user_id='" . $session["userId"] . "'";
        //echo $sql;exit;
        $this->db->query($sql);
        echo "<script>alert('保存成功')</script>";
    }

    public function getinfo()
    {
        $name = $_REQUEST["name"];
        $url = $_REQUEST["url"];
        $sql = "select * from sys_conf where name='" . $name . "'";
        $query = $this->db->query($sql);
        $query = $query->result_array();
        if (0 == count($query))
            echo false;
        else {
            echo json_encode($query[0]);
        }
    }
}

<?php
class Metronic extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
    }

    public function index()
    {
        header("Content-Type:text/html;charset=utf-8");
        $session = $this->session->userdata('sessioninfo');
        $sql = "
		select sys_conf.id,sys_conf.group as menu1,sys_conf.name as menu2,des_url
		from  sys_conf         as  sys_conf
		inner join sys_rights  as  sys_rights on sys_rights.uriid = sys_conf.id
		where sys_rights.userid='".$session["userId"]."'  and  sys_conf.type != 'app' 
				and (not (sys_conf.des_url like 'public%')) 
		group by sys_conf.id
		";
        $query = $this->db->query($sql);
        $data['session'] = $session;
        $query = $query->result_array();
        if(!empty($_GET['search'])){
            $_POST['search']=$_GET['search'];
            $sql="SELECT group_concat(sys_conf.name as menu2) as id from  sys_conf where  sys_conf.name like '%{$_POST['search']}%' LIMIT 1 ";
            $query_search = $this->db->query($sql);
            $query_search = $query_search->result_array();
            $query_search = array_flip(explode(",", $query_search['0']['id']));
        }
        foreach($query as $v){
            if((!empty($query_search) && array_key_exists($v['menu2'],$query_search))){
                $v['menu2']=$v['menu2'].'_'.$v['des_url'].'_'.$v['id']."_1";
                $temp3[]=$v['menu1'];
            }else{
                $v['menu2']=$v['menu2'].'_'.$v['des_url'].'_'.$v['id'];
            }
            $temp[]=$v['menu1'];
            $temp1["{$v['menu1']}"][]=$v['menu2'];
        }
        $temp =array_flip(array_flip($temp));    //去掉重复的字符串,也就是重复的一维数组

        $data['datalist']=array('0'=>$temp,'1'=>$temp1,'2'=>(!empty($temp3))?array_flip(array_flip($temp3)):"");
        $data['baseUrl'] = site_url();
		$data['cfgsystem'] = $this->config->item('cfg-system');
        //密码修改状态信息获取
        if(isset($_REQUEST["modiMess"]))
        {
            $data['changepwd'] = $_REQUEST["modiMess"];
        }
        // 一级菜单获得图标
        $iconlist = $this->db->query("select name,value from sys_dictdata where type='项目名称'")->result_array();
        foreach($data['datalist'][0] as $key=>$item){
            foreach($iconlist as $list){
                if ($item == $list['name']){
                    if (substr($list['value'],0,4)=='icon') {
                        $data['datalist'][0][$key] = $data['datalist'][0][$key] . '_' . $list['value'];
                    }
                    else {
                        $data['datalist'][0][$key] = $data['datalist'][0][$key].'_icon-home';
                    }
                }
            }
            if (strpos($data['datalist'][0][$key],"_") === False){
                $data['datalist'][0][$key] = $data['datalist'][0][$key].'_icon-home';
            }
        }
		$this->load->view('admin/metronic.php',$data);
    }


}

<?php

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function index()
    {
        $session = $this->session->userdata('sessioninfo');
		$data = $this->config->item('cfg-system');
        try {
            if ($session["isLogin"]) {
                $this->config->load('cfg-system', true);
                $this->sysconfig = $this->config->item('cfg-system');
                if ($this->sysconfig['system']['dispmode'] == 'WIN8') {
                    redirect('admin/home');
                } else {
                    redirect('admin/metronic');
                }
            } else $this->load->view('login/loginNovercode.php',$data);
        } catch (Exception $e) {
            $this->load->view('login/loginNovercode.php',$data);
        }
    }

    /**
     * 生成验证码
     */
    public
    function vercode()
    {
        require_once APPPATH . '/libraries/Captcha/Captcha.php';
        Captcha::$seKey = 'oa_seKey'; //登录验证码的session键名
        Captcha::$useImgBg = true;  //是否使用背景图片
        Captcha::$useNoise = false; //是否添加杂点
        Captcha::$useCurve = false; //是否绘制干扰线
        Captcha::$useZh = false; //是否使用中文验证码
        Captcha::$fontSize = 15; //验证码字体大小(像素)
        Captcha::$length = 4; // 验证码字符数
        Captcha::$_codeSet = '0123456789'; //验证码字符串
        Captcha::entry(); //输出图片
    }

    public
    function loginremote()
    {
        $params = $this->input->get(NULL, TRUE);
        $query = $this->db->get_where('sys_user', array('username' => $params['username'], 'password' => $params['password']));
        $result = $query->result_array();
        if (sizeof($result) == 1) {
            if (($result[0]['lastchange'] == "") ||
                (strtotime('+3 month', strtotime($result[0]['lastchange'])) < strtotime(date("Y-m-d H:i:s")))
            ) {
                redirect(site_url() . "/admin/login/forcechange/" . $params['useranme']);
                exit;
            }
            $session['isLogin'] = true;
            $session['userId'] = $result[0]['id'];
            $session["loginName"] = $result[0]["username"];
            $session["fullName"] = $result[0]["fullname"];
            $session["accounttype"] = $result[0]["accounttype"];
            $session["ip"] = $_SERVER["REMOTE_ADDR"];
            $session["user_agent"] = $_SERVER["HTTP_USER_AGENT"];
            $sql = "update sys_user set lastuse='" . date('Y-m-d G:i:s') . "' where id='" . $result[0]['id'] . "'";
            $this->db->query($sql);
            $sql = "SELECT modulename,controllername,btnrights FROM sys_conf s,sys_rights r WHERE s.id=r.uriid AND r.userid='" . $session['userId'] . "'";
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $session['rights'] = json_encode($data);
            $this->session->set_userdata('sessioninfo', $session);
        }
        redirect(site_url());
    }

    public
    function logingo()
    {
        $session = array();
        $username = $_POST['username'];
        $password = $_POST['passeword'];
        $password = md5($_POST['passeword']);
        $vercode = strtoupper($_POST['vercode']);
        if (!empty($_SESSION['oa_seKey']['code'])) {
            $code = $_SESSION['oa_seKey']['code'];
        } else {
            $code = "";
        }
        if ($vercode == $code) {
            $query = $this->db->get_where('sys_user', array('username' => $username, 'password' => $password));
            $result = $query->result_array();
            if (sizeof($result) == 1) {
                $session['isLogin'] = true;
                $session['userId'] = $result[0]['id'];
                $session["loginName"] = $result[0]["username"];
                $session["fullName"] = $result[0]["fullname"];
                $session["accounttype"] = $result[0]["accounttype"];
                $session["ip"] = $_SERVER["REMOTE_ADDR"];
                $session["user_agent"] = $_SERVER["HTTP_USER_AGENT"];
                $sql = "update sys_user set lastuse='" . date('Y-m-d G:i:s') . "' where id='" . $result[0]['id'] . "'";
                $this->db->query($sql);
                $sql = "SELECT modulename,controllername,btnrights FROM sys_conf s,sys_rights r WHERE s.id=r.uriid AND r.userid='" . $session['userId'] . "'";
                $query = $this->db->query($sql);
                $data = $query->result_array();
                $session['rights'] = json_encode($data);
                $this->session->set_userdata('sessioninfo', $session);
                echo "<script>self.location.reload();</script>";
            } else {
                echo '账号或密码错误！';
            }
        } else {
            echo '验证码错误！';
        }
        exit;
    }

    public
    function forcechange($username = "")
    {
        $data['username'] = $username;
        $this->load->view('login/forceChgPass.php', $data);
    }

    public
    function forcechgpass()
    {
        $this->load->model('public/common_tools', 'tools');
        $params = $this->input->get(NULL, TRUE);
        $params['oldpassword'] = md5($params['oldpassword']);
        $params['newpassword'] = md5($params['newpassword']);
        $params['new2password'] = md5($params['new2password']);
        if ($params['newpassword'] <> $params['new2password']) {
            $this->tools->_JSONRESULT("二次输入的密码不一致!");
        }
        if ($params['oldpassword'] == $params['newpassword']) {
            $this->tools->_JSONRESULT("新密码不能和老密码一致!");
        }
        $result = $this->db->get_where('sys_user', array('username' => $params['username'], 'password' => $params['oldpassword']))->result_array();
        if (sizeof($result) == 1) {
            $this->tools->chgPassword($params['username'], $params['newpassword']);
            $this->tools->_JSONRESULT("密码修改成功!", true);
        } else {
            $this->tools->_JSONRESULT("帐号，密码不正确!");
        }
    }

    public
    function logingonovercode()
    {
        $session = array();
        $username = $_POST['username'];
        $password = $_POST['passeword'];
        $password = md5($_POST['passeword']);
        $query = $this->db->get_where('sys_user', array('username' => $username, 'password' => $password));
        $result = $query->result_array();
        if (sizeof($result) == 1) {
            if (($result[0]['lastchange'] == "") ||
                (strtotime('+3 month', strtotime($result[0]['lastchange'])) < strtotime(date("Y-m-d H:i:s")))
            ) {
                echo "<script>self.location.href='" . site_url() . "/admin/login/forcechange/" . $username . "';</script>";
                exit;
            }
            $session['isLogin'] = true;
            $session['userId'] = $result[0]['id'];
            $session["loginName"] = $result[0]["username"];
            $session["fullName"] = $result[0]["fullname"];
            $session["accounttype"] = $result[0]["accounttype"];
            $session["ip"] = $_SERVER["REMOTE_ADDR"];
            $session["user_agent"] = $_SERVER["HTTP_USER_AGENT"];
            $sql = "update sys_user set lastuse='" . date('Y-m-d G:i:s') . "' where id='" . $result[0]['id'] . "'";
            $this->db->query($sql);
            $sql = "SELECT modulename,controllername,btnrights FROM sys_conf s,sys_rights r WHERE s.id=r.uriid AND r.userid='" . $session['userId'] . "'";
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $session['rights'] = json_encode($data);
            $this->session->set_userdata('sessioninfo', $session);
            echo "<script>self.location.reload();</script>";
        } else {
            echo '账号或密码错误！';
        }
    }

    public
    function test()
    {
        $this->load->model('public/common_centreon', 'centreon');
        $dd = $this->centreon->addContact("test1", md5("password"), "Test1");
        var_dump($dd);
    }

    /**
     * 注销
     */
    public
    function logout()
    {
        $this->session->unset_userdata('sessioninfo');
        redirect('admin/login');
    }

}

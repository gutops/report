<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Users extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->deletemode = 'DEL';
        $this->idmode = 'UUID';   // 表关键字模式  ID 为自增量类型， UUID 为guid模式
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        $data['dicttype'] = $this->dball->getDictData('账号类型');
        return $data;
    }

    /*
   * 在数据操作(create,update,delete)前,切换页面前调用
   * type = create,update,delete
   */
    protected function _beforeDBAct($type, $result)
    {
        $this->load->model("public/Common_centreon","centreon");
        $result = parent::_beforeDBAct($type,$result);
        if (($type == 'create') || ($type == 'edit')) {
            if (strlen($result['password']) <> 32) {
                $result['password'] = MD5($result['password']);
                $this->centreon->chgPasswd($result['username'],$result['password']);
            }
            $config = array("id" => "id", "field" => "username", "table" => "sys_user");
            $this->dball->dbUniqueCheck($type, $result, $config);
        }
        return $result;
    }

    protected function _afterDBAct($type, $result)
    {
        $result = parent::_afterDBAct($type,$result);
        if ($type == "create"){
            $this->load->model("public/Common_centreon","centreon");
            $data = $result['obj'];
            $this->centreon->addContact($data['username'],$data['password'],$data['fullname']);
        }
        return $result;
    }

}

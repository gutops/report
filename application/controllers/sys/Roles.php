<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Roles extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->deletemode = 'DEL';
        $this->idmode = 'UUID';   // 表关键字模式  ID 为自增量类型， UUID 为guid模式
    }

    /*
     *  提供列表页,明细页的下拉框数据准备
     */
    protected function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        if (($this->url_method === 'view') || ($this->url_method === 'edit')) {
            $sql_getBtnDefine = "select s.id,s.name,s.icon,s.btnDefine,r.btnrights,s.project_name,s.bg_color,s.description,r.uriid from sys_conf s ";
            $sql_getBtnDefine .= " left outer join sys_rolerights r on s.id=r.uriid and r.rolesid='" . $data['keyId'] . "' where s.src_url <> 'default' order by s.id";
            $btnDefine = $this->mydb->find($sql_getBtnDefine);
            $btnName = $this->dball->getConfData("按钮名称");
            for ($i = 0; $i < sizeof($btnDefine["obj"]); $i++) {
                $btn = $btnDefine["obj"][$i]["btnDefine"];
                foreach ($btnName as $btnrep)
                    $btn = str_replace($btnrep["name"], $btnrep["value"], $btn);
                $btnDefine["obj"][$i]["btnDefine"] = $btn;
                $btn = $btnDefine["obj"][$i]["btnrights"];
                foreach ($btnName as $btnrep)
                    $btn = str_replace($btnrep["name"], $btnrep["value"], $btn);
                $btnDefine["obj"][$i]["btnrights"] = $btn;
            }
            $data['datalist'] = $btnDefine['obj']; //按钮信息
            $getProjectSql = "SELECT DISTINCT project_name AS projectName FROM sys_conf WHERE project_name IS NOT NULL AND project_name !='' GROUP BY project_name";
            $selectProject = $this->mydb->find($getProjectSql);
            $data['projects'] = $selectProject['obj'];
        }
        if ($this->url_method == "create"){
            $this->detailurl = $this->url_module . '/' . $this->url_model . '_editdetail.php';
        }
        return $data;
    }


    /*
* 在数据操作(create,update,delete)前,切换页面前调用
* type = create,update,delete
*/
    protected function _beforeDBAct($type, $result)
    {
        log_message('debug', '_beforeDBAct()->type : ' . print_r($type, true));
        if ($type === 'edit') {
            $this->mydb->db->delete('sys_rolerights', array('rolesid' => $result[$this->griddefine['keyid']]));
            $uri = array();
            $uridata = array();
            foreach ($_POST as $key => $value) {  //提取所有uri checked的项目
                $ll = explode('_', $key);
                if (sizeof($ll) == 2) {
                    $this->load->helper('guid_helper');
                    $uridata["Id"] = guid();
                    $uridata["rolesid"] = $result['id'];
                    $uridata["uriid"] = $ll[1];
                    $uridata["btnrights"] = "";
                    $uridata["createuser"] = $this->session->userdata['sessioninfo']['userId'];
                    $uridata["createdatetime"] = date('Y-m-d G:i:s');
                    $uri[$ll[1]] = $uridata;
                }
            }
            foreach ($_POST as $key => $value) {  //为每个uri checked的项目处理各个btn
                $ll = explode('_', $key);
                if (sizeof($ll) == 3) {
                    if (isset($uri[$ll[1]]))
                        $uri[$ll[1]]["btnrights"] .= "," . $ll[2];
                }
            }
            $btndata = $this->dball->getConfData("按钮名称");
            foreach ($uri as $arr) {
                $arr["btnrights"] = Ousuclass::arrayreplace($arr["btnrights"], $btndata, "value", "name");
                $this->mydb->setTable("sys_rolerights");
                $this->mydb->setKeyId("Id");
                $this->mydb->addData($arr);
            }
            $result = array();
        }
        log_message('debug', '_beforeDBAct()->params : ' . print_r($result, true));
        return $result;
    }


}

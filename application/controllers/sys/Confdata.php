<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Confdata extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->deletemode = 'DEL';
        $this->idmode = 'UUID';   // 表关键字模式  ID 为自增量类型， UUID 为guid模式
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        $query = $this->db->query("SELECT type as name FROM sys_configdata GROUP BY type");
        $result = $query->result_array();
        $data['dicttype'] = $result;
        return $data;
    }

}

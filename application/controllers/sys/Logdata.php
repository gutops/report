<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Logdata extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->deletemode = 'DEL';
        $this->idmode = 'ID';   // 表关键字模式  ID 为自增量类型， UUID 为guid模式
    }

}

<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Iconman extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->deletemode = 'DEL';
        $this->idmode = 'UUID';   // 表关键字模式  ID 为自增量类型， UUID 为guid模式
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        $data['dicttype'] = $this->dball->getDictData('项目名称');
        $data['dictcolor'] = $this->dball->getConfData('图标颜色');
        $data['dictimg'] = $this->dball->getConfData('图标类型');
        return $data;
    }

    protected function _beforeDBAct($type, $result)
    {
        $result = parent::_beforeDBAct($type, $result);
        if (($type == 'create') || ($type == 'edit')) {
            $config = array("id" => "id", "field" => "name", "table" => "sys_conf");
            $this->dball->dbUniqueCheck($type, $result, $config);
            if ($result['type'] == 'newshortcut'){
                $result['src_url'] = str_replace("32","64",$result['icon']);
            }
        }
        return $result;
    }

    public function _beforeMethod($type, $data)
    {
        $data = parent::_beforeMethod($type, $data);
        if ($type == "page") {
            foreach ($data['aaData'] as $key => $item) {
                $data['aaData'][$key]['icon'] = '<img style="background:blue;" src="' . base_url('assets/img/webQQ/' . $item['icon']) . '">' . $item['icon'];
                if (isset($item['bg_color'])){
                    $data['aaData'][$key]['bg_color'] = '<span style="color:white;background-color:'.$item['bg_color'].';">'.$item['bg_color'].'</span>';
                }
            }
        }
        return $data;
    }

    public function cascade()
    {
        $type = $_POST['type'];
        $val = $_POST['val'];
        $where = " 1=1 ";
        if ($val) {
            $where = $where . " and value='$val'";
        }
        if ($type) {
            $where = $where . " and type='$type'";
        }
        $sql = "SELECT * FROM sys_conf where $where";

        $query = $this->mydb->find($sql);
        $data = $query['obj'];
        echo Ousuclass::all_html_option($data, 'name', 'name');
    }

}

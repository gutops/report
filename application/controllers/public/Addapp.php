<?php
include_once APPPATH . "libraries/Guid.php";

class Addapp extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        // 定义模块和动作
        $this->url_module = $this->uri->segment(1, $this->router->default_controller);
        $this->url_model = $this->uri->segment(2, 'index');
        $this->url_method = $this->uri->segment(3, 'index');
        // 加载数据库定义
        $this->load->database();
    }

    public function index()
    {
        $session = $this->session->userdata('sessioninfo');
        if ($session["loginName"] == "superadmin") {
            $sql = "select id,name,description,icon,type,bg_color,btnDefine,sys_conf.group 
			from sys_conf where src_url!='default' 
			order by sys_conf.group";
            $addright = "insert into sys_rights(Id,userid,uriid,btnrights,createdatetime,modifydatetime,createuser,modifyuser) 
			select uuid() as Id,'" . $session["userId"] . "' as userid, 
			sys_conf.id as uriid,btnDefine as btnrights,now() as createdatetime,now() as modifydatetime,
			'" . $session["fullName"] . "' as createuser,'" . $session["fullName"] . "' as modifyuser 
			from sys_conf 
			where id not in(select sys_conf.id as id 
			from sys_rights,sys_conf
			where userid='" . $session["userId"] . "' and sys_rights.uriid = sys_conf.id)";
            $insert_result = $this->db->query($addright);
        } else {
            $sql = "select sys_conf.id,name,description,icon,type,bg_color,sys_conf.group 
			from sys_conf,sys_rights 
			where src_url!='default' and sys_conf.id=sys_rights.uriid and sys_rights.userid = '" . $session["userId"] . "'
		    order by sys_conf.group";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $sql = "select conf_id from user_conf where user_id = '" . $session["userId"] . "'";
        $query = $this->db->query($sql);
        $exit_list = $query->result_array();
        $data["applist"] = $result;
        $data["baseUrl"] = site_url();
        $data["exitlist"] = json_encode($exit_list);
        $this->load->view($this->url_module . '/' . $this->url_model . '_index.php', $data);
    }

    public function changebtn()
    {
        $type = $_REQUEST["type"];
        $id = $_REQUEST["id"];
        $page = $_REQUEST["page"];
        $row = $_REQUEST["row"];
        $col = $_REQUEST["col"];
        $this->change($type, $id, $page, $row, $col);
    }

    private function change($type, $id, $page, $row, $col)
    {
        $session = $this->session->userdata('sessioninfo');
        $sql = "select name,row_num,col_num from sys_conf where id='" . $id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $gg = new Guid();
        if ("add" == $type) {
            $pos = $this->checkdrop($id, $page, $row, $col);
            if (!$pos) {
                echo false;
                return;
            }
            $pos_x = intval($pos[1]) + 1;
            $pos_y = intval($pos[0]) + 1;
            $sql = "insert into user_conf(id,user_id,conf_id,conf_name,page,pos_x,pos_y,can_show,is_show) 
			values('" . $gg->toString() . "','" . $session["userId"] . "'
			,'" . $id . "','" . $result[0]["name"] . "'," . $page . "," . $pos_x . "," . $pos_y . ",1,1)";
        } else {
            $e_sql = "select conf_id,user_conf.pos_x,user_conf.pos_y,col_num,row_num from user_conf,sys_conf 
			where user_id = '" . $session["userId"] . "' and conf_id ='" . $id . "' and user_conf.conf_id=sys_conf.id";
            $query = $this->db->query($e_sql);
            $e_re = $query->row_array();
            $sql = "delete from user_conf where user_id = '" . $session["userId"] . "' 
					and conf_id='" . $id . "'";
        }
        $this->db->query($sql);
        if ("add" == $type) {
            $sql = "select type,page,src_url,des_url,bg_color,name,col_num,row_num,sys_conf.description,sys_conf.icon as icon,sys_conf.canresize as canresize,user_conf.pos_x as pos_x,user_conf.pos_y as pos_y,sys_conf.id as id,
			sys_conf.pos_x as d_pos_x,sys_conf.pos_y as d_pos_y,sys_conf.width as d_pos_width,sys_conf.height as d_pos_height,sys_conf.div_pos_x as div_pos_x,
			sys_conf.div_pos_y as div_pos_y,sys_conf.div_width as div_width,sys_conf.div_height as div_height 
			from sys_conf,user_conf,sys_rights 
			where user_id='" . $session["userId"] . "' and user_conf.conf_id=sys_conf.id and user_conf.is_show = 1 and user_conf.can_show = 1 
			and sys_rights.userid = user_conf.user_id and sys_rights.uriid = user_conf.conf_id and sys_conf.id='" . $id . "'";
            $query = $this->db->query($sql);
            $re = $query->row_array();
            echo json_encode($re);
        } else {
            if (isset($e_re))
                echo json_encode($e_re);
            else
                echo true;
        }
    }

    private function checkdrop($conf_id, $page, $row_num, $col_num)
    {
        $session = $this->session->userdata('sessioninfo');
        $sql = "select row_num,col_num from sys_conf where id='" . $conf_id . "'";
        $query = $this->db->query($sql);
        $size = $query->row_array();
        $sql = "select row_num,col_num,user_conf.pos_x as pos_x,user_conf.pos_y as pos_y 
		from sys_conf,user_conf 
		where user_id='" . $session["userId"] . "' and user_conf.conf_id = sys_conf.id and page=" . $page;
        $query = $this->db->query($sql);
        $list = $query->result_array();
        $map = array();
        //init map
        for ($i = 0; $i < $row_num; $i++)
            for ($j = 0; $j < $col_num; $j++)
                $map[$i][$j] = 0;
        foreach ($list as $key => $value) {
            $row = intval($value["pos_y"]);
            $row_len = intval($value["row_num"]);
            $col = intval($value["pos_x"]);
            $col_len = intval($value["col_num"]);
            for ($i = $row; $i < ($row + $row_len); $i++)
                for ($j = $col; $j < ($col + $col_len); $j++)
                    $map[$i - 1][$j - 1] = 1;
        }

        //init position
        $row = $size["row_num"];
        $col = $size["col_num"];
        $position = $this->mapcheck($map, $row, $col, $row_num, $col_num);
        return $position;
    }

    private function mapcheck($map, $row, $col, $row_num, $col_num)
    {
        $pos_x_start = $col_num - 1;
        $pos_y_start = 0;
        //loop
        for ($i = 0; $i < $col_num; $i++)
            for ($j = 0; $j < $row_num; $j++) {
                if (($col_num - 1 - $i) < $col)
                    return false;
                $len_row = 0;
                if (0 == $map[$j][$i]) {
                    for ($p_j = $j; $p_j < $row_num; $p_j++) {
                        if (0 == $map[$p_j][$i]) {
                            ++$len_row;
                            if ($len_row == $row) {
                                for ($d_j = $j; $d_j <= $p_j; $d_j++) {
                                    $len_col = 0;
                                    for ($d_i = $i; $d_i < $col_num; $d_i++) {
                                        if (1 == $map[$d_j][$d_i])
                                            break 3;
                                        ++$len_col;
                                        if ($len_col == $col)
                                            return array($j, $i);
                                    }
                                }
                            }

                        } else {
                            break;
                        }
                    }
                }
            }
        return false;
    }
}
<?php

class Tools extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('bootstrap/tools');
        $this->load->database();
    }

    public function modifypwd()
    {
        header('Content-type:text/html;charset=utf-8');
        $this->load->view('sys/modifypwd_datalist.php');
    }

    /**
     * 修改企业密码
     **/
    public function modifypwdupdate()
    {
        $this->load->model("public/Common_tools","tools");
        $this->sessioninfo = $this->session->userdata('sessioninfo');
        $userId = $this->sessioninfo["userId"];
        $oldCompanyPwd = md5($_POST['oldCompanyPwd']);
        $newCompanyPwd = md5($_POST['newCompanyPwd']);
        $modiMess = '';
        $result = $this->db->get_where('sys_user', array('id' => $userId, 'password' => $oldCompanyPwd));
        $result = $result->row_array();
        if (!empty($result['id'])) {
            if ($result['password'] != $newCompanyPwd) {
                $datainfo["password"] = $newCompanyPwd;
                $this->tools->chgPassword($this->sessioninfo['loginName'],$newCompanyPwd);
                $result = 1;
                if ($result == 1) echo 1;
                else echo 2;
            } else echo 3;
        } else echo 4;
    }

    public function cascade()
    {
        $params = $this->input->post(NULL,TRUE);
        if (isset($params['type'])) $this->db->where("type",$params['type']);
        if (isset($params['value'])) $this->db->where("value",$params['value']);
        $data = $this->db->get("sys_dictdata")->result_array();
        echo bs_html_option($data, 'name', 'name');
    }

    public function cascadetree()
    {
        $params = $this->input->post(NULL,TRUE);
        if (isset($params['type'])) $this->db->where("type",$params['value']);
        $data = $this->db->get("sys_dictdata")->result_array();
        echo bs_html_option($data, 'id', 'name');
    }

}

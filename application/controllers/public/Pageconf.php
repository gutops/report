<?php
class Pageconf extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		// 定义模块和动作
		$this->url_module = $this->uri->segment(1,$this->router->default_controller);
		$this->url_model = $this->uri->segment(2,'index');
		$this->url_method = $this->uri->segment(3,'index');
		// 加载数据库定义
		$this->load->database();
                $this->load->library('session');
	}
	public function index()
	{
		$session = $this->session->userdata('sessioninfo');
		$sql = "select page_num,page_name from user_pageconf where user_id='".$session["userId"]."' order by page_num";
		$page = $this->db->query($sql);
                $page = $page->result_array();
		if (sizeof($page)>0) $page[0]['page_num'] = '默认打开';
		$data["page"] = $page;
		$data["session"] = $session;
		$this->load->view($this->url_module.'/'.$this->url_model.'_index.php',$data);
	}
	
	public function savepageconf()
	{
		$session = $this->session->userdata('sessioninfo');
		$num = explode(",",$_POST["num"]);
		$name = explode(",",$_POST["name"]);
		for($i=0;$i<count($num);$i++)
		{
			if ($num[$i] == '默认打开' ) $num[$i] = 0;
			$sql = "update user_pageconf set page_name='".$name[$i]."' " .
					"where page_num=".$num[$i]." and user_id='".$session["userId"]."'";
			$this->db->query($sql);
		}
	}
	public function deletepageconf()
	{
		$session = $this->session->userdata('sessioninfo');
		$sql = "delete from user_pageconf  where page_num=".$_POST["id"]." and user_id='".$session["userId"]."'";
		$this->db->query($sql);
	}
	public function addpageconf()
	{
		$session = $this->session->userdata('sessioninfo');
		if($_POST["num"] == 'NaN' ) $_POST["num"]=1;
		$sql = "insert into user_pageconf(Id,user_id,page_num) values(UUID(),'".$session["userId"].
		"',".$_POST["num"].")";
		$this->db->query($sql);
	}
}
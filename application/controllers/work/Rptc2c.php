<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Rptc2c extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'ID';
        $this->deletemode = "DEL";
    }

    public function _getListSql()
    {
        $sql = "SELECT ct.*,abc.cardNumber,abc.cardBank,abc.subBank as cardAddress,concat(surname,trueName) as ownerName 
              FROM c2c_transaction ct
              Left Join app_bank_card abc  on ct.customerBankId=abc.id 
              Where 1=1 ";
        return $sql;
    }

    public function page()
    {
        $this->db->close();
        $this->load->database("lockcoin");
        parent::page();
    }

    public function _beforeMethod($type,$data)
    {
        $data = parent::_beforeMethod($type,$data);
        if ($type == "page") {
            foreach($data['aaData'] as $key=>$item){
                if (isset($item['status'])) { //1待审核 2已完成 3以否决
                    if ($item['status'] == '1') {
                        $status = '待审核';
                    } else if ($item['status'] == "2") {
                        $status = "已完成";
                    } else {
                        $status = "已否决";
                    }
                    $data['aaData'][$key]['status'] = $status;
                }
                if (isset($item['transactionType'])) { //1线上充值,2线上提现 3线下充值 4线下取现
                    if ($item['transactionType'] == '1') {
                        $status = '线上充值';
                    } else if ($item['transactionType'] == '2'){
                        $status = '线上提现';
                    } else if ($item['transactionType'] == '3'){
                        $status = '线下充值';
                    } else {
                        $status = '线下取现';
                    }
                    $data['aaData'][$key]['transactionType'] = $status;
                }
            }
        }
        return $data;
    }

    public function  getExcelData($keyid)
    {
        $this->db->close();
        $this->load->database("lockcoin");
        return parent::getExcelData($keyid);
    }

    public function  getExelTitle()
    {
        $this->db->close();
        $this->load->database();
        return parent::getExelTitle();
    }

}

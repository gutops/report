<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Flowcoldcharge extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'id';
        $this->deletemode = "DEL";
    }
    public function _getListSql()
    {
        $sql = "SELECT * FROM lock_flow WHERE type= '冷钱包充币' AND 1=1";
        return $sql;
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        $data['cointype'] = $this->dball->getDictData('币种');
        if ($this->url_method == "create"){
            $data['datalist']['act_time'] = date('Y-m-d G:i:s');
            $data['datalist']['act_type'] = '二级冷钱包';
            $data['datalist']['status'] = '申请充币';
            $data['datalist']['targetname']='不发送';
        }
        return $data;
    }

    public function  _setAddModifyData($datainfo)
    {
        $datainfo = parent::_setAddModifyData($datainfo);
        $datainfo['type'] = '冷钱包充币';
        $datainfo['order_no'] = $this->dball->getNo("COLD".Date("Ymd"),5);
        if (($datainfo['targetname'] == '发送')) {
            $this->load->library("Yunpian");
            $smsconfig = $this->config->item('cfg-system');
            $smsconfig = $smsconfig['sms'];
            $mobile = $smsconfig['mobilecharge'][$datainfo['act_type']];
            $this->yunpian->send($mobile,'【比特热点】请审核从['.$datainfo['act_type']."]充币，充币数量".$datainfo['amount']);
        }
        return $datainfo;
    }

    public function checkcharge($keyid=""){
        if ($keyid === "") {
            redirect($this->url_module . '/' . $this->url_model);
            exit();
        };
        $update = array();
        $update['status'] = '审核充币';
        $update['modifydatetime'] = date('Y-m-d G:i:s');
        $update['modifyuser'] = $this->sessioninfo['userId'];
        $this->db->where('id',$keyid);
        $this->db->update('lock_flow',$update);
        redirect($this->url_module . '/' . $this->url_model);
    }

    public function finishcharge($keyid=""){
        if ($keyid === "") {
            redirect($this->url_module . '/' . $this->url_model);
            exit();
        };
        $update = array();
        $update['status'] = '完成充币';
        $update['oper_time'] = date('Y-m-d G:i:s');
        $update['modifydatetime'] = date('Y-m-d G:i:s');
        $update['modifyuser'] = $this->sessioninfo['userId'];
        $this->db->where('id',$keyid);
        $this->db->update('lock_flow',$update);
        redirect($this->url_module . '/' . $this->url_model);
    }

    public function audit($keyid=""){
        if ($keyid === "") {
            redirect($this->url_module . '/' . $this->url_model);
            exit();
        };
        $update = array();
        $update['status'] = '审计通过';
        $update['modifydatetime'] = date('Y-m-d G:i:s');
        $update['modifyuser'] = $this->sessioninfo['userId'];
        $this->db->where('id',$keyid);
        $this->db->update('lock_flow',$update);
        redirect($this->url_module . '/' . $this->url_model);
    }

}

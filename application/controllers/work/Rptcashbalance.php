<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Rptcashbalance extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'ID';
        $this->deletemode = "DEL";
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        if ($this->session->userdata('rptcashbalance_starttime') <> null){
            $data['starttime'] = $this->session->userdata('rptcashbalance_starttime') ;
        }
        else {
            $data['starttime'] = date("Y-m-d",strtotime("last month"));
        }
        if ($this->session->userdata('rptcashbalance_endtime') <> null){
            $data['endtime'] = $this->session->userdata('rptcashbalance_endtime') ;
        }
        else {
            $data['endtime'] = date("Y-m-d",strtotime("+1 day"));
        }
        return $data;
    }

	public function recalc(){
        $config= $this->input->get(null);
        $seg = $config['starttime'] . '-' . $config['endtime'];
        $starttime = $config['starttime'];
        $endtime = $config['endtime'];
        $this->session->set_userdata('rptcashbalance_starttime',$starttime);
        $this->session->set_userdata('rptcashbalance_endtime',$endtime);
        $dblk = $this->load->database('lockcoin',true);
        // 帐号余额
        $sql = "
            delete from lock_cash_balance";
        $this->db->query($sql);
        $days = round((strtotime($endtime)-strtotime($starttime))/3600/24);
        $data = array();
        $data['TIMESEG'] = $seg;
        $data['CASH_TIME'] = Date("Y-m-d",strtotime("$starttime -1 day"));
        $INITTIME = $data['CASH_TIME'];
        $this->db->insert("lock_cash_balance",$data);
        $datas = array();
        for($i = 0;$i<$days;$i++){
            $data = array();
            $data['TIMESEG'] = $seg;
            $data['CASH_TIME'] = Date("Y-m-d",strtotime("$starttime +$i day"));
            $datas[] = $data;
        }
        $this->db->insert_batch("lock_cash_balance",$datas);
        $sql = "
            select '$INITTIME' as CASH_TIME,sum(transactionMoney) as CASH_BALANCE 
            from app_transaction where `status`=2  and transactionType in (1,3,5) 
               and modified < '$starttime'
            ";
       $data = $dblk->query($sql)->row_array();
       if (count($data)>0){
           $this->db->where("CASH_TIME",$INITTIME);
           $this->db->update("lock_cash_balance",$data);
       }
        $sql = "
            select '$INITTIME' as CASH_TIME,sum(transactionMoney) as CASH_BALANCE 
            from app_transaction where `status`=2  and transactionType in (2,4) 
               and modified < '$starttime'
            ";
        $data = $dblk->query($sql)->row_array();
        if (count($data)>0){
            IF ($data['CASH_BALANCE'] == null) $data['CASH_BALANCE'] = 0;
            $sql = "UPDATE lock_cash_balance SET CASH_BALANCE = CASH_BALANCE - ".$data['CASH_BALANCE']." WHERE CASH_TIME='$INITTIME'";
            $this->db->query($sql);
        }
        $sql = "
            select DATE_FORMAT(modified,'%Y-%m-%d') as CASH_TIME,sum(transactionMoney) as CASH_CHARGE 
            from app_transaction where `status`=2  and transactionType in (1,3,5) 
               and modified >= '$starttime' and modified <'$endtime'
            group by DATE_FORMAT(modified,'%Y-%m-%d')
            ";
        $data = $dblk->query($sql)->result_array();
        if (count($data)>0){
            $this->db->update_batch("lock_cash_balance",$data,"CASH_TIME");
        }
        $sql = "
            select DATE_FORMAT(modified,'%Y-%m-%d') as CASH_TIME,sum(transactionMoney) as CASH_APPLY 
            from app_transaction where `status`=2  and transactionType in (2,4) 
               and modified >= '$starttime' and modified <'$endtime'
            group by DATE_FORMAT(modified,'%Y-%m-%d')
            ";
        $data = $dblk->query($sql)->result_array();
        if (count($data)>0){
            $this->db->update_batch("lock_cash_balance",$data,"CASH_TIME");
        }
        $data = $this->db->query("select * from lock_cash_balance order by CASH_TIME")->result_array();
        $balance = 0;
        foreach($data as $item){
            if($item['CASH_BALANCE'] == 0){
                $balance = $balance + $item['CASH_CHARGE'] - $item['CASH_APPLY'];
                $this->db->query("UPDATE lock_cash_balance SET CASH_BALANCE=".$balance." WHERE ID=".$item["ID"]);
            } else {
                $balance = $item['CASH_BALANCE'];
            }
        }
        $this->_JSONRESULT("计算成功",True);
    }

}

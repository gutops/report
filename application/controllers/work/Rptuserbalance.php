<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Rptuserbalance extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'ID';
        $this->deletemode = "DEL";
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        if ($this->session->userdata('rptuserbalance_starttime') <> null){
            $data['starttime'] = $this->session->userdata('rptuserbalance_starttime') ;
        }
        else {
            $data['starttime'] = date("Y-m-d",strtotime("last month"));
        }
        if ($this->session->userdata('rptuserbalance_endtime') <> null){
            $data['endtime'] = $this->session->userdata('rptuserbalance_endtime') ;
        }
        else {
            $data['endtime'] = date("Y-m-d",strtotime("+1 day"));
        }
        if ($this->session->userdata('rptuserbalance_username') <> null){
            $data['username'] = $this->session->userdata('rptuserbalance_username') ;
        }
        else {
            $data['username'] = "";
        }
        return $data;
    }

	public function recalc(){
        $config= $this->input->get(null);
        $seg = $config['starttime'] . '-' . $config['endtime'];
        $starttime = $config['starttime'];
        $endtime = $config['endtime'];
        $username = $config['username'];
        $this->session->set_userdata('rptuserbalance_starttime',$starttime);
        $this->session->set_userdata('rptuserbalance_endtime',$endtime);
        $this->session->set_userdata('rptuserbalance_username',$username);
        $dblk = $this->load->database('lockcoin',true);
        // 帐号余额
        $sql = "
          select '$seg' as TIMESEG,userName as USERNAME,concat(surname,trueName) as FULLNAME,
          accountNum as ACCOUNT,hotMoney as MONEY_HOT,coldMoney as MONEY_COLD,'CNY' as COIN_CODE
          FROM app_account WHERE userName='$username'
          ";
        $data = $dblk->query($sql)->row_array();
        if (count($data)==0){
            $this->_JSONRESULT("没有找到这个用户");
        }
        $this->db->empty_table("lock_user_balance");
        // 期间充值
        $sql = "
            select userName as USERNAME,SUM(transactionMoney) as COIN_CHARGE from app_transaction 
            where userName='$username' and transactionType in (1,3,5) and status=2 
              and modified>='$starttime' and modified < '$endtime'
            group by userName
            ";
        $data1 = $dblk->query($sql)->row_array();
        if (count($data1)>0){
            $data['COIN_CHARGE']  = $data1['COIN_CHARGE'];
        }
        // 期初充值
        $sql = "
            select userName as USERNAME,SUM(transactionMoney) as COIN_CHARGE from app_transaction 
            where userName='$username' and transactionType in (1,3,5) and status=2 
              and modified<'$starttime' 
            group by userName
            ";
        $data1 = $dblk->query($sql)->row_array();
        if (count($data1)>0){
            $data['COIN_INIT']  = $data1['COIN_CHARGE'];
        }
        // 期间提现
        $sql = "
            select userName as USERNAME,SUM(transactionMoney+fee) as COIN_APPLY from app_transaction 
            where userName='$username' and transactionType in (2,4) and status=2 
              and modified>='$starttime' and modified < '$endtime'
            group by userName
            ";
        $data1 = $dblk->query($sql)->row_array();
        if (count($data1)>0){
            $data['COIN_APPLY']  = $data1['COIN_CHARGE'];
        }
        // 期初提现
        $sql = "
            select userName as USERNAME,SUM(transactionMoney+fee) as COIN_APPLY from app_transaction 
            where userName='$username' and transactionType in (2,4) and status=2 
              and modified<'$starttime' 
            group by userName
            ";
        $data1 = $dblk->query($sql)->row_array();
        if (count($data1)>0){
            $data['COIN_INIT']  =$data['COIN_INIT']  -  $data1['COIN_APPLY'];
        }
        $this->db->insert("lock_user_balance",$data);
        // 初始化其他币
        $sql = "SELECT coinCode From ex_product";
        $product = $dblk->query($sql)->result_array();
        unset($data['MONEY_HOT']);unset($data['MONEY_COLD']);unset($data['ACCOUNT']);
        unset($data['COIN_INIT']);unset($data['COIN_CHARGE']);unset($data['COIN_APPLY']);
        foreach($product as $item){
            $data['COIN_CODE'] = $item['coinCode'];
            $this->db->insert("lock_user_balance",$data);
        }
        // 期间充币
        $sql = "
            select coinCode as COIN_CODE,sum(transactionMoney) as COIN_CHARGE from ex_dm_transaction 
            where customerName='$username' and transactionType=1 and `status`=2
              and modified>='$starttime' and modified<'$endtime'
            group by coinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        if (count($data1)>0){
            $this->db->update_batch("lock_user_balance",$data1,"COIN_CODE");
        }
        // 期初充币
        $sql = "
            select coinCode as COIN_CODE,sum(transactionMoney) as COIN_INIT from ex_dm_transaction 
            where customerName='$username' and transactionType=1 and `status`=2
              and modified<'$starttime'
            group by coinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        if (count($data1)>0){
            $this->db->update_batch("lock_user_balance",$data1,"COIN_CODE");
        }
        // 期间提币
        $sql = "
            select coinCode as COIN_CODE,sum(transactionMoney+fee) as COIN_APPLY from ex_dm_transaction 
            where customerName='$username' and transactionType=2 and `status`=2
              and modified>='$starttime' and modified<'$endtime'
            group by coinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        if (count($data1)>0){
            $this->db->update_batch("lock_user_balance",$data1,"COIN_CODE");
        }
        // 期初提币
        $sql = "
            select coinCode as COIN_CODE,sum(transactionMoney+fee) as COIN_INIT from ex_dm_transaction 
            where customerName='$username' and transactionType=2 and `status`=2
              and modified<'$starttime'
            group by coinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        foreach($data1 as $item){
            $sql = "UPDATE lock_user_balance SET COIN_INIT = COIN_INIT - ".$item['COIN_INIT']." WHERE COIN_CODE='".$item['COIN_CODE']."'";
            $this->db->query($sql);
        }
        // 期初买入
        $sql = "
            select coinCode as COIN_CODE,fixPriceCoinCode as COIN_FIX,sum(transactionCount) as COIN_BUY,sum(transactionSum) as MONEY_BUY 
            from ex_order_info 
            where buyUserName='$username' and  transactionTime<'$starttime'
            group by coinCode,fixPriceCoinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        foreach($data1 as $item){
            $sql = "UPDATE lock_user_balance SET COIN_INIT = COIN_INIT + ".$item['COIN_BUY']." WHERE COIN_CODE='".$item['COIN_CODE']."'";
            $this->db->query($sql);
            $sql = "UPDATE lock_user_balance SET COIN_INIT = COIN_INIT - ".$item['MONEY_BUY']." WHERE COIN_CODE='".$item['COIN_FIX']."'";
            $this->db->query($sql);
        }
        // 期间买入
        $sql = "
            select coinCode as COIN_CODE,sum(transactionCount) as COIN_BUY
            from ex_order_info 
            where buyUserName='$username' and  transactionTime>='$starttime' and transactionTime<'$endtime'
            group by coinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        if (count($data1)>0){
            $this->db->update_batch("lock_user_balance",$data1,"COIN_CODE");
        }
        // 期初卖出
        $sql = "
            select coinCode as COIN_CODE,fixPriceCoinCode as COIN_FIX,sum(transactionCount) as COIN_SELL ,sum(transactionSum) as MONEY_SELL 
            from ex_order_info 
            where sellUserName='$username' and  transactionTime<'$starttime'
            group by coinCode,fixPriceCoinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        foreach($data1 as $item){
            $sql = "UPDATE lock_user_balance SET COIN_INIT = COIN_INIT - ".$item['COIN_SELL']." WHERE COIN_CODE='".$item['COIN_CODE']."'";
            $this->db->query($sql);
            $sql = "UPDATE lock_user_balance SET COIN_INIT = COIN_INIT + ".$item['MONEY_SELL']." WHERE COIN_CODE='".$item['COIN_FIX']."'";
            $this->db->query($sql);
        }
        // 期间卖出
        $sql = "
            select coinCode as COIN_CODE,sum(transactionCount) as COIN_SELL 
            from ex_order_info 
            where sellUserName='$username' and  transactionTime>='$starttime' and transactionTime<'$endtime'
            group by coinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        if (count($data1)>0){
            $this->db->update_batch("lock_user_balance",$data1,"COIN_CODE");
        }
        // 期间买入-FIX
        $sql = "
            select fixPriceCoinCode as COIN_CODE,sum(transactionCount) as COIN_BUY,sum(transactionSum) as MONEY_BUY 
            from ex_order_info 
            where buyUserName='$username' and  transactionTime>='$starttime' and transactionTime<'$endtime'
            group by fixPriceCoinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        foreach($data1 as $item){
            $sql = "UPDATE lock_user_balance SET COIN_SELL = COIN_SELL + ".$item['MONEY_BUY']." WHERE COIN_CODE='".$item['COIN_CODE']."'";
            $this->db->query($sql);
        }
        // 期间卖出-FIX
        $sql = "
            select fixPriceCoinCode as COIN_CODE,sum(transactionCount) as COIN_SELL,sum(transactionSum) as MONEY_SELL 
            from ex_order_info 
            where sellUserName='$username' and  transactionTime>='$starttime' and transactionTime<'$endtime'
            group by fixPriceCoinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        foreach($data1 as $item){
            $sql = "UPDATE lock_user_balance SET COIN_BUY = COIN_BUY + ".$item['MONEY_SELL']." WHERE COIN_CODE='".$item['COIN_CODE']."'";
            $this->db->query($sql);
        }
        // 委托卖
        $sql = "
            select coinCode as COIN_CODE,sum(surplusEntrustCount) as COIN_COLD_AMOUNT
            from ex_entrust 
            where userName='$username' and type=2 and status in (0,1)
            group by coinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        foreach($data1 as $item){
            $sql = "UPDATE lock_user_balance SET COIN_COLD_AMOUNT = COIN_COLD_AMOUNT + ".$item['COIN_COLD_AMOUNT']." WHERE COIN_CODE='".$item['COIN_CODE']."'";
            $this->db->query($sql);
        }
        // 委托买
        $sql = "
            select fixPriceCoinCode as COIN_CODE,sum(entrustSum - transactionSum) as COIN_COLD_AMOUNT
            from ex_entrust 
            where userName='$username'and type=1 and status in (0,1)
            group by fixPriceCoinCode
            ";
        $data1 = $dblk->query($sql)->result_array();
        foreach($data1 as $item){
            $sql = "UPDATE lock_user_balance SET COIN_COLD_AMOUNT = COIN_COLD_AMOUNT + ".$item['COIN_COLD_AMOUNT']." WHERE COIN_CODE='".$item['COIN_CODE']."'";
            $this->db->query($sql);
        }
        // 计算币余额
        $sql = "SELECT * FROM lock_user_balance";
        $data = $this->db->query($sql)->result_array();
        foreach($data as $key=>$item){
            $data[$key]['COIN_AMOUNT'] = $item['COIN_INIT'] + $item['COIN_CHARGE'] - $item['COIN_APPLY'] + $item['COIN_BUY'] - $item['COIN_SELL'];
        }
        $this->db->update_batch("lock_user_balance",$data,"ID");
        // 提取用户币帐号余额
        $sql = "
            select coinCode as COIN_CODE,hotMoney as COIN_HOT,coldMoney as COIN_COLD 
            from ex_digitalmoney_account where userName='$username'
            ";
        $data1 = $dblk->query($sql)->result_array();
        if (count($data1)>0){
            $this->db->update_batch("lock_user_balance",$data1,"COIN_CODE");
        }
        $this->_JSONRESULT("计算成功",True);
    }

    public function updateamount($keyid=""){
        if ($keyid === "") {
            redirect($this->url_module . '/' . $this->url_model);
            exit();
        };
        $sql = "SELECT * FROM lock_user_balance where ID=".$keyid;
        $data = $this->db->query($sql)->result_array();
        if (sizeof($data) == 0) {
            $this->_JSONRESULT("没有找到对应记录!");
        }
        $data= $data[0];
        $dblk = $this->load->database('lockcoin',true);
        $sql = "UPDATE ex_digitalmoney_account SET hotMoney =  ".$data['COIN_AMOUNT']." - coldMoney WHERE userName='".$data['USERNAME']."' and coinCode='".$data['COIN_CODE']."'";
        $dblk->query($sql);
        $this->_JSONRESULT("账户余额更新完成",true);
    }

    public function updatecold($keyid=""){
        if ($keyid === "") {
            redirect($this->url_module . '/' . $this->url_model);
            exit();
        };
        $sql = "SELECT * FROM lock_user_balance where ID=".$keyid;
        $data = $this->db->query($sql)->result_array();
        if (sizeof($data) == 0) {
            $this->_JSONRESULT("没有找到对应记录!");
        }
        $data= $data[0];
        $dblk = $this->load->database('lockcoin',true);
        $sql = "UPDATE ex_digitalmoney_account SET coldMoney =  ".$data['COIN_COLD_AMOUNT']." WHERE userName='".$data['USERNAME']."' and coinCode='".$data['COIN_CODE']."'";
        $dblk->query($sql);
        $this->_JSONRESULT("账户冻结数量更新完成",true);
    }

}

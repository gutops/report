<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Rptlockbalance extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'ID';
        $this->deletemode = "DEL";
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        if ($this->session->userdata('rptlockbalance_starttime') <> null){
            $data['starttime'] = $this->session->userdata('rptlockbalance_starttime') ;
        }
        else {
            $data['starttime'] = date("Y-m-d",strtotime("last month"));
        }
        if ($this->session->userdata('rptlockbalance_endtime') <> null){
            $data['endtime'] = $this->session->userdata('rptlockbalance_endtime') ;
        }
        else {
            $data['endtime'] = date("Y-m-d",strtotime("+1 day"));
        }
        return $data;
    }
    public function page(){
        $this->db->close();
        $this->load->database("lockcoin");
        parent::page();
    }
	public function recalc(){
        $config= $this->input->get(null);
        $seg = $config['starttime'] . '-' . $config['endtime'];
        $starttime = $config['starttime'];
        $endtime = $config['endtime'];
        $this->session->set_userdata('rptlockbalance_starttime',$starttime);
        $this->session->set_userdata('rptlockbalance_endtime',$endtime);
        $dblk = $this->load->database('lockcoin',true);
        // 帐号余额
        $sql = "
            delete from lock_lock_balance";
        $dblk->query($sql);
        $sql = "
            insert into lock_lock_balance(USERNAME,ACCOUNT,FULLNAME,COIN_HOT,COIN_COLD,COIN_CODE,TIMESEG)
              select userName as USERNAME,accountNUM as ACCOUNT,concat(surname,trueName) as FULLNAME,
                          hotMoney as COIN_HOT,coldMoney as COIN_COLD,'LOCK' as COIN_CODE,
                          '$seg' as TIMESEG
                        from ex_digitalmoney_account where coinCode='LOCK'";
        $dblk->query($sql);
        $sql = "
            update lock_lock_balance llb,(
                        select customerName as USERNAME,sum(transactionMoney-fee) as COIN_INIT from ex_dm_transaction 
                        where transactionType=1 and `status`=2 and coinCode='LOCK'
                          and modified<'$starttime'
                        group by customerName
            ) t set llb.COIN_INIT = t.COIN_INIT WHERE llb.USERNAME=t.USERNAME";
        $dblk->query($sql);
        $sql = "
            update lock_lock_balance llb,(
                        select customerName as USERNAME,sum(transactionMoney-fee) as COIN_CHARGE from ex_dm_transaction 
                        where transactionType=1 and `status`=2 and coinCode='LOCK'
                          and modified>='$starttime' and modified<'$endtime'
                        group by customerName
            ) t set llb.COIN_CHARGE = t.COIN_CHARGE WHERE llb.USERNAME=t.USERNAME";
        $dblk->query($sql);
        $sql = "
            update lock_lock_balance llb,(
                        select customerName as USERNAME,sum(transactionMoney-fee) as COIN_INIT from ex_dm_transaction 
                        where transactionType=2 and `status`=2 and coinCode='LOCK'
                          and modified<'$starttime'
                        group by customerName
            ) t set llb.COIN_INIT = llb.COIN_INIT - t.COIN_INIT WHERE llb.USERNAME=t.USERNAME";
        $dblk->query($sql);
        $sql = "
            update lock_lock_balance llb,(
                        select customerName as USERNAME,sum(transactionMoney-fee) as COIN_APPLY from ex_dm_transaction 
                        where transactionType=2 and `status`=2 and coinCode='LOCK'
                          and modified>='$starttime' and modified<'$endtime'
                        group by customerName
            ) t set llb.COIN_APPLY = t.COIN_APPLY WHERE llb.USERNAME=t.USERNAME";
        $dblk->query($sql);
        $sql = "
            update lock_lock_balance llb,(
                        select buyUserName as USERNAME,sum(transactionCount) as COIN_BUY 
                        from ex_order_info 
                        where coinCode = 'LOCK' and transactionTime<'$starttime'  
                        group by buyUserName
            ) t set llb.COIN_INIT =  llb.COIN_INIT + t.COIN_BUY WHERE llb.USERNAME=t.USERNAME";
        $dblk->query($sql);
        $sql = "
            update lock_lock_balance llb,(
                        select sellUserName as USERNAME,sum(transactionSum - transactionSellFee) as COIN_SELL
                        from ex_order_info 
                        where fixPriceCoinCode = 'LOCK' and transactionTime<'$starttime'  
                        group by sellUserName
            ) t set llb.COIN_INIT =  llb.COIN_INIT - t.COIN_SELL WHERE llb.USERNAME=t.USERNAME";
        $dblk->query($sql);
        $sql = "
            update lock_lock_balance llb,(
                        select buyUserName as USERNAME,sum(transactionCount) as COIN_BUY 
                        from ex_order_info 
                        where coinCode = 'LOCK' and transactionTime>='$starttime' and transactionTime<'$endtime'  
                        group by buyUserName
            ) t set llb.COIN_BUY =  t.COIN_BUY WHERE llb.USERNAME=t.USERNAME";
        $dblk->query($sql);
        $sql = "
            update lock_lock_balance llb,(
                        select sellUserName as USERNAME,sum(transactionSum - transactionSellFee) as COIN_SELL 
                        from ex_order_info 
                        where fixPriceCoinCode = 'LOCK' and transactionTime>='$starttime' and transactionTime<'$endtime'  
                        group by sellUserName
            ) t set llb.COIN_SELL =  t.COIN_SELL WHERE llb.USERNAME=t.USERNAME";
        $dblk->query($sql);
        $sql = "
            UPDATE lock_lock_balance SET COIN_AMOUNT = COIN_INIT+COIN_CHARGE-COIN_APPLY+COIN_BUY-COIN_SELL
         ";
        $dblk->query($sql);
        $sql ="
            insert into lock_lock_balance(TIMESEG,USERNAME,COIN_CODE,COIN_INIT,COIN_CHARGE,
              COIN_APPLY,COIN_BUY,COIN_SELL,COIN_AMOUNT,COIN_HOT,COIN_COLD)
            select TIMESEG,'TOTAL' as USERNAME,'LOCK' as COIN_CODE,
              SUM(COIN_INIT) as COIN_INIT,SUM(COIN_CHARGE) as COIN_CHARGE,
              SUM(COIN_APPLY) as COIN_APPLY,SUM(COIN_BUY) as COIN_BUY,
              SUM(COIN_SELL) as COIN_SELL,SUM(COIN_AMOUNT) as COIN_AMOUNT,
              SUM(COIN_HOT) as COIN_HOT,SUM(COIN_COLD) as COIN_COLD
            from lock_lock_balance
        ";
        $dblk->query($sql);
        $this->_JSONRESULT("计算成功",True);
    }

}

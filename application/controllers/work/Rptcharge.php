<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Rptcharge extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'ID';
        $this->deletemode = "DEL";
    }

    public function _getListSql()
    {
        $sql = "SELECT * FROM app_transaction WHERE transactionType in(5,3,1) ";
        return $sql;
    }

    public function page(){
        $this->db->close();
        $this->load->database("lockcoin");
		parent::page();
	}

	public function _beforeMethod($type,$data)
    {
        $data = parent::_beforeMethod($type,$data);
        if ($type == "page") {
            foreach($data['aaData'] as $key=>$item){
                if (isset($item['status'])) {
                    if ($item['status'] == '2') {
                        $status = '成功';
                    } else if ($item['status'] == "3") {
                        $status = "失败";
                    } else {
                        $status = "充值中";
                    }
                    $data['aaData'][$key]['status'] = $status;
                }
                if (isset($item['transactionType'])) {
                    if ($item['transactionType'] == '3') {
                        $status = '线下充值';
                    }
                    $data['aaData'][$key]['transactionType'] = $status;
                }
            }
        }
        return $data;
    }

}

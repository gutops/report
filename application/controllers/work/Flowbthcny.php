<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Flowbthcny extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'id';
        $this->deletemode = "DEL";
    }
    public function _getListSql()
    {
        $sql = "SELECT * FROM lock_flow WHERE type= 'BTH充值CNY' AND 1=1";
        return $sql;
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        if ($this->url_method == "create"){
            $data['datalist']['coincode']= 'BTH';
        }
        return $data;
    }

    public function  _setAddModifyData($datainfo)
    {
        $datainfo = parent::_setAddModifyData($datainfo);
        $datainfo['type'] = 'BTH充值CNY';
        $datainfo['order_no'] = $this->dball->getNo("BTHCNY".Date("Ymd"),5);
        $datainfo['status'] = '出纳对帐';
        return $datainfo;
    }

    public function usdtimport($keyid=""){
        if ($keyid === "") {
            echo "<script>alert('没有选择记录！');window.location.replace('".site_url().$this->url_module.'/'.$this->url_model."');</script>";
            exit();
        };
        $data = $this->db->query("SELECT file From lock_flow WHERE id=".$keyid)->row_array();
        if (null == $data['file']){
            echo "<script>alert('请先点击【修改】，添加收款凭证图片');window.location.replace('".site_url().$this->url_module.'/'.$this->url_model."');</script>";
            exit;
        }
        $update = array();
        $update['status'] = 'USDT转入';
        $update['oper_time'] = date('Y-m-d G:i:s');
        $update['modifydatetime'] = date('Y-m-d G:i:s');
        $update['modifyuser'] = $this->sessioninfo['userId'];
        $this->db->where('id',$keyid);
        $this->db->update('lock_flow',$update);
        redirect($this->url_module . '/' . $this->url_model);
    }

   public function companyaudit($keyid=""){
        if ($keyid === "") {
            redirect($this->url_module . '/' . $this->url_model);
            exit();
        };
        $update = array();
        $update['status'] = '审核通过';
        $update['modifydatetime'] = date('Y-m-d G:i:s');
        $update['modifyuser'] = $this->sessioninfo['userId'];
        $this->db->where('id',$keyid);
        $this->db->update('lock_flow',$update);
        redirect($this->url_module . '/' . $this->url_model);
    }

}

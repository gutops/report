<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Flowlockwithdraw extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'id';
        $this->deletemode = "DEL";
    }
    public function _getListSql()
    {
        $sql = "SELECT * FROM lock_flow WHERE type= 'LOCKCOIN提现' AND 1=1";
        return $sql;
    }

    public function _getGlobalData($data)
    {
        $data = parent::_getGlobalData($data);
        return $data;
    }

    public function  _setAddModifyData($datainfo)
    {
        $datainfo = parent::_setAddModifyData($datainfo);
        $datainfo['type'] = 'LOCKCOIN提现';
        return $datainfo;
    }

    public function importdata(){
        $this->dblk = $this->load->database("lockcoin",TRUE);
        $data = $this->db->query("select max(act_time) as act_time from lock_flow where type='LOCKCOIN提现'")->row_array();
        if ( null == $data['act_time']){
            $data['act_time'] = '2015-01-01';
        }
        $data = $this->dblk->query("
          select ct.*,abc.surname,abc.trueName,abc.cardName,abc.cardBank,abc.cardNumber,abc.subBank 
          from c2c_transaction ct,app_bank_card abc
          where ct.customerBankId=abc.id and ct.status=1 and ct.transactionType=2 
          and ct.created > '".$data['act_time']."'
          ")->result_array();
        $inserts = array();
        foreach ($data as $item){
            $insert = array();
            $insert['type'] = 'LOCKCOIN提现';
            $insert['coincode'] = $item['coinCode'];
            $insert['username'] = $item['surname'] . $item['trueName'];
            $insert['accountname'] = $item['userName'];
            $insert['targetname'] = $item['businessman'];
            $insert['act_time'] = $item['created'];
            $insert['status'] = "提现导入";
            $insert['memo'] = $item['remark'];
            $insert['random'] = $item['randomNum'];
            $insert['amount'] = $item['transactionMoney'];
            $insert['order_id'] = $item['id'];
            $insert['order_no'] = $item['transactionNum'];
            $insert['bankowner'] = $item['cardName'];
            $insert['bankname'] = $item['cardBank'];
            $insert['bankcardno'] = $item['cardNumber'];
            $insert['bankarea'] = $item['subBank'];
            $insert['is_disabled'] = 0;
            $insert['createdatetime'] = date('Y-m-d G:i:s');
            $insert['createuser'] = $this->sessioninfo['userId'];
            $inserts[] = $insert;
        }
        if (count($inserts) > 0 ){
            $this->db->insert_batch('lock_flow',$inserts);
        }
        redirect($this->url_module . '/' . $this->url_model);
    }

    public function moneysend($keyid=""){
        if ($keyid === "") {
            echo "<script>alert('没有选择记录！');window.location.replace('".site_url().$this->url_module.'/'.$this->url_model."');</script>";
            exit();
        };
        $data = $this->db->query("SELECT file From lock_flow WHERE id=".$keyid)->row_array();
        if (null == $data['file']){
            echo "<script>alert('请先点击【修改】，添加付款凭证图片');window.location.replace('".site_url().$this->url_module.'/'.$this->url_model."');</script>";
            exit;
        }
        $update = array();
        $update['status'] = '出纳转款';
        $update['oper_time'] = date('Y-m-d G:i:s');
        $update['modifydatetime'] = date('Y-m-d G:i:s');
        $update['modifyuser'] = $this->sessioninfo['userId'];
        $this->db->where('id',$keyid);
        $this->db->update('lock_flow',$update);
        redirect($this->url_module . '/' . $this->url_model);
    }

    public function lockaudit($keyid=""){
        if ($keyid === "") {
            redirect($this->url_module . '/' . $this->url_model);
            exit();
        };
        $update = array();
        $update['status'] = '后台审核';
        $update['modifydatetime'] = date('Y-m-d G:i:s');
        $update['modifyuser'] = $this->sessioninfo['userId'];
        $this->db->where('id',$keyid);
        $this->db->update('lock_flow',$update);
        redirect($this->url_module . '/' . $this->url_model);
    }

    public function companyaudit($keyid=""){
        if ($keyid === "") {
            redirect($this->url_module . '/' . $this->url_model);
            exit();
        };
        $update = array();
        $update['status'] = '审核通过';
        $update['modifydatetime'] = date('Y-m-d G:i:s');
        $update['modifyuser'] = $this->sessioninfo['userId'];
        $this->db->where('id',$keyid);
        $this->db->update('lock_flow',$update);
        redirect($this->url_module . '/' . $this->url_model);
    }

}

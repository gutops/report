<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Rptcoinbalance extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'ID';
        $this->deletemode = "DEL";
    }

    public function  calc(){
        $sql = "DELETE FROM lock_coin_balance";
        $this->db->query($sql);
        $dblock = $this->load->database("lockcoin",true);
        $sql = "
            select NOW() as TOTALTIME,a.coinCode as COIN_CODE,
								a.充币-e.`充币` as COIN_CHARGE,
								b.`充币` as COIN_SEND,c.提币 as COIN_APPLY,
                d.hotnums as COIN_USER_OWN, d.coldnums as COIN_USER_COLD,e.`充币` as COIN_MANUAL
            from  (
              select coinCode,sum(transactionMoney) as 充币 from ex_dm_transaction where transactionType=1 and `status`=2 group by coinCode
            ) a left join (
              select coinCode,sum(transactionMoney) as 充币 from ex_dm_transaction where transactionType=1 and `status`=2 and remark like \"注册%\" group by coinCode
            ) b on a.coinCode=b.coinCode
            left join (
              select coinCode,sum(transactionMoney) as 提币 from ex_dm_transaction where transactionType=2 and `status`=2 group by coinCode
            ) c on a.coinCode = c.coinCode
            left join (
              select coinCode,sum(hotMoney) as hotnums,sum(coldMoney) as coldnums from ex_digitalmoney_account where status = 1 group by coinCode
            ) d on a.coinCode = d.coinCode
            left join (
              select coinCode,sum(transactionMoney) as 充币 from ex_dm_transaction where transactionType=1 and `status`=2 and remark like \"手动充币%\" group by coinCode
            ) e on a.coinCode = e.coinCode        ";
        $data = $dblock->query($sql)->result_array();
        $this->db->insert_batch("lock_coin_balance",$data);
        redirect($this->url_module . '/' . $this->url_model);
    }

    public function _beforeMethod($type, $data)
    {
        $data = parent::_beforeMethod($type,$data);
        if ($type == "page"){
            foreach($data['aaData'] as $key=>$item){
                foreach($this->griddefine['initlistfield'] as $fitem){
                    if (isset($fitem['adjust']) && ($fitem['adjust'] == 'rtrim0')&&isset($item[$fitem['fieldname']])){
                        $data['aaData'][$key][$fitem['fieldname']] = rtrim(rtrim($item[$fitem['fieldname']],'0'),'.');
                    }
                }
            }
        }
        return $data;
    }
}

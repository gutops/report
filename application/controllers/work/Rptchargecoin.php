<?php
include_once APPPATH . "libraries/Listdetail_Controller.php";

class Rptchargecoin extends Listdetail_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->idmode = 'ID';
        $this->deletemode = "DEL";
    }

    public function _getListSql()
    {
        $sql = "SELECT * FROM ex_dm_transaction ";
        return $sql;
    }

    public function page(){
        $this->db->close();
        $this->load->database("lockcoin");
		parent::page();
	}

	public function _beforeMethod($type,$data)
    {
        $data = parent::_beforeMethod($type,$data);
        if ($type == "page") {
            foreach($data['aaData'] as $key=>$item){
                if (!isset($item['status'])) continue;
                if ($item['status'] == '2') {$status = '成功';}
                else if($item['status'] == "3" ){$status = "失败";}
                else {$status="充值中";}
                $data['aaData'][$key]['status'] = $status;
                if (!isset($item['transactionType'])) continue;
                if ($item['transactionType'] == '1') {$status = '充币';}
                else {$status="提币";}
                $data['aaData'][$key]['transactionType'] = $status;
            }
        }
        return $data;
    }

}

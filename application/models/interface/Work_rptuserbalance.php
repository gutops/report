<?php
class Work_rptuserbalance  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
		$this->dblk = $this->load->database("lockcoin",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnUpdate' => array( //添加记录
                    'id' => 'btnUpdate',                        //菜单的ID
                    'name' => '更新余额',                        //显示名称
                    'url' => 'updateamount',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'checkMsg' => '是否更新账户余额？',
                    'isCheck' => true,                        //是否显示确认页面
                ),
                'btnSync' => array( //添加记录
                    'id' => 'btnSync',                        //菜单的ID
                    'name' => '更新冻结',                        //显示名称
                    'url' => 'updatecold',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'checkMsg' => '是否更新账户冻结？',
                    'isCheck' => true,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array(),
                'datacheck' => array('btnUpdate','btnSync'),
                'fieldstatus' => array(
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                    'S_CREATED' => array(
                        'name' => 'S_CREATED',
                        'type' => 'date',
                        'dispname' => '时间(始)',
                        'widthcontrol' => '4,6,6',
                    ),
                    'E_CREATED' => array(
                        'name' => 'E_CREATED',
                        'type' => 'date',
                        'dispname' => '时间(至)',
                        'widthcontrol' => '4,6,6',
                    ),
                    'COIN_CODE' => array(
                        'name' => 'COIN_CODE',
                        'type' => 'select',
                        'dispname' => '币种',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getbasedatawithdb($this->dblk,"ex_product","coinCode,name","","DEL"),
                        'field' => 'coinCode,name',
                    ),
                    'USERNAME' => array(
                        'name' => 'USERNAME',
                        'type' => 'text',
                        'dispname' => '用户帐号',
                        'widthcontrol' => '4,6,6',
                    ),
                    'ACCOUNT' => array(
                        'name' => 'ACCOUNT',
                        'type' => 'text',
                        'dispname' => '资金帐号',
                        'widthcontrol' => '4,6,6',
                    ),
                ),
            ),
            'initsearchdefine' => array(
                'S_CREATED' => array(
                    'name' => 'S_CREATED',
                    'type' => 'date',
                    'dispname' => '时间(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_CREATED' => array(
                    'name' => 'E_CREATED',
                    'type' => 'date',
                    'dispname' => '时间(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'USERNAME' => array(
                    'name' => 'USERNAME',
                    'type' => 'text',
                    'dispname' => '用户帐号',
                    'widthcontrol' => '4,6,6',
                ),
               'ACCOUNT' => array(
                    'name' => 'ACCOUNT',
                    'type' => 'text',
                    'dispname' => '资金帐号',
                    'widthcontrol' => '4,6,6',
                ),
 			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'lock_user_balance',
            'keyid' => 'ID',
            'listfield' => array( //显示字段
                'TIMESEG' => array(
                    'fieldname' => 'TIMESEG',
                    'dispname' => '核算时间',
                ),
                'USERNAME' => array(
                    'fieldname' => 'USERNAME',
                    'dispname' => '用户',
                ),
                'FULLNAME' => array(
                    'fieldname' => 'FULLNAME',
                    'dispname' => '姓名',
                ),
                'ACCOUNT' => array(
                    'fieldname' => 'ACCOUNT',
                    'dispname' => '资金帐号',
                ),
                'MONEY_HOT' => array(
                    'fieldname' => 'MONEY_HOT',
                    'dispname' => '可用资金',
                    'adjust' => 'rtrim0',
                ),
                'MONEY_COLD' => array(
                    'fieldname' => 'MONEY_COLD',
                    'dispname' => '冻结资金',
                    'adjust' => 'rtrim0',
                ),
            ),
            'initlistfield' => array(
                'TIMESEG' => array(
                    'fieldname' => 'TIMESEG',
                    'dispname' => '核算时间',
                ),
                'USERNAME' => array(
                    'fieldname' => 'USERNAME',
                    'dispname' => '用户',
                ),
                'FULLNAME' => array(
                    'fieldname' => 'FULLNAME',
                    'dispname' => '姓名',
                ),
                'ACCOUNT' => array(
                    'fieldname' => 'ACCOUNT',
                    'dispname' => '资金帐号',
                ),
                'MONEY_HOT' => array(
                    'fieldname' => 'MONEY_HOT',
                    'dispname' => '可用资金',
                    'adjust' => 'rtrim0',
                ),
                'MONEY_COLD' => array(
                    'fieldname' => 'MONEY_COLD',
                    'dispname' => '冻结资金',
                    'adjust' => 'rtrim0',
                ),
                'COIN_CODE' => array(
                    'fieldname' => 'COIN_CODE',
                    'dispname' => '币种',
                ),
                'COIN_INIT' => array(
                    'fieldname' => 'COIN_INIT',
                    'dispname' => '期初数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_CHARGE' => array(
                    'fieldname' => 'COIN_CHARGE',
                    'dispname' => '充币数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_APPLY' => array(
                    'fieldname' => 'COIN_APPLY',
                    'dispname' => '提币数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_BUY' => array(
                    'fieldname' => 'COIN_BUY',
                    'dispname' => '买入数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_SELL' => array(
                    'fieldname' => 'COIN_SELL',
                    'dispname' => '卖出数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_AMOUNT' => array(
                    'fieldname' => 'COIN_AMOUNT',
                    'dispname' => '币余额数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_COLD_AMOUNT' => array(
                    'fieldname' => 'COIN_COLD_AMOUNT',
                    'dispname' => '冻结数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_HOT' => array(
                    'fieldname' => 'COIN_HOT',
                    'dispname' => '帐号可用币',
                    'adjust' => 'rtrim0',
                ),
                'COIN_COLD' => array(
                    'fieldname' => 'COIN_COLD',
                    'dispname' => '账户冻结币',
                    'adjust' => 'rtrim0',
                ),
            ),
        );
       return $gridinfo;
    }

}


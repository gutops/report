<?php
class Work_rpttrade  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
		$this->dblk = $this->load->database("lockcoin",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
            ),
            'btnStatus' => array(
                'init' => array(),
                'datacheck' => array(),
                'fieldstatus' => array(
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                    'orderNum' => array(
                        'name' => 'orderNum',
                        'type' => 'text',
                        'dispname' => '交易单号',
                        'widthcontrol' => '4,6,6',
                    ),
                    'coinCode' => array(
                        'name' => 'coinCode',
                        'type' => 'select',
                        'dispname' => '币种',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getbasedatawithdb($this->dblk,"ex_product","coinCode,name","","DEL"),
                        'field' => 'coinCode,name',
                    ),
                    'S_transactionTime' => array(
                        'name' => 'S_transactionTime',
                        'type' => 'date',
                        'dispname' => '成交时间(始)',
                        'widthcontrol' => '4,6,6',
                    ),
                    'E_transactionTime' => array(
                        'name' => 'E_transactionTime',
                        'type' => 'date',
                        'dispname' => '成交时间(至)',
                        'widthcontrol' => '4,6,6',
                    ),
                ),
            ),
            'initsearchdefine' => array(
               'orderNum' => array(
                    'name' => 'orderNum',
                    'type' => 'text',
                    'dispname' => '交易单号',
                    'widthcontrol' => '4,6,6',
                ),
                'coinCode' => array(
                    'name' => 'coinCode',
                    'type' => 'select',
                    'dispname' => '币种',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getbasedatawithdb($this->dblk,"ex_product","coinCode,name","","DEL"),
                    'field' => 'coinCode,name',
                ),
                'S_transactionTime' => array(
                    'name' => 'S_transactionTime',
                    'type' => 'date',
                    'dispname' => '成交时间(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_transactionTime' => array(
                    'name' => 'E_transactionTime',
                    'type' => 'date',
                    'dispname' => '成交时间(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'buyEntrusNum' => array(
                    'name' => 'buyEntrusNum',
                    'type' => 'text',
                    'dispname' => '买方委托号',
                    'widthcontrol' => '4,6,6',
                ),
                'sellEntrusNum' => array(
                    'name' => 'sellEntrusNum',
                    'type' => 'text',
                    'dispname' => '卖方委托号',
                    'widthcontrol' => '4,6,6',
                ),
                'buyUserName' => array(
                    'name' => 'buyUserName',
                    'type' => 'text',
                    'dispname' => '买委托人',
                    'widthcontrol' => '4,6,6',
                ),
                'sellUserName' => array(
                    'name' => 'sellUserName',
                    'type' => 'text',
                    'dispname' => '卖委托人',
                    'widthcontrol' => '4,6,6',
                ),
			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'ex_order_info',
            'keyid' => 'id',
            'listfield' => array( //显示字段
                'orderNum'=> array(
                    'fieldname' => 'orderNum',
                    'dispname' => '交易单号',
                ),
                'coinCode'=> array(
                    'fieldname' => 'coinCode',
                    'dispname' => '币种',
                ),
                'transactionPrice'=> array(
                    'fieldname' => 'transactionPrice',
                    'dispname' => '成交单价',
                ),
                'transactionCount'=> array(
                    'fieldname' => 'transactionCount',
                    'dispname' => '成交数量',
                ),
                'transactionTime'=> array(
                    'fieldname' => 'transactionTime',
                    'dispname' => '成交时间',
                    'order' => 'desc',
                ),
                'transactionSum'=> array(
                    'fieldname' => 'transactionSum',
                    'dispname' => '成交额',
                ),
            ),
            'initlistfield' => array(
                'orderNum'=> array(
                    'fieldname' => 'orderNum',
                    'dispname' => '交易单号',
                ),
                'coinCode'=> array(
                    'fieldname' => 'coinCode',
                    'dispname' => '币种',
                ),
                'transactionPrice'=> array(
                    'fieldname' => 'transactionPrice',
                    'dispname' => '成交单价',
                    'adjust' => 'rtrim0',
                ),
                'transactionCount'=> array(
                    'fieldname' => 'transactionCount',
                    'dispname' => '成交数量',
                    'adjust' => 'rtrim0',
                ),
                'transactionTime'=> array(
                    'fieldname' => 'transactionTime',
                    'dispname' => '成交时间',
					'order' => 'desc',
                ),
                'transactionSum'=> array(
                    'fieldname' => 'transactionSum',
                    'dispname' => '成交额',
                    'adjust' => 'rtrim0',
                ),
                'transactionBuyFee'=> array(
                    'fieldname' => 'transactionBuyFee',
                    'dispname' => '买方手续费',
                    'adjust' => 'rtrim0',
                ),
                'transactionSellFee'=> array(
                    'fieldname' => 'transactionSellFee',
                    'dispname' => '卖方手续费',
                    'adjust' => 'rtrim0',
                ),
                'buyEntrustNum'=> array(
                    'fieldname' => 'buyEntrustNum',
                    'dispname' => '买方委托号',
                ),
                'sellEntrustNum'=> array(
                    'fieldname' => 'sellEntrustNum',
                    'dispname' => '卖方委托号',
                ),
                'transactionBuyFeeRate'=> array(
                    'fieldname' => 'transactionBuyFeeRate',
                    'dispname' => '买方手续费率',
                    'adjust' => 'rtrim0',
                ),
                'transactionSellFeeRate'=> array(
                    'fieldname' => 'transactionSellFeeRate',
                    'dispname' => '卖方手续费率',
                    'adjust' => 'rtrim0',
                ),
                'buyUserName' => array(
                    'fieldname' => 'buyUserName',
                    'dispname' => '买委托人',
                ),
                'sellUserName' => array(
                    'fieldname' => 'sellUserName',
                    'dispname' => '卖委托人',
                ),
            ),
        );
       return $gridinfo;
    }

}


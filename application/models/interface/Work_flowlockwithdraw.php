<?php
class Work_flowlockwithdraw  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnModify' => array( //修改记录
                    'id' => 'btnModify',                        //菜单的ID
                    'name' => '修改',                        //显示名称
                    'url' => 'edit',                    //调用路径
                    'icon' => 'icon-pencil',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnView' => array( //明细记录
                    'id' => 'btnView',                        //菜单的ID
                    'name' => '明细',                        //显示名称
                    'url' => 'view',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnImport' => array( //添加记录
                    'id' => 'btnImport',                        //菜单的ID
                    'name' => '导入数据',                        //显示名称
                    'url' => 'importdata',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnCheck' => array( //添加记录
                    'id' => 'btnCheck',                        //菜单的ID
                    'name' => '后台审核',                        //显示名称
                    'url' => 'lockaudit',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnSend' => array( //添加记录
                    'id' => 'btnSend',                        //菜单的ID
                    'name' => '付款',                        //显示名称
                    'url' => 'moneysend',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnAudit' => array( //添加记录
                    'id' => 'btnAudit',                        //菜单的ID
                    'name' => '审计审核',                        //显示名称
                    'url' => 'companyaudit',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array('btnImport'),
                'datacheck' => array('btnModify','btnView'),
                'fieldstatus' => array(
                    'btnSend'=> array('status'=>'后台审核'),
                    'btnCheck'=> array('status'=>'提现导入'),
                    'btnAudit'=> array('status'=>'出纳转款'),
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                ),
            ),
            'initsearchdefine' => array(
                'S_act_time' => array(
                    'name' => 'S_act_time',
                    'type' => 'date',
                    'dispname' => '订单时间(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_act_time' => array(
                    'name' => 'E_act_time',
                    'type' => 'date',
                    'dispname' => '订单时间(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'coincode' => array(
                    'name' => 'coincode',
                    'type' => 'select',
                    'dispname' => '币种',
                    'widthcontrol' => '4,6,6',
                    'data' => array(array('name'=>'USDT'),array('name'=>'LOCK')),
                    'field' => 'name,name',
                ),
                'status' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'dispname' => '状态',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getConfData('提现流程'),
                        'field' => 'name,name',
                ),
                'username' => array(
                    'name' => 'username',
                    'type' => 'text',
                    'dispname' => '用户姓名',
                    'widthcontrol' => '4,6,6',
                ),
 			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'lock_flow',
            'keyid' => 'id',
            'listfield' => array( //显示字段
            ),
            'initlistfield' => array(
                'order_no' => array(
                    'fieldname' => 'order_no',
                    'dispname' => '交易单编号',
                ),
                'username' => array(
                    'fieldname' => 'username',
                    'dispname' => '用户姓名',
                ),
                'accountname' => array(
                    'fieldname' => 'accountname',
                    'dispname' => '用户账号',
                ),
                'coincode' => array(
                    'fieldname' => 'coincode',
                    'dispname' => '币种',
                ),
                'act_time' => array(
                    'fieldname' => 'act_time',
                    'dispname' => '订单时间',
                    'order' => 'desc',
                ),
                'bankowner' => array(
                    'fieldname' => 'bankowner',
                    'dispname' => '持卡人',
                ),
                'bankname' => array(
                    'fieldname' => 'bankname',
                    'dispname' => '银行名称',
                ),
                'bankcardno' => array(
                    'fieldname' => 'bankcardno',
                    'dispname' => '银行卡号',
                ),
                'bankarea' => array(
                    'fieldname' => 'bankarea',
                    'dispname' => '开户行',
                ),
                'status' => array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'targetname' => array(
                    'fieldname' => 'targetname',
                    'dispname' => '市商名称',
                ),
                'memo' => array(
                    'fieldname' => 'memo',
                    'dispname' => '备注',
                    'adjust' => 'rtrim0',
                ),
                'amount' => array(
                    'fieldname' => 'amount',
                    'dispname' => '金额',
                    'adjust' => 'rtrim0',
                ),
                'random' => array(
                    'fieldname' => 'random',
                    'dispname' => '随机数',
                ),
            ),
        );
       return $gridinfo;
    }

}


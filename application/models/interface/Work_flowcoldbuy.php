<?php
class Work_flowcoldbuy  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnAdd' => array( //添加记录
                    'id' => 'btnAdd',                        //菜单的ID
                    'name' => '添加',                        //显示名称
                    'url' => 'create',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnModify' => array( //修改记录
                    'id' => 'btnModify',                        //菜单的ID
                    'name' => '修改',                        //显示名称
                    'url' => 'edit',                    //调用路径
                    'icon' => 'icon-pencil',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnView' => array( //明细记录
                    'id' => 'btnView',                        //菜单的ID
                    'name' => '明细',                        //显示名称
                    'url' => 'view',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnCheck' => array( //添加记录
                    'id' => 'btnCheck',                        //菜单的ID
                    'name' => '审核购币',                        //显示名称
                    'url' => 'checkcharge',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnBuy' => array( //添加记录
                    'id' => 'btnBuy',                        //菜单的ID
                    'name' => '财务打款',                        //显示名称
                    'url' => 'moneygo',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnFinish' => array( //添加记录
                    'id' => 'btnFinish',                        //菜单的ID
                    'name' => '完成购币',                        //显示名称
                    'url' => 'finishcharge',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnAudit' => array( //添加记录
                    'id' => 'btnAudit',                        //菜单的ID
                    'name' => '审计通过',                        //显示名称
                    'url' => 'audit',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array('btnAdd'),
                'datacheck' => array('btnView'),
                'fieldstatus' => array(
                    'btnCheck'=> array('status'=>'申请购币'),
                    'btnBuy'=> array('status'=>'审核购币'),
                    'btnFinish'=> array('status'=>'财务打款'),
                    'btnAudit'=> array('status'=>'完成购币'),
                    'btnModify'=> array('status'=>'申请购币'),
                    'btnModify'=> array('status'=>'财务打款'),
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                ),
            ),
            'initsearchdefine' => array(
                'S_act_time' => array(
                    'name' => 'S_act_time',
                    'type' => 'date',
                    'dispname' => '申请时间(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_act_time' => array(
                    'name' => 'E_act_time',
                    'type' => 'date',
                    'dispname' => '申请时间(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'coincode' => array(
                    'name' => 'coincode',
                    'type' => 'select',
                    'dispname' => '币种',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getDictData('币种'),
                    'field' => 'name,name',
                ),
                'act_type' => array(
                    'name' => 'act_type',
                    'type' => 'select',
                    'dispname' => '冷钱包级别',
                    'widthcontrol' => '4,6,6',
                    'data' => array(array('name'=>'一级冷钱包'),array('name'=>'二级冷钱包')),
                    'field' => 'name,name',
                ),
                'status' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'dispname' => '状态',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getConfData('冷钱包充币'),
                        'field' => 'name,name',
                ),
 			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'lock_flow',
            'keyid' => 'id',
            'listfield' => array( //显示字段
            ),
            'initlistfield' => array(
                'order_no' => array(
                    'fieldname' => 'order_no',
                    'dispname' => '申请单编号',
                ),
                'coincode' => array(
                    'fieldname' => 'coincode',
                    'dispname' => '币种',
                ),
                'act_type' => array(
                    'fieldname' => 'act_type',
                    'dispname' => '冷钱包级别',
                ),
                'act_time' => array(
                    'fieldname' => 'act_time',
                    'dispname' => '申请时间',
                    'order' => 'desc',
                ),
                'status' => array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'memo' => array(
                    'fieldname' => 'memo',
                    'dispname' => '备注',
                    'adjust' => 'rtrim0',
                ),
                'applyamount' => array(
                    'fieldname' => 'applyamount',
                    'dispname' => '申请数量',
                    'adjust' => 'rtrim0',
                ),
                'amount' => array(
                    'fieldname' => 'amount',
                    'dispname' => '购买数量',
                    'adjust' => 'rtrim0',
                ),
                'targetname' => array(
                    'fieldname' => 'targetname',
                    'dispname' => '发送短信',
                ),
                'oper_time' => array(
                    'fieldname' => 'oper_time',
                    'dispname' => '完成时间',
                ),
            ),
        );
       return $gridinfo;
    }

}


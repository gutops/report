<?php
class Work_rptc2c  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
		$this->dblk = $this->load->database("lockcoin",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnExcelExport' => array( //明细记录
                    'id' => 'btnExcelExport',                        //菜单的ID
                    'name' => '导出',                        //显示名称
                    'url' => 'excelexport',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array('btnExcelExport'),
                'datacheck' => array(),
                'fieldstatus' => array(
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                    'userName' => array(
                        'name' => 'userName',
                        'type' => 'text',
                        'dispname' => '账号',
                        'widthcontrol' => '4,6,6',
                    ),
                    'transactionType' => array(
                        'name' => 'transactionType',
                        'type' => 'select',
                        'dispname' => '交易类型',//1线上充值,2线上提现 3线下充值 4线下取现
                        'widthcontrol' => '4,6,6',
                        'data' => array(array('id'=>1,'name'=>'线上充值'),array('id'=>2,'name'=>'线上提现'),array('id'=>3,'name'=>'线下充值'),array('id'=>4,'name'=>'线下取现')),
                        'field' => 'name,id',
                    ),
                    'status' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'dispname' => '状态',//1待审核 2已完成 3以否决
                        'widthcontrol' => '4,6,6',
                        'data' => array(array('id'=>1,'name'=>'待审核'),array('id'=>2,'name'=>'已完成'),array('id'=>3,'name'=>'已否决')),
                        'field' => 'name,id',
                    ),
                    'coinCode' => array(
                        'name' => 'coinCode',
                        'type' => 'select',
                        'dispname' => '币种',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getbasedatawithdb($this->dblk,"ex_product","coinCode,name","","DEL"),
                        'field' => 'coinCode,name',
                    ),
                ),
            ),
            'initsearchdefine' => array(
                'transactioinNum' => array(
                    'name' => 'transactioinNum',
                    'type' => 'text',
                    'dispname' => '交易单号',
                    'widthcontrol' => '4,6,6',
                ),
                'userName' => array(
                    'name' => 'userName',
                    'type' => 'text',
                    'dispname' => '账号',
                    'widthcontrol' => '4,6,6',
                ),
                'transactionType' => array(
                    'name' => 'transactionType',
                    'type' => 'select',
                    'dispname' => '交易类型',//1线上充值,2线上提现 3线下充值 4线下取现
                    'widthcontrol' => '4,6,6',
                    'data' => array(array('id'=>1,'name'=>'线上充值'),array('id'=>2,'name'=>'线上提现'),array('id'=>3,'name'=>'线下充值'),array('id'=>4,'name'=>'线下取现')),
                    'field' => 'name,id',
                ),
                'status' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'dispname' => '状态',//1待审核 2已完成 3以否决
                    'widthcontrol' => '4,6,6',
                    'data' => array(array('id'=>1,'name'=>'待审核'),array('id'=>2,'name'=>'已完成'),array('id'=>3,'name'=>'已否决')),
                    'field' => 'name,id',
                ),
                'coinCode' => array(
                    'name' => 'coinCode',
                    'type' => 'select',
                    'dispname' => '币种',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getbasedatawithdb($this->dblk,"ex_product","coinCode,name","","DEL"),
                    'field' => 'coinCode,coinCode',
                ),
                'S_created' => array(
                    'name' => 'S_created',
                    'type' => 'date',
                    'dispname' => '成交时间(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_created' => array(
                    'name' => 'E_created',
                    'type' => 'date',
                    'dispname' => '成交时间(至)',
                    'widthcontrol' => '4,6,6',
                ),
			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'c2c_transaction',
            'keyid' => 'id',
            'listfield' => array( //显示字段
                'transactionNum'=> array(
                    'fieldname' => 'transactionNum',
                    'dispname' => '交易单号',
                ),
                'userName'=> array(
                    'fieldname' => 'userName',
                    'dispname' => '账号',
                ),
                'transactionType'=> array(
                    'fieldname' => 'transactionType',
                    'dispname' => '交易类型',
                ),
                'transactionMoney'=> array(
                    'fieldname' => 'transactionMoney',
                    'dispname' => '交易总价',
                ),
                'status'=> array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'coinCode'=> array(
                    'fieldname' => 'coinCode',
                    'dispname' => '币种',
                ),
                'created'=> array(
                    'fieldname' =>'created',
                    'dispname' => '成交时间',
                    'order' => 'desc',
                ),
                'cardNumber'=> array(
                    'fieldname' =>'cardNumber',
                    'dispname' => '银行卡号',
                ),
                'ownerName'=> array(
                    'fieldname' =>'ownerName',
                    'dispname' => '持卡人',
                ),
            ),
            'initlistfield' => array(
                'transactionNum'=> array(
                    'fieldname' => 'transactionNum',
                    'dispname' => '交易单号',
                ),
                'userName'=> array(
                    'fieldname' => 'userName',
                    'dispname' => '账号',
                ),
                'transactionType'=> array(
                    'fieldname' => 'transactionType',
                    'dispname' => '交易类型',
                ),
                'transactionMoney'=> array(
                    'fieldname' => 'transactionMoney',
                    'dispname' => '交易总价',
                ),
                'transactionCount'=> array(
                    'fieldname' => 'transactionCount',
                    'dispname' => '交易数量',
                ),
                'transactionPrice'=> array(
                    'fieldname' => 'transactionPrice',
                    'dispname' => '交易单价',
                ),
                'status'=> array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'remark'=> array(
                    'fieldname' => 'remark',
                    'dispname' => '备注',
                ),
                'coinCode'=> array(
                    'fieldname' => 'coinCode',
                    'dispname' => '币种',
                ),
                'created'=> array(
                    'fieldname' =>'created',
                    'dispname' => '成交时间',
					'order' => 'desc',
                ),
                'cardNumber'=> array(
                    'fieldname' =>'cardNumber',
                    'dispname' => '银行卡号',
                ),
                'ownerName'=> array(
                    'fieldname' =>'ownerName',
                    'dispname' => '持卡人',
                ),
                'cardBank'=> array(
                    'fieldname' =>'cardBank',
                    'dispname' => '银行',
                ),
                'cardAddress'=> array(
                    'fieldname' =>'cardAddress',
                    'dispname' => '开户行',
                ),
            ),
        );
       return $gridinfo;
    }

}


<?php
class Work_rptcashbalance  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
		$this->dblk = $this->load->database("lockcoin",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
            ),
            'btnStatus' => array(
                'init' => array('btnCalc'),
                'datacheck' => array(),
                'fieldstatus' => array(
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(),
            ),
            'initsearchdefine' => array(
			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'lock_cash_balance',
            'keyid' => 'ID',
            'listfield' => array( //显示字段
                'TIMESEG'=> array(
                    'fieldname' => 'TIMESEG',
                    'dispname' => '时间段',
                ),
                'CASH_TIME'=> array(
                    'fieldname' => 'CASH_TIME',
                    'dispname' => '统计日期',
                ),
                'CASH_CHARGE'=> array(
                    'fieldname' => 'CASH_CHARGE',
                    'dispname' => '充值金额',
                    'adjust' => 'rtrim0',
                ),
                'CASH_APPLY'=> array(
                    'fieldname' => 'CASH_APPLY',
                    'dispname' => '提现金额',
                    'adjust' => 'rtrim0',
                ),
                'CASH_BALANCE'=> array(
                    'fieldname' => 'CASH_BALANCE',
                    'dispname' => '当日余额',
                    'adjust' => 'rtrim0',
                ),
            ),
            'initlistfield' => array(
                'TIMESEG'=> array(
                    'fieldname' => 'TIMESEG',
                    'dispname' => '时间段',
                ),
                'CASH_TIME'=> array(
                    'fieldname' => 'CASH_TIME',
                    'dispname' => '统计日期',
                ),
                'CASH_CHARGE'=> array(
                    'fieldname' => 'CASH_CHARGE',
                    'dispname' => '充值金额',
                    'adjust' => 'rtrim0',
                ),
                'CASH_APPLY'=> array(
                    'fieldname' => 'CASH_APPLY',
                    'dispname' => '提现金额',
                    'adjust' => 'rtrim0',
                ),
                'CASH_BALANCE'=> array(
                    'fieldname' => 'CASH_BALANCE',
                    'dispname' => '当日余额',
                    'adjust' => 'rtrim0',
                ),
            ),
        );
       return $gridinfo;
    }

}


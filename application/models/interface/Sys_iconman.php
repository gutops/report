<?php

class Sys_iconman extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model('dball', 'dball', TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnAdd' => array( //添加记录
                    'id' => 'btnAdd',                        //菜单的ID
                    'name' => '添加',                        //显示名称
                    'url' => 'create',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnModify' => array( //修改记录
                    'id' => 'btnModify',                        //菜单的ID
                    'name' => '修改',                        //显示名称
                    'url' => 'edit',                    //调用路径
                    'icon' => 'icon-pencil',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnDelete' => array( //删除记录
                    'id' => 'btnDelete',                        //菜单的ID
                    'name' => '删除',                        //显示名称
                    'url' => 'delete',                    //调用路径
                    'icon' => 'icon-trash',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => true,                        //是否显示确认页面
                ),
                'btnView' => array( //明细记录
                    'id' => 'btnView',                        //菜单的ID
                    'name' => '明细',                        //显示名称
                    'url' => 'view',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array('btnAdd'),
                'datacheck' => array('btnModify', 'btnDelete', 'btnView'),
                'fieldstatus' => array(
                    'btnTest' => array('name' => '管理员')
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 3,
                ),
                'items' => array(),
            ),
            'initsearchdefine' => array(
                'project_name' => array(
                    'name' => 'project_name',
                    'type' => 'select',
                    'dispname' => '所属项目',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getConfData("项目名称"),
                    'field' => 'name,name',
                ),
                'type' => array(
                    'name' => 'type',
                    'type' => 'select',
                    'dispname' => '类型',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getConfData('图标类型'),
                    'field' => 'name,name',
                ),
                'src_url' => array(
                    'name' => 'src_url',
                    'type' => 'text',
                    'dispname' => '源URL/图片URL',
                    'widthcontrol' => '4,6,6',
                ),
                'des_url' => array(
                    'name' => 'des_url',
                    'type' => 'text',
                    'dispname' => '目的URL/图片URL',
                    'widthcontrol' => '4,6,6',
                ),
                'login_url' => array(
                    'name' => 'login_url',
                    'type' => 'text',
                    'dispname' => '跨域登陆URL',
                    'widthcontrol' => '4,6,6',
                ),
                'bg_color' => array(
                    'name' => 'bg_color',
                    'type' => 'select',
                    'dispname' => '背景颜色',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getConfData('图标颜色'),
                    'field' => 'name,name',
                ),
                'name' => array(
                    'name' => 'name',
                    'type' => 'text',
                    'dispname' => '名称',
                    'widthcontrol' => '4,6,6',
                ),
                'level' => array(
                    'name' => 'level',
                    'type' => 'text',
                    'dispname' => '等级',
                    'widthcontrol' => '4,6,6',
                ),
                'col_num' => array(
                    'name' => 'col_num',
                    'type' => 'text',
                    'dispname' => '列数',
                    'widthcontrol' => '4,6,6',
                ),
                'row_num' => array(
                    'name' => 'row_num',
                    'type' => 'text',
                    'dispname' => '行数',
                    'widthcontrol' => '4,6,6',
                ),
                'description' => array(
                    'name' => 'description',
                    'type' => 'text',
                    'dispname' => '描述',
                    'widthcontrol' => '4,3,9',
                ),
                'icon' => array(
                    'name' => 'icon',
                    'type' => 'select',
                    'dispname' => '图标路径',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getDirList('/assets/img/webQQ/app/32'),
                    'field' => 'name,id',
                ),
                'pos_x' => array(
                    'name' => 'pos_x',
                    'type' => 'text',
                    'dispname' => '图标(左)',
                    'widthcontrol' => '4,6,6',
                ),
                'pos_y' => array(
                    'name' => 'pos_x',
                    'type' => 'text',
                    'dispname' => '图标(顶)',
                    'widthcontrol' => '4,6,6',
                ),
                'width' => array(
                    'name' => 'width',
                    'type' => 'text',
                    'dispname' => '图标(宽)',
                    'widthcontrol' => '4,6,6',
                ),
                'height' => array(
                    'name' => 'height',
                    'type' => 'text',
                    'dispname' => '图标(高)',
                    'widthcontrol' => '4,6,6',
                ),
                'btnDefine' => array(
                    'name' => 'btnDefine',
                    'type' => 'text',
                    'dispname' => '按钮',
                    'widthcontrol' => '4,6,6',
                ),
                'div_pos_x' => array(
                    'name' => 'div_pos_x',
                    'type' => 'text',
                    'dispname' => '窗口(左)',
                    'widthcontrol' => '4,6,6',
                ),
                'div_pos_y' => array(
                    'name' => 'div_pos_y',
                    'type' => 'text',
                    'dispname' => '窗口(顶)',
                    'widthcontrol' => '4,6,6',
                ),
                'div_width' => array(
                    'name' => 'div_width',
                    'type' => 'text',
                    'dispname' => '窗口(宽)',
                    'widthcontrol' => '4,6,6',
                ),
                'div_height' => array(
                    'name' => 'div_height',
                    'type' => 'text',
                    'dispname' => '窗口(高)',
                    'widthcontrol' => '4,6,6',
                ),
                'canresize' => array(
                    'name' => 'canresize',
                    'type' => 'text',
                    'dispname' => '变化',
                    'widthcontrol' => '4,6,6',
                ),
                'modulename' => array(
                    'name' => 'modulename',
                    'type' => 'text',
                    'dispname' => '模块',
                    'widthcontrol' => '4,6,6',
                ),
                'controllername' => array(
                    'name' => 'controllername',
                    'type' => 'text',
                    'dispname' => '控制器',
                    'widthcontrol' => '4,6,6',
                ),
                'group' => array(
                    'name' => 'group',
                    'type' => 'select',
                    'dispname' => '权限组',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getgroupdata("sys_conf", "project_name", "", "DEL"),
                    'field' => 'project_name,project_name',
                ),
            ),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'sys_conf',
            'keyid' => 'id',
            'listfield' => array( //显示字段
                'name' => array(
                    'fieldname' => 'name',
                    'dispname' => '名称',
                ),
                'type' => array(
                    'fieldname' => 'type',
                    'dispname' => '类型',
                ),
                'project_name' => array(
                    'fieldname' => 'project_name',
                    'dispname' => '名称',
                ),
                'modulename' => array(
                    'fieldname' => 'modulename',
                    'dispname' => '模块名称',
                ),
                'controllername' => array(
                    'fieldname' => 'controllername',
                    'dispname' => '控制器名称',
                ),
                'icon' => array(
                    'fieldname' => 'icon',
                    'dispname' => '显示图标',
                ),
            ),
            'initlistfield' => array(
                'project_name' => array(
                    'fieldname' => 'project_name',
                    'dispname' => '所属项目',
                ),
                'type' => array(
                    'fieldname' => 'type',
                    'dispname' => '类型',
                ),
                'src_url' => array(
                    'fieldname' => 'src_url',
                    'dispname' => '源URL/图片URL',
                ),
                'des_url' => array(
                    'fieldname' => 'des_url',
                    'dispname' => '目的URL/图片URL',
                ),
                'login_url' => array(
                    'fieldname' => 'login_url',
                    'dispname' => '跨域登陆URL',
                ),
                'bg_color' => array(
                    'fieldname' => 'bg_color',
                    'dispname' => '背景颜色',
                ),
                'name' => array(
                    'fieldname' => 'name',
                    'dispname' => '名称',
                ),
                'level' => array(
                    'fieldname' => 'level',
                    'dispname' => '等级',
                ),
                'col_num' => array(
                    'fieldname' => 'col_num',
                    'dispname' => '列数',
                ),
                'row_num' => array(
                    'fieldname' => 'row_num',
                    'dispname' => '行数',
                ),
                'description' => array(
                    'fieldname' => 'description',
                    'dispname' => '描述',
                ),
                'icon' => array(
                    'fieldname' => 'icon',
                    'dispname' => '图标路径',
                ),
                'pos_x' => array(
                    'fieldname' => 'pos_x',
                    'dispname' => '图标(左)',
                ),
                'pos_y' => array(
                    'fieldname' => 'pos_x',
                    'dispname' => '图标(顶)',
                ),
                'width' => array(
                    'fieldname' => 'width',
                    'dispname' => '图标(宽)',
                ),
                'height' => array(
                    'fieldname' => 'height',
                    'dispname' => '图标(高)',
                ),
                'btnDefine' => array(
                    'fieldname' => 'btnDefine',
                    'dispname' => '按钮',
                ),
                'div_pos_x' => array(
                    'fieldname' => 'div_pos_x',
                    'dispname' => '窗口(左)',
                ),
                'div_pos_y' => array(
                    'fieldname' => 'div_pos_y',
                    'dispname' => '窗口(顶)',
                ),
                'div_width' => array(
                    'fieldname' => 'div_width',
                    'dispname' => '窗口(宽)',
                ),
                'div_height' => array(
                    'fieldname' => 'div_height',
                    'dispname' => '窗口(高)',
                ),
                'canresize' => array(
                    'fieldname' => 'canresize',
                    'dispname' => '变化',
                ),
                'modulename' => array(
                    'fieldname' => 'modulename',
                    'dispname' => '模块',
                ),
                'controllername' => array(
                    'fieldname' => 'controllername',
                    'dispname' => '控制器',
                ),
                'group' => array(
                    'fieldname' => 'group',
                    'dispname' => '权限组',
                ),
            ),
        );

        return $gridinfo;
    }

}


<?php
class Work_rptcoinbalance  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
		$this->dblk = $this->load->database("lockcoin",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnCalc' => array( //明细记录
                    'id' => 'btnCalc',                        //菜单的ID
                    'name' => '重算',                        //显示名称
                    'url' => 'calc',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array('btnCalc'),
                'datacheck' => array(),
                'fieldstatus' => array(
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(),
            ),
            'initsearchdefine' => array(
			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'lock_coin_balance',
            'keyid' => 'ID',
            'listfield' => array( //显示字段
                'TOTALTIME'=> array(
                    'fieldname' => 'TOTALTIME',
                    'dispname' => '统计时间',
                ),
                'COIN_CODE'=> array(
                    'fieldname' => 'COIN_CODE',
                    'dispname' => '币种',
                ),
                'COIN_CHARGE'=> array(
                    'fieldname' => 'COIN_CHARGE',
                    'dispname' => '充币',
                ),
                'COIN_SEND'=> array(
                    'fieldname' => 'COIN_SEND',
                    'dispname' => '注册送币',
                ),
                'COIN_APPLY'=> array(
                    'fieldname' => 'COIN_APPLY',
                    'dispname' => '提币',
                ),
            ),
            'initlistfield' => array(
                'TOTALTIME'=> array(
                    'fieldname' => 'TOTALTIME',
                    'dispname' => '统计时间',
                ),
                'COIN_CODE'=> array(
                    'fieldname' => 'COIN_CODE',
                    'dispname' => '币种',
                ),
                'COIN_CHARGE'=> array(
                    'fieldname' => 'COIN_CHARGE',
                    'dispname' => '充币',
                    'adjust' => 'rtrim0',
                ),
                'COIN_SEND'=> array(
                    'fieldname' => 'COIN_SEND',
                    'dispname' => '注册送币',
                    'adjust' => 'rtrim0',
                ),
                'COIN_MANUAL'=> array(
                    'fieldname' => 'COIN_MANUAL',
                    'dispname' => '手动充币',
                    'adjust' => 'rtrim0',
                ),
                'COIN_USER_COLD'=> array(
                    'fieldname' => 'COIN_USER_COLD',
                    'dispname' => '转入冷钱包',
                    'adjust' => 'rtrim0',
                ),
                'COIN_APPLY'=> array(
                    'fieldname' => 'COIN_APPLY',
                    'dispname' => '提币',
                    'adjust' => 'rtrim0',
                ),
                'COIN_BALANCE'=> array(
                    'fieldname' => 'COIN_BALANCE',
                    'dispname' => '归集余额',
                    'adjust' => 'rtrim0',
                ),
                'COIN_PRICE'=> array(
                    'fieldname' => 'COIN_PRICE',
                    'dispname' => '最新单价',
                    'adjust' => 'rtrim0',
                ),
                'COIN_CNY'=> array(
                    'fieldname' => 'COIN_CNY',
                    'dispname' => '人民币',
                    'adjust' => 'rtrim0',
                ),
                'COIN_USER_OWN'=> array(
                    'fieldname' => 'COIN_USER_OWN',
                    'dispname' => '用户持币量热',
                    'adjust' => 'rtrim0',
                ),
                'COIN_NEED_ADD'=> array(
                    'fieldname' => 'COIN_NEED_ADD',
                    'dispname' => '需补充币量',
                    'adjust' => 'rtrim0',
                ),
            ),
        );
       return $gridinfo;
    }

}


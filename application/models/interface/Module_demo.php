<?php
//include_once APPPATH."models/Dball.php";
class Module_demo // !--- MODIFY --- //
{
	public function __construct()
	{
//		$this->CI =&get_instance();
//		$this->CI->load->model('dball','dball',TRUE);
	}
	
	public function getMenuDefine()
	{		
		$menuinfo = array(
	 		'databutton' => array( //数据行按钮
 				'btnNew' => array( //添加记录
                                        'id' => 'btnNew',						//菜单的ID
                                        'name' => '新建',					    //显示名称
                                        'url' => 'filter/new',					//调用路径
                                        'color' => 'yellow',						//颜色
                                        'isCheck' => false,				        //是否显示确认页面
 				),
				'btnWait' => array( //添加记录
					'id' => 'btnWait',						//菜单的ID
					'name' => '等候',					    //显示名称
					'url' => 'filter/wait',					//调用路径	
					'color' => 'yellow',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnProcess' => array( //修改记录
					'id' => 'btnProcess',						//菜单的ID
					'name' => '进行中',					    //显示名称
					'url' => 'filter/process',					//调用路径	
					'color' => 'blue',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnNopay' => array( //删除记录
					'id' => 'btnNopay',						//菜单的ID
					'name' => '未结账',					    //显示名称
					'url' => 'filter/nopay',					//调用路径	
					'color' => 'dark',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnPayOk' => array( //明细记录
					'id' => 'btnPayOk',						//菜单的ID
					'name' => '已结账',					    //显示名称
					'url' => 'filter/payok',					//调用路径	
					'color' => 'green',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnFinish' => array( //明细记录
					'id' => 'btnFinish',						//菜单的ID
					'name' => '分配产值',					    //显示名称
					'url' => 'filter/finish',					//调用路径	
					'color' => 'purple',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnFinishall' => array( //明细记录
					'id' => 'btnFinishall',						//菜单的ID
					'name' => '分配产值完成',					    //显示名称
					'url' => 'filter/finishall',					//调用路径	
					'color' => 'purple',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnAll' => array( //明细记录
					'id' => 'btnAll',						//菜单的ID
					'name' => '查看全部',					    //显示名称
					'url' => 'filter',					//调用路径	
					'color' => 'red',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				
			),
			'dispbutton' => array( //可视操作按钮
				'btnAdd' => array( //添加记录
					'id' => 'btnAdd',						//菜单的ID
					'name' => '添加',					    //显示名称
					'url' => 'create',					//调用路径	
					'icon' => 'icon-plus-sign',			    //图标
					'color' => 'blue',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnModify' => array( //修改记录
					'id' => 'btnModify',						//菜单的ID
					'name' => '修改',					    //显示名称
					'url' => 'edit',					//调用路径	
					'icon' => 'icon-pencil',			    //图标
					'color' => 'blue',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnDelete' => array( //删除记录
					'id' => 'btnDelete',						//菜单的ID
					'name' => '删除',					    //显示名称
					'url' => 'delete',					//调用路径	
					'icon' => 'icon-trash',			    //图标
					'color' => 'blue',						//颜色
					'isCheck' => true,				        //是否显示确认页面
				),
				'btnView' => array( //明细记录
					'id' => 'btnView',						//菜单的ID
					'name' => '明细',					    //显示名称
					'url' => 'view',					//调用路径	
					'icon' => 'icon-book',			    //图标
					'color' => 'blue',						//颜色
					'isCheck' => false,				        //是否显示确认页面
				),
				'btnExcelExport' => array( //明细记录
                                        'id' => 'btnExcelExport',						//菜单的ID
                                        'name' => 'Excel导出',					    //显示名称
                                        'url' => 'ExcelExport',					//调用路径
                                        'icon' => 'icon-book',			    //图标
                                        'color' => 'blue',						//颜色
                                        'checkMsg' => '是否什么',
                                        'isCheck' => true,				        //是否显示确认页面
 				),	
			),
			'btnStatus' => array(
				'init' => array('btnAdd'),                // 初始点亮按钮
				'datacheck' =>array('btnView'),           // 选中记录点亮按钮
				'fieldstatus' => array(                    // 控制按钮和字段对应关系
					'btnDelete'=> array('ACCOUNTTYPE'=>'企业子帐号'),
					'btnModify'=> array('ACCOUNTTYPE'=>'企业子帐号'),
				)
			),
			'searchdefine' => array(
				'init' => array(
					'columns' => 2,                         // 统计下面行数
				),
				'items' => array(
					0 =>array(
						'column' => 0,
						'name' => 'COMPANY_NAME',
						'type' => 'text',
						'dispname' => '企业名称',
						'widthcontrol' => '4,3,9',
                                                'replace' => '1=1',             // 不在套sql，而是在getlistsql中的条件进行替换
					),
					1 =>array(                               //  下拉框选择
						'column' => 0,
						'name' => 'ACCOUNTTYPE',
						'type' => 'select',
						'dispname' => '类型',
						'widthcontrol' => '4,3,9',
						'data' => $this->CI->dball->getDictData('企业帐号类型'), 
                                            // 可以不设置data，需要在_getGlobalData中对$data['menuinfo']['searchdefine'][1]['data']进行数据设置
						'field' => 'name,name',         // 对结果数据去那个字段作为id 和 name
					),
					2 =>array(                               //  下拉框选择
						'column' => 1,
						'name' => 'PLAN_TIME',
						'type' => 'date',
						'dispname' => '预约时间',
						'widthcontrol' => '4,3,9',
					)
				),
			),
		);
		return $menuinfo;
	}

	public function getGridDefine()
	{
		$gridinfo = array(
			'table' => 'xyx_demo',
			'keyid' => 'Id',
			'listfield' => array( //显示字段
				array(
					'fieldname' => 'COMPANY_NAME',
					'dispname' => '企业名称',
					'style' => "width:8px;" ,               //可以追加样式定义
					'class' => "hidden-480",                //可以
                                        'replace' => '1=1',                     // 不在嵌套sql条件，而是在getlistsql中进行替换
				),
				array(
					'fieldname' => 'ACCOUNTTYPE',
					'dispname' => '企业帐号类型',
				),
				array(
					'fieldname' => 'USERNAME',
					'dispname' => '帐号名称',
				),
				array(
					'fieldname' => 'createdatetime',
					'dispname' => '创建时间',
					'shortdisp' => 6,
				),
				array(
					'fieldname' => 'LASTUSE',
					'dispname' => '最后登录时间',
				),
			)
		);
		
		return $gridinfo;
	}

}


<?php
class Sys_users extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model('dball', 'dball', TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnAdd' => array( //添加记录
                    'id' => 'btnAdd',                        //菜单的ID
                    'name' => '添加',                        //显示名称
                    'url' => 'create',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnModify' => array( //修改记录
                    'id' => 'btnModify',                        //菜单的ID
                    'name' => '修改',                        //显示名称
                    'url' => 'edit',                    //调用路径
                    'icon' => 'icon-pencil',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnDelete' => array( //删除记录
                    'id' => 'btnDelete',                        //菜单的ID
                    'name' => '删除',                        //显示名称
                    'url' => 'delete',                    //调用路径
                    'icon' => 'icon-trash',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => true,                        //是否显示确认页面
                ),
                'btnView' => array( //明细记录
                    'id' => 'btnView',                        //菜单的ID
                    'name' => '明细',                        //显示名称
                    'url' => 'view',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array('btnAdd'),
                'datacheck' => array('btnModify', 'btnDelete', 'btnView', 'btnTest'),
                'fieldstatus' => array()
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 2,
                ),
                'items' => array(
                    'username' => array(
                        'name' => 'username',
                        'type' => 'text',
                        'dispname' => '用户名称',
                        'widthcontrol' => '4,6,6',
                    ),
                    'fullname' => array(
                        'name' => 'fullname',
                        'type' => 'text',
                        'dispname' => '用户全名',
                        'widthcontrol' => '4,6,6',
                    ),
                    'accounttype' => array(
                        'name' => 'type',
                        'type' => 'select',
                        'dispname' => '账号类型',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getDictData("账号类型"),
                        'field' => 'name,name',
                    ),
                ),
            ),
            'initsearchdefine' => array(
                'username' => array(
                    'name' => 'username',
                    'type' => 'text',
                    'dispname' => '用户名称',
                    'widthcontrol' => '4,6,6',
                ),
                'fullname' => array(
                    'name' => 'fullname',
                    'type' => 'text',
                    'dispname' => '用户全名',
                    'widthcontrol' => '4,6,6',
                ),
                'accounttype' => array(
                    'name' => 'type',
                    'type' => 'select',
                    'dispname' => '账号类型',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getDictData("账号类型"),
                    'field' => 'name,name',
                ),
                'remarks' => array(
                    'name' => 'remarks',
                    'type' => 'text',
                    'dispname' => '备注',
                    'widthcontrol' => '4,6,6',
                ),
                'S_lastuse' => array(
                    'name' => 'S_lastuse',
                    'type' => 'date',
                    'dispname' => '最后登录(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_lastuse' => array(
                    'name' => 'E_lastuse',
                    'type' => 'date',
                    'dispname' => '最后登录(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'S_lastchange' => array(
                    'name' => 'S_lastchange',
                    'type' => 'date',
                    'dispname' => '密码修改(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_lastchange' => array(
                    'name' => 'E_lastchange',
                    'type' => 'date',
                    'dispname' => '密码修改(至)',
                    'widthcontrol' => '4,6,6',
                ),
            ),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'sys_user',
            'keyid' => 'id',
            'listfield' => array( //显示字段
                'username' => array(
                    'fieldname' => 'username',
                    'dispname' => '用户名称',
                ),
                'fullname' => array(
                    'fieldname' => 'fullname',
                    'dispname' => '用户全名',
                ),
                'accounttype' => array(
                    'fieldname' => 'accounttype',
                    'dispname' => '账号类型',
                ),
            ),
            'initlistfield' => array(
                'username' => array(
                    'fieldname' => 'username',
                    'dispname' => '用户名称',
                ),
                'fullname' => array(
                    'fieldname' => 'fullname',
                    'dispname' => '用户全名',
                ),
                'accounttype' => array(
                    'fieldname' => 'accounttype',
                    'dispname' => '账号类型',
                ),
                'remarks' => array(
                    'fieldname' => 'remarks',
                    'dispname' => '备注',
                ),
                'lastuse' => array(
                    'fieldname' => 'lastuse',
                    'dispname' => '最后登录',
                ),
                'lastchange' => array(
                    'fieldname' => 'lastchange',
                    'dispname' => '密码修改',
                ),
            )
        );

        return $gridinfo;
    }

}


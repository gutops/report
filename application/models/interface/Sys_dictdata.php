<?php
class Sys_dictdata  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnAdd' => array( //添加记录
                    'id' => 'btnAdd',                        //菜单的ID
                    'name' => '添加',                        //显示名称
                    'url' => 'create',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnModify' => array( //修改记录
                    'id' => 'btnModify',                        //菜单的ID
                    'name' => '修改',                        //显示名称
                    'url' => 'edit',                    //调用路径
                    'icon' => 'icon-pencil',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnDelete' => array( //删除记录
                    'id' => 'btnDelete',                        //菜单的ID
                    'name' => '删除',                        //显示名称
                    'url' => 'delete',                    //调用路径
                    'icon' => 'icon-trash',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => true,                        //是否显示确认页面
                ),
                'btnView' => array( //明细记录
                    'id' => 'btnView',                        //菜单的ID
                    'name' => '明细',                        //显示名称
                    'url' => 'view',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array('btnAdd'),
                'datacheck' => array('btnModify', 'btnDelete', 'btnView', 'btnTest'),
                'fieldstatus' => array(
                    'btnTest' => array('name' => '管理员')
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(),
            ),
            'initsearchdefine' => array(
                'type' => array(
                    'name' => 'type',
                    'type' => 'select',
                    'dispname' => '类型',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getDictData('字典类型'),
                    'field' => 'name,name',
                ),
                'seqno' => array(
                    'name' => 'seqno',
                    'type' => 'text',
                    'dispname' => '键值',
                    'widthcontrol' => '4,6,6',
                ),
                'seq' => array(
                    'name' => 'seq',
                    'type' => 'text',
                    'dispname' => '序号',
                    'widthcontrol' => '4,6,6',
                ),
                'name' => array(
                    'name' => 'name',
                    'type' => 'text',
                    'dispname' => '名称',
                    'widthcontrol' => '4,6,6',
                ),
                'value' => array(
                    'name' => 'value',
                    'type' => 'text',
                    'dispname' => '值',
                    'widthcontrol' => '4,6,6',
                ),
                'pid' => array(
                    'name' => 'pid',
                    'type' => 'select',
                    'dispname' => '上级',
                    'widthcontrol' => '4,6,6',
                    'data' =>$this->dball->getbasedata("sys_dictdata", "id,name", "", "DEL"),
                    'field' => 'name,id',
                ),
                'msg1' => array(
                    'name' => 'msg1',
                    'type' => 'text',
                    'dispname' => '文本1',
                    'widthcontrol' => '4,6,6',
                ),
                'msg2' => array(
                    'name' => 'msg2',
                    'type' => 'text',
                    'dispname' => '文本2',
                    'widthcontrol' => '4,6,6',
                ),
                'msg3' => array(
                    'name' => 'msg3',
                    'type' => 'text',
                    'dispname' => '文本3',
                    'widthcontrol' => '4,6,6',
                ),
                'msg4' => array(
                    'name' => 'msg4',
                    'type' => 'text',
                    'dispname' => '文本4',
                    'widthcontrol' => '4,6,6',
                ),
                'msg5' => array(
                    'name' => 'msg5',
                    'type' => 'text',
                    'dispname' => '文本5',
                    'widthcontrol' => '4,6,6',
                ),
                'S_msg6' => array(
                    'name' => 'S_msg6',
                    'type' => 'date',
                    'dispname' => '日期1(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_msg6' => array(
                    'name' => 'E_msg6',
                    'type' => 'date',
                    'dispname' => '日期1(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'S_msg7' => array(
                    'name' => 'S_msg7',
                    'type' => 'date',
                    'dispname' => '日期2(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_msg7' => array(
                    'name' => 'E_msg7',
                    'type' => 'date',
                    'dispname' => '日期2(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'S_msg8' => array(
                    'name' => 'S_msg8',
                    'type' => 'date',
                    'dispname' => '日期3(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_msg8' => array(
                    'name' => 'E_msg8',
                    'type' => 'date',
                    'dispname' => '日期3(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'S_msg9' => array(
                    'name' => 'S_msg9',
                    'type' => 'text',
                    'dispname' => '数值1(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_msg9' => array(
                    'name' => 'E_msg9',
                    'type' => 'text',
                    'dispname' => '数值1(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'S_msg10' => array(
                    'name' => 'S_msg10',
                    'type' => 'text',
                    'dispname' => '数值2(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_msg10' => array(
                    'name' => 'E_msg10',
                    'type' => 'text',
                    'dispname' => '数值2(至)',
                    'widthcontrol' => '4,6,6',
                ),
            ),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'sys_dictdata',
            'keyid' => 'id',
            'listfield' => array( //显示字段
            ),
            'initlistfield' => array(
                'type'=> array(
                    'fieldname' => 'type',
                    'dispname' => '类型',
                    'class' => 'hidden-480',
                ),
                'seqno'=> array(
                    'fieldname' => 'seqno',
                    'dispname' => '键值',
                ),
                'seq'=> array(
                    'fieldname' => 'seq',
                    'dispname' => '编号',
                ),
                'name'=> array(
                    'fieldname' => 'name',
                    'dispname' => '名称',
                ),
                'value'=> array(
                    'fieldname' => 'value',
                    'dispname' => '值',
                ),
                'msg1'=> array(
                    'fieldname' => 'msg1',
                    'dispname' => '文本1',
                ),
                'msg2'=> array(
                    'fieldname' => 'msg2',
                    'dispname' => '文本2',
                ),
                'msg3'=> array(
                    'fieldname' => 'msg3',
                    'dispname' => '文本3',
                ),
                'msg4'=> array(
                    'fieldname' => 'msg4',
                    'dispname' => '文本5',
                ),
                'msg5'=> array(
                    'fieldname' => 'msg5',
                    'dispname' => '文本5',
                ),
                'msg6'=> array(
                    'fieldname' => 'msg6',
                    'dispname' => '日期1',
                ),
                'msg7'=> array(
                    'fieldname' => 'msg7',
                    'dispname' => '日期2',
                ),
                'msg8'=> array(
                    'fieldname' => 'msg8',
                    'dispname' => '日期3',
                ),
                'msg9'=> array(
                    'fieldname' => 'msg9',
                    'dispname' => '数值1',
                ),
                'msg10'=> array(
                    'fieldname' => 'msg10',
                    'dispname' => '数值2',
                ),
            ),
        );
       return $gridinfo;
    }

}


<?php
class Work_rptlockbalance  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
		$this->dblk = $this->load->database("lockcoin",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
            ),
            'btnStatus' => array(
                'init' => array(),
                'datacheck' => array(),
                'fieldstatus' => array(
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                ),
            ),
            'initsearchdefine' => array(
 			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'lock_lock_balance',
            'keyid' => 'ID',
            'listfield' => array( //显示字段
                'TIMESEG' => array(
                    'fieldname' => 'TIMESEG',
                    'dispname' => '核算时间',
                ),
                'USERNAME' => array(
                    'fieldname' => 'USERNAME',
                    'dispname' => '用户',
                ),
                'FULLNAME' => array(
                    'fieldname' => 'FULLNAME',
                    'dispname' => '姓名',
                ),
                'ACCOUNT' => array(
                    'fieldname' => 'ACCOUNT',
                    'dispname' => 'LOCK帐号',
                ),
            ),
            'initlistfield' => array(
                'TIMESEG' => array(
                    'fieldname' => 'TIMESEG',
                    'dispname' => '核算时间',
                ),
                'USERNAME' => array(
                    'fieldname' => 'USERNAME',
                    'dispname' => '用户',
                ),
                'FULLNAME' => array(
                    'fieldname' => 'FULLNAME',
                    'dispname' => '姓名',
                ),
                'ACCOUNT' => array(
                    'fieldname' => 'ACCOUNT',
                    'dispname' => 'LOCK帐号',
                ),
                'COIN_CODE' => array(
                    'fieldname' => 'COIN_CODE',
                    'dispname' => '币种',
                ),
                'COIN_INIT' => array(
                    'fieldname' => 'COIN_INIT',
                    'dispname' => '期初数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_CHARGE' => array(
                    'fieldname' => 'COIN_CHARGE',
                    'dispname' => '充币数量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_APPLY' => array(
                    'fieldname' => 'COIN_APPLY',
                    'dispname' => '提币数量',
                    'adjust' => 'rtrim0',
                ),
                'FIX_CODE' => array(
                    'fieldname' => 'FIX_CODE',
                    'dispname' => '定价币',
                ),
                'COIN_BUY' => array(
                    'fieldname' => 'COIN_BUY',
                    'dispname' => '买入数量',
                    'adjust' => 'rtrim0',
                ),
                'FIX_SELL' => array(
                    'fieldname' => 'FIX_SELL',
                    'dispname' => '卖出定价币量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_SELL' => array(
                    'fieldname' => 'COIN_SELL',
                    'dispname' => '卖出数量',
                    'adjust' => 'rtrim0',
                ),
                'FIX_BUY' => array(
                    'fieldname' => 'FIX_BUY',
                    'dispname' => '买入定价币量',
                    'adjust' => 'rtrim0',
                ),
                'COIN_AMOUNT' => array(
                    'fieldname' => 'COIN_AMOUNT',
                    'dispname' => '币余额',
                    'adjust' => 'rtrim0',
                ),
                'COIN_HOT' => array(
                    'fieldname' => 'COIN_HOT',
                    'dispname' => '帐号可用币',
                    'adjust' => 'rtrim0',
                ),
                'COIN_COLD' => array(
                    'fieldname' => 'COIN_COLD',
                    'dispname' => '账户冻结币',
                    'adjust' => 'rtrim0',
                ),
            ),
        );
       return $gridinfo;
    }

}


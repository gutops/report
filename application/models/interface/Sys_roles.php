<?php

class Sys_roles // !--- MODIFY --- //
{
    public function __construct()
    {
    }


    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnAdd' => array( //添加记录
                    'id' => 'btnAdd',                        //菜单的ID
                    'name' => '添加',                        //显示名称
                    'url' => 'create',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnModify' => array( //修改记录
                    'id' => 'btnModify',                        //菜单的ID
                    'name' => '修改',                        //显示名称
                    'url' => 'edit',                    //调用路径
                    'icon' => 'icon-pencil',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnDelete' => array( //删除记录
                    'id' => 'btnDelete',                        //菜单的ID
                    'name' => '删除',                        //显示名称
                    'url' => 'delete',                    //调用路径
                    'icon' => 'icon-trash',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => true,                        //是否显示确认页面
                ),
                'btnView' => array( //明细记录
                    'id' => 'btnView',                        //菜单的ID
                    'name' => '明细',                        //显示名称
                    'url' => 'view',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                )
            ),

            'btnStatus' => array(
                'init' => array('btnAdd'),
                'datacheck' => array('btnModify', 'btnView', 'btnDelete'),
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 1,
                ),
                'items' => array(
                    'rolename' => array(
                        'name' => 'rolename',
                        'type' => 'text',
                        'dispname' => '角色名称',
                        'widthcontrol' => '4,6,6',
                    ),
                    'remarks' => array(
                        'name' => 'remarks',
                        'type' => 'text',
                        'dispname' => '备注',
                        'widthcontrol' => '4,6,6',
                    ),
                ),
            ),
            'initsearchdefine' => array(
                'rolename' => array(
                    'name' => 'rolename',
                    'type' => 'text',
                    'dispname' => '角色名称',
                    'widthcontrol' => '4,6,6',
                ),
                'remarks' => array(
                    'name' => 'remarks',
                    'type' => 'text',
                    'dispname' => '备注',
                    'widthcontrol' => '4,6,6',
                ),
            ),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'sys_roles',
            'keyid' => 'id',
            'listfield' => array(
                'rolename' => array(
                    'fieldname' => 'rolename',            //需要和sql语句中的字段名完全一致
                    'dispname' => '角色名称',            //中文字段信息
                ),
                'remarks' => array(
                    'fieldname' => 'remarks',            //需要和sql语句中的字段名完全一致
                    'dispname' => '备注',            //中文字段信息
                ),
            ),
            'initlistfield' => array( //显示字段
                'rolename' => array(
                    'fieldname' => 'rolename',            //需要和sql语句中的字段名完全一致
                    'dispname' => '角色名称',            //中文字段信息
                ),
                'remarks' => array(
                    'fieldname' => 'remarks',            //需要和sql语句中的字段名完全一致
                    'dispname' => '备注',            //中文字段信息
                ),
            )
        );

        return $gridinfo;
    }

}


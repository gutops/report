<?php

class Sys_logdata extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model('dball', 'dball', TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
            ),
            'btnStatus' => array(
                'init' => array(),
                'datacheck' => array(),
                'fieldstatus' => array()
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(),
            ),
            'initsearchdefine' => array(
                'name' => array(
                    'name' => 'name',
                    'type' => 'text',
                    'dispname' => '类型',
                    'widthcontrol' => '4,6,6',
                ),
                'table' => array(
                    'name' => 'table',
                    'type' => 'text',
                    'dispname' => '操作表',
                    'widthcontrol' => '4,6,6',
                ),
                'desc' => array(
                    'name' => 'desc',
                    'type' => 'text',
                    'dispname' => 'LOG信息',
                    'widthcontrol' => '4,6,6',
                ),
                'S_logTime' => array(
                    'name' => 'S_logTime',
                    'type' => 'date',
                    'dispname' => '日期(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_logTime' => array(
                    'name' => 'E_logTime',
                    'type' => 'date',
                    'dispname' => '日期(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'operator' => array(
                    'name' => 'operator',
                    'type' => 'text',
                    'dispname' => '操作员',
                    'widthcontrol' => '4,6,6',
                ),
            ),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'sys_log',
            'keyid' => 'Id',
            'listfield' => array( //显示字段
            ),
            'initlistfield' => array(
                'name' => array(
                    'fieldname' => 'name',
                    'dispname' => '类型',
                    'style' => 'width:80px;'
                ),
                'table' => array(
                    'fieldname' => 'table',
                    'dispname' => '操作表',
                    'style' => 'width:100px;'
                ),
                'desc' => array(
                    'fieldname' => 'desc',
                    'dispname' => 'LOG信息',
                ),
                'logTime' => array(
                    'fieldname' => 'logTime',
                    'dispname' => '日期(始)',
                    'order' => 'desc',
                    'style' => 'width:150px;'
                ),
                'operator' => array(
                    'fieldname' => 'operator',
                    'dispname' => '操作员',
                    'style' => 'width:100px;'
                ),
            ),
        );

        return $gridinfo;
    }

}


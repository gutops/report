<?php
class Work_flowbthcny  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnAdd' => array( //添加记录
                    'id' => 'btnAdd',                        //菜单的ID
                    'name' => '添加',                        //显示名称
                    'url' => 'create',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnModify' => array( //修改记录
                    'id' => 'btnModify',                        //菜单的ID
                    'name' => '修改',                        //显示名称
                    'url' => 'edit',                    //调用路径
                    'icon' => 'icon-pencil',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnView' => array( //明细记录
                    'id' => 'btnView',                        //菜单的ID
                    'name' => '明细',                        //显示名称
                    'url' => 'view',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnCheck' => array( //添加记录
                    'id' => 'btnCheck',                        //菜单的ID
                    'name' => 'USDT转入',                        //显示名称
                    'url' => 'usdtimport',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnAudit' => array( //添加记录
                    'id' => 'btnAudit',                        //菜单的ID
                    'name' => '审计审核',                        //显示名称
                    'url' => 'companyaudit',                    //调用路径
                    'icon' => 'icon-plus-sign',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
            ),
            'btnStatus' => array(
                'init' => array('btnAdd'),
                'datacheck' => array('btnModify','btnView'),
                'fieldstatus' => array(
                    'btnCheck'=> array('status'=>'出纳对帐'),
                    'btnAudit'=> array('status'=>'USDT转入'),
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                ),
            ),
            'initsearchdefine' => array(
                'S_act_time' => array(
                    'name' => 'S_act_time',
                    'type' => 'date',
                    'dispname' => '转款时间(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_act_time' => array(
                    'name' => 'E_act_time',
                    'type' => 'date',
                    'dispname' => '转款时间(至)',
                    'widthcontrol' => '4,6,6',
                ),
                'coincode' => array(
                    'name' => 'coincode',
                    'type' => 'select',
                    'dispname' => '币种',
                    'widthcontrol' => '4,6,6',
                    'data' => array(array('name'=>'USDT')),
                    'field' => 'name,name',
                ),
                'status' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'dispname' => '状态',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getConfData('BTHCNY'),
                        'field' => 'name,name',
                ),
                'username' => array(
                    'name' => 'username',
                    'type' => 'text',
                    'dispname' => '用户姓名',
                    'widthcontrol' => '4,6,6',
                ),
                'accountname' => array(
                    'name' => 'accountname',
                    'type' => 'text',
                    'dispname' => '用户账号',
                    'widthcontrol' => '4,6,6',
                ),
 			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'lock_flow',
            'keyid' => 'id',
            'listfield' => array( //显示字段
            ),
            'initlistfield' => array(
                'order_no' => array(
                    'fieldname' => 'order_no',
                    'dispname' => '交易单编号',
                ),
                'username' => array(
                    'fieldname' => 'username',
                    'dispname' => '用户姓名',
                ),
                'accountname' => array(
                    'fieldname' => 'accountname',
                    'dispname' => '用户账号',
                ),
                'coincode' => array(
                    'fieldname' => 'coincode',
                    'dispname' => '币种',
                ),
                'act_time' => array(
                    'fieldname' => 'act_time',
                    'dispname' => '转款时间',
                    'order' => 'desc',
                ),
                'status' => array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'memo' => array(
                    'fieldname' => 'memo',
                    'dispname' => '备注',
                    'adjust' => 'rtrim0',
                ),
                'amount' => array(
                    'fieldname' => 'amount',
                    'dispname' => '金额',
                    'adjust' => 'rtrim0',
                ),
                'random' => array(
                    'fieldname' => 'random',
                    'dispname' => 'USDT',
                ),
                'oper_time' => array(
                    'fieldname' => 'oper_time',
                    'dispname' => '操作时间',
                ),
            ),
        );
       return $gridinfo;
    }

}


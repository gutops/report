<?php
class Work_rptcharge  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
		$this->dblk = $this->load->database("lockcoin",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
            ),
            'btnStatus' => array(
                'init' => array(),
                'datacheck' => array(),
                'fieldstatus' => array(
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                    'S_created' => array(
                        'name' => 'S_created',
                        'type' => 'date',
                        'dispname' => '时间(始)',
                        'widthcontrol' => '4,6,6',
                    ),
                    'E_created' => array(
                        'name' => 'E_created',
                        'type' => 'date',
                        'dispname' => '时间(至)',
                        'widthcontrol' => '4,6,6',
                    ),
                    'transactionNum' => array(
                        'name' => 'transactionNum',
                        'type' => 'text',
                        'dispname' => '交易单号',
                        'widthcontrol' => '4,6,6',
                    ),
                    'userName' => array(
                        'name' => 'userName',
                        'type' => 'text',
                        'dispname' => '用户',
                        'widthcontrol' => '4,6,6',
                    ),
                    'status' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'dispname' => '状态',
                        'widthcontrol' => '4,6,6',
                        'data' => array(array('id'=>2,'name'=>'成功'),array('id'=>3,'name'=>'失败'),array('id'=>1,'name'=>'充值中'),),
                        'field' => 'name,id',
                    ),
                ),
            ),
            'initsearchdefine' => array(
                'S_created' => array(
                    'name' => 'S_created',
                    'type' => 'date',
                    'dispname' => '时间(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_created' => array(
                    'name' => 'E_created',
                    'type' => 'date',
                    'dispname' => '时间(至)',
                    'widthcontrol' => '4,6,6',
                ),
               'transactionNum' => array(
                    'name' => 'transactionNum',
                    'type' => 'text',
                    'dispname' => '交易单号',
                    'widthcontrol' => '4,6,6',
                ),
                'userName' => array(
                    'name' => 'userName',
                    'type' => 'text',
                    'dispname' => '用户',
                    'widthcontrol' => '4,6,6',
                ),
                'transactionMoney' => array(
                    'name' => 'transactionMoney',
                    'type' => 'text',
                    'dispname' => '充值金额',
                    'widthcontrol' => '4,6,6',
                ),
                'status' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'dispname' => '状态',
                    'widthcontrol' => '4,6,6',
                    'data' => array(array('id'=>2,'name'=>'成功'),array('id'=>3,'name'=>'失败'),array('id'=>1,'name'=>'充值中'),),
                    'field' => 'name,id',
                ),
                'transactionType' => array(
                    'name' => 'transactionType',
                    'type' => 'text',
                    'dispname' => '充值类型',
                    'widthcontrol' => '4,6,6',
                ),
                'bankNum' => array(
                    'name' => 'bankNum',
                    'type' => 'text',
                    'dispname' => '银行',
                    'widthcontrol' => '4,6,6',
                ),
                'remark' => array(
                    'name' => 'remark',
                    'type' => 'text',
                    'dispname' => '备注',
                    'widthcontrol' => '4,6,6',
                ),
                'custromerAccountNumber' => array(
                    'name' => 'custromerAccountNumber',
                    'type' => 'text',
                    'dispname' => '银行卡号',
                    'widthcontrol' => '4,6,6',
                ),
                'ourAccountNumber' => array(
                    'name' => 'ourAccountNumber',
                    'type' => 'text',
                    'dispname' => '我方银行卡号',
                    'widthcontrol' => '4,6,6',
                ),
			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'app_transaction',
            'keyid' => 'id',
            'listfield' => array( //显示字段
                'transactionNum' => array(
                    'fieldname' => 'transactionNum',
                    'dispname' => '交易单号',
                ),
                'userName' => array(
                    'fieldname' => 'userName',
                    'dispname' => '用户',
                ),
                'transactionMoney' => array(
                    'fieldname' => 'transactionMoney',
                    'dispname' => '充值金额',
                ),
                'status' => array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'transactionType' => array(
                    'fieldname' => 'transactionType',
                    'dispname' => '充值类型',
                ),
                'bankNum' => array(
                    'fieldname' => 'bankNum',
                    'dispname' => '银行',
                ),
                'remark' => array(
                    'fieldname' => 'remark',
                    'dispname' => '备注',
                ),
                'custromerAccountNumber' => array(
                    'fieldname' => 'custromerAccountNumber',
                    'dispname' => '银行卡号',
                ),
            ),
            'initlistfield' => array(
                'transactionNum' => array(
                    'fieldname' => 'transactionNum',
                    'dispname' => '交易单号',
                ),
                'userName' => array(
                    'fieldname' => 'userName',
                    'dispname' => '用户',
                ),
                'transactionMoney' => array(
                    'fieldname' => 'transactionMoney',
                    'dispname' => '充值金额',
                    'adjust' => 'rtrim0',
                ),
                'status' => array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'transactionType' => array(
                    'fieldname' => 'transactionType',
                    'dispname' => '充值类型',
                ),
                'bankNum' => array(
                    'fieldname' => 'bankNum',
                    'dispname' => '银行',
                ),
                'remark' => array(
                    'fieldname' => 'remark',
                    'dispname' => '备注',
                ),
                'custromerAccountNumber' => array(
                    'fieldname' => 'custromerAccountNumber',
                    'dispname' => '银行卡号',
                ),
                'ourAccountNumber' => array(
                    'fieldname' => 'ourAccountNumber',
                    'dispname' => '我方银行卡号',
                ),
                'fee' => array(
                    'fieldname' => 'fee',
                    'dispname' => '手续费',
                    'adjust' => 'rtrim0',
                ),
                'surname' => array(
                    'fieldname' => 'surname',
                    'dispname' => '姓',
                ),
                'trueName' => array(
                    'fieldname' => 'trueName',
                    'dispname' => '名',
                ),
                'rejectionReason' => array(
                    'fieldname' => 'rejectionReason',
                    'dispname' => '拒绝原因',
                ),
                'created' => array(
                    'fieldname' => 'created',
                    'dispname' => '充值时间',
                    'order' => 'desc',
                ),
            ),
        );
       return $gridinfo;
    }

}


<?php

class Sys_rights extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model('dball', 'dball', TRUE);
    }


    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
                'btnModify' => array( //修改记录
                    'id' => 'btnModify',                        //菜单的ID
                    'name' => '修改',                        //显示名称
                    'url' => 'edit',                    //调用路径
                    'icon' => 'icon-pencil',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnView' => array( //明细记录
                    'id' => 'btnView',                        //菜单的ID
                    'name' => '明细',                        //显示名称
                    'url' => 'view',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                )
            ),
            'otherButton' => array( //可视操作按钮
                'btnAddRight' => array( //明细记录
                    'id' => 'btnAddRight',                        //菜单的ID
                    'name' => '添加权限',                        //显示名称
                    'url' => 'rightsPredefine',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                ),
                'btnRemoveRight' => array( //明细记录
                    'id' => 'btnRemoveRight',                        //菜单的ID
                    'name' => '移除权限',                        //显示名称
                    'url' => 'rightsPredefine',                    //调用路径
                    'icon' => 'icon-book',                //图标
                    'color' => 'blue',                        //颜色
                    'isCheck' => false,                        //是否显示确认页面
                )
            ),
            'btnStatus' => array(
                'init' => array(''),
                'datacheck' => array('btnModify', 'btnView', 'btnAddRight', 'btnRemoveRight'),
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 3,
                ),
                'items' => array(
                    'username' => array(
                        'name' => 'username',
                        'type' => 'text',
                        'dispname' => '用户名称',
                        'widthcontrol' => '4,6,6',
                    ),
                    'fullname' => array(
                        'name' => 'fullname',
                        'type' => 'text',
                        'dispname' => '用户全名',
                        'widthcontrol' => '4,6,6',
                    ),
                    'accounttype' => array(
                        'name' => 'type',
                        'type' => 'select',
                        'dispname' => '账号类型',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getDictData("账号类型"),
                        'field' => 'name,name',
                    ),
                    'remarks' => array(
                        'name' => 'remarks',
                        'type' => 'text',
                        'dispname' => '备注',
                        'widthcontrol' => '4,6,6',
                    ),
                ),
            ),
            'initsearchdefine' => array(
                'username' => array(
                    'name' => 'username',
                    'type' => 'text',
                    'dispname' => '用户名称',
                    'widthcontrol' => '4,6,6',
                ),
                'fullname' => array(
                    'name' => 'fullname',
                    'type' => 'text',
                    'dispname' => '用户全名',
                    'widthcontrol' => '4,6,6',
                ),
                'accounttype' => array(
                    'name' => 'type',
                    'type' => 'select',
                    'dispname' => '账号类型',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getDictData("账号类型"),
                    'field' => 'name,name',
                ),
                'remarks' => array(
                    'name' => 'remarks',
                    'type' => 'text',
                    'dispname' => '备注',
                    'widthcontrol' => '4,6,6',
                ),
            ),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'sys_user',
            'keyid' => 'id',
            'listfield' => array( //显示字段
                'username' => array(
                    'fieldname' => 'username',            //需要和sql语句中的字段名完全一致
                    'dispname' => '用户名字',            //中文字段信息
                ),
                'fullname' => array(
                    'fieldname' => 'fullname',
                    'dispname' => '用户全名',
                ),
                'remarks' => array(
                    'fieldname' => 'remarks',
                    'dispname' => '备注'
                ),
                'accounttype' => array(
                    'fieldname' => 'accounttype',            //需要和sql语句中的字段名完全一致
                    'dispname' => '账号类型',            //中文字段信息
                )
            ),
            'initlistfield' => array(
                'username' => array(
                    'fieldname' => 'username',            //需要和sql语句中的字段名完全一致
                    'dispname' => '用户名字',            //中文字段信息
                ),
                'fullname' => array(
                    'fieldname' => 'fullname',
                    'dispname' => '用户全名',
                ),
                'remarks' => array(
                    'fieldname' => 'remarks',
                    'dispname' => '备注'
                ),
                'accounttype' => array(
                    'fieldname' => 'accounttype',            //需要和sql语句中的字段名完全一致
                    'dispname' => '账号类型',            //中文字段信息
                )
            )
        );

        return $gridinfo;
    }

}


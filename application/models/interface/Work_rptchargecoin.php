<?php
class Work_rptchargecoin  extends CI_Model// !--- MODIFY --- //
{
    public function __construct()
    {
        $this->load->model("Dball","dball",TRUE);
		$this->dblk = $this->load->database("lockcoin",TRUE);
    }

    public function getMenuDefine()
    {
        $menuinfo = array(
            'dispbutton' => array( //可视操作按钮
            ),
            'btnStatus' => array(
                'init' => array(),
                'datacheck' => array(),
                'fieldstatus' => array(
                )
            ),
            'searchdefine' => array(
                'init' => array(
                    'columns' => 0,
                ),
                'items' => array(
                    'S_modified' => array(
                        'name' => 'S_modified',
                        'type' => 'date',
                        'dispname' => '时间(始)',
                        'widthcontrol' => '4,6,6',
                    ),
                    'E_modified' => array(
                        'name' => 'E_modified',
                        'type' => 'date',
                        'dispname' => '时间(至)',
                        'widthcontrol' => '4,6,6',
                    ),
                    'transactionType' => array(
                        'name' => 'transactionType',
                        'type' => 'select',
                        'dispname' => '充提类型',
                        'widthcontrol' => '4,6,6',
                        'data' => array(array('id'=>1,'name'=>'充币'),array('id'=>2,'name'=>'提币'),),
                        'field' => 'name,id',
                    ),
                    'status' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'dispname' => '状态',
                        'widthcontrol' => '4,6,6',
                        'data' => array(array('id'=>2,'name'=>'成功'),array('id'=>3,'name'=>'失败'),array('id'=>1,'name'=>'充值中'),),
                        'field' => 'name,id',
                    ),
                    'coinCode' => array(
                        'name' => 'coinCode',
                        'type' => 'select',
                        'dispname' => '币种',
                        'widthcontrol' => '4,6,6',
                        'data' => $this->dball->getbasedatawithdb($this->dblk,"ex_product","coinCode,name","","DEL"),
                        'field' => 'coinCode,coinCode',
                    ),
                ),
            ),
            'initsearchdefine' => array(
                'S_modified' => array(
                    'name' => 'S_modified',
                    'type' => 'date',
                    'dispname' => '时间(始)',
                    'widthcontrol' => '4,6,6',
                ),
                'E_modified' => array(
                    'name' => 'E_modified',
                    'type' => 'date',
                    'dispname' => '时间(至)',
                    'widthcontrol' => '4,6,6',
                ),
               'transactionNum' => array(
                    'name' => 'transactionNum',
                    'type' => 'text',
                    'dispname' => '交易流水号',
                    'widthcontrol' => '4,6,6',
                ),
                'customerName' => array(
                    'name' => 'customerName',
                    'type' => 'text',
                    'dispname' => '用户',
                    'widthcontrol' => '4,6,6',
                ),
                'transactionType' => array(
                    'name' => 'transactionType',
                    'type' => 'select',
                    'dispname' => '充提类型',
                    'widthcontrol' => '4,6,6',
                    'data' => array(array('id'=>1,'name'=>'充币'),array('id'=>2,'name'=>'提币'),),
                    'field' => 'name,id',
                ),
                'status' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'dispname' => '状态',
                    'widthcontrol' => '4,6,6',
                    'data' => array(array('id'=>2,'name'=>'成功'),array('id'=>3,'name'=>'失败'),array('id'=>1,'name'=>'充值中'),),
                    'field' => 'name,id',
                ),
                'coinCode' => array(
                    'name' => 'coinCode',
                    'type' => 'select',
                    'dispname' => '币种',
                    'widthcontrol' => '4,6,6',
                    'data' => $this->dball->getbasedatawithdb($this->dblk,"ex_product","coinCode,name","","DEL"),
                    'field' => 'coinCode,coinCode',
                ),
                'inAddress' => array(
                    'name' => 'inAddress',
                    'type' => 'text',
                    'dispname' => '充币地址',
                    'widthcontrol' => '4,6,6',
                ),
                'outAddress' => array(
                    'name' => 'outAddress',
                    'type' => 'text',
                    'dispname' => '提币地址',
                    'widthcontrol' => '4,6,6',
                ),
                'remark' => array(
                    'name' => 'remark',
                    'type' => 'text',
                    'dispname' => '备注',
                    'widthcontrol' => '4,6,6',
                ),
			),
        );
        return $menuinfo;
    }

    public function getGridDefine()
    {
        $gridinfo = array(
            'table' => 'ex_dm_transaction',
            'keyid' => 'id',
            'listfield' => array( //显示字段
                'modified' => array(
                    'fieldname' => 'modified',
                    'dispname' => '时间',
                    'order' => 'desc',
                ),
                'transactionNum' => array(
                    'fieldname' => 'transactionNum',
                    'dispname' => '交易流水号',
                ),
                'customerName' => array(
                    'fieldname' => 'customerName',
                    'dispname' => '用户',
                ),
                'transactionType' => array(
                    'fieldname' => 'transactionType',
                    'dispname' => '充提类型',
                ),
                'transactionMoney' => array(
                    'fieldname' => 'transactionMoney',
                    'dispname' => '数量',
                ),
                'status' => array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'coinCode' => array(
                    'fieldname' => 'coinCode',
                    'dispname' => '币种代码',
                ),
            ),
            'initlistfield' => array(
                'modified' => array(
                    'fieldname' => 'modified',
                    'dispname' => '时间',
                    'order' => 'desc',
                ),
                'transactionNum' => array(
                    'fieldname' => 'transactionNum',
                    'dispname' => '交易流水号',
                ),
                'customerName' => array(
                    'fieldname' => 'customerName',
                    'dispname' => '用户',
                ),
                'transactionType' => array(
                    'fieldname' => 'transactionType',
                    'dispname' => '充提类型',
                ),
                'transactionMoney' => array(
                    'fieldname' => 'transactionMoney',
                    'dispname' => '数量',
                    'adjust' => 'rtrim0',
                ),
                'status' => array(
                    'fieldname' => 'status',
                    'dispname' => '状态',
                ),
                'coinCode' => array(
                    'fieldname' => 'coinCode',
                    'dispname' => '币种代码',
                ),
                'website' => array(
                    'fieldname' => 'website',
                    'dispname' => '来源',
                ),
                'inAddress' => array(
                    'fieldname' => 'inAddress',
                    'dispname' => '充币地址',
                ),
                'outAddress' => array(
                    'fieldname' => 'outAddress',
                    'dispname' => '提币地址',
                ),
                'remark' => array(
                    'fieldname' => 'remark',
                    'dispname' => '备注',
                ),
                'fee' => array(
                    'fieldname' => 'fee',
                    'dispname' => '手续费',
                    'adjust' => 'rtrim0',
                ),
                'surname' => array(
                    'fieldname' => 'surname',
                    'dispname' => '姓',
                ),
                'trueName' => array(
                    'fieldname' => 'trueName',
                    'dispname' => '名',
                ),
                'rejectionReason' => array(
                    'fieldname' => 'rejectionReason',
                    'dispname' => '拒绝原因',
                ),
                'confirmations' => array(
                    'fieldname' => 'confirmations',
                    'dispname' => '确认原因',
                ),
                'timereceived' => array(
                    'fieldname' => 'timereceived',
                    'dispname' => '收到时间',
                ),
                'blocktime' => array(
                    'fieldname' => 'blocktime',
                    'dispname' => '锁定时间',
                ),
                'orderNo' => array(
                    'fieldname' => 'orderNo',
                    'dispname' => '订单号',
                ),
            ),
        );
       return $gridinfo;
    }

}


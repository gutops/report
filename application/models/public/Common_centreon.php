<?php

/**
 * Class Common_tools
 */
class Common_centreon extends CI_Model
{
    /**
     * Common_tools constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getCentreonDB(){
        $CI =& get_instance();
        $CI->config->load('cfg-system');
        $cfgcentreon = $CI->config->item('cfg-system');
        $cfgcentreon = $cfgcentreon['centreon'];
        if ($cfgcentreon['active']){
            $dsn = 'mysqli://'.$cfgcentreon['account'].':'.$cfgcentreon['password'].'@'.$cfgcentreon['host'].'/'.$cfgcentreon['db'];
            $dbcentreon = $this->load->database($dsn,true);
            return $dbcentreon;
        }
        else  return false;
    }

    public function chgPasswd($username,$password){
        if ($dbcentreon = self::getCentreonDB()){
            $update['contact_passwd'] = $password;
            $dbcentreon->where("contact_alias",$username);
            return $dbcentreon->update('contact',$update);
        } else return false;
    }

    public function addContact($username,$password,$fullname=""){
        if ($dbcentreon = self::getCentreonDB()){
            $data = $dbcentreon->get_where("contact",array("contact_alias"=>"$username"))->result_array();
            if (sizeof($data) > 0) {
                return true;
            }
            $insert = array();
            $insert['contact_name'] = $fullname;
            $insert['contact_alias'] = $username;
            $insert['contact_passwd'] = $password;
            $insert['contact_lang'] = 'browser';
            $insert['contact_email'] = 'abc@abc.com';
            $insert['contact_js_effects'] = '0';
            $insert['contact_oreon'] = '1';
            $insert['reach_api'] = 0;
            $insert['contact_enable_notifications'] = '2';
            $insert['contact_admin'] = '0';
            $insert['contact_activate'] = '1';
            $insert['contact_auth_type'] = 'local';
            $insert['contact_register'] = 1;
            return $dbcentreon->insert('contact',$insert);
        } else return false;
    }
}
